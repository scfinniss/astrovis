#ifndef INFO
#define INFO

#include <vector>
#include <sys/time.h>
#include "../fits/Datacube.h"

class Info {
    
public:
    Info();
    ~Info();
    
    std::vector<Datacube*> datacubes;
    std::vector<double> times;
    
    void print_info ();
    void Add (Datacube * dc);
    void start_timer();
    void stop_timer();
    void begin_timer ();
    void end_timer ();
    
    struct timeval start, end, begin, stop;
    
    double time;
    double beginTime;
    double endTime;
    double average;
    
};


#endif