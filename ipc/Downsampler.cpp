#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cmath>
#include <errno.h>
#include <QtGui>
#include "../fits/FitsFileHandler.h"
#include "../fits/Datacube.h"
#include "Downsampler.h"
#include "info.h"
#include <float.h>
#include <queue>
#include <boost/thread.hpp>

#include "../fits/ViewTestWidget.h"

#include "../common.h"

using namespace boost;

Downsampler* Downsampler::dsBusy = NULL;

Downsampler::Downsampler (FitsFileHandler * c, int header) {

    this->cube = c;

    c->changeHeader(header);

    accumulate = NULL;
    average = NULL;
    dc = NULL;
    result = NULL;
    minMaxThreads = NULL;
    tempSlice = NULL;

    info = new Info ();

}

Downsampler::~Downsampler () {
    cube = NULL;

    iterationThread.join();

    if(average)
        delete average;
        
    if(accumulate)
        delete accumulate;

    if(dc)
        delete dc;

    if(result)
        delete result;

    {
        lock_guard<mutex> lg (busyLock);
        if(Downsampler::dsBusy == this)
        Downsampler::dsBusy = NULL;
    }

    if(minMaxThreads)
        delete [] minMaxThreads;
}

int Downsampler::StartDownsampling (int x, int y, int z, int sw, int sh, int sd, int w, int h, int d, int s){
    
    dprintf("Starting downsampling\n");

    {
        lock_guard<mutex> lg (busyLock);
        Downsampler::dsBusy = this;
    }

    numThreads = 8;
    minMaxThreads = new boost::thread [numThreads];

    started = false;
    readyToEmit = false;

    if(dc)
        delete dc;
    dc = NULL;

    if(accumulate)
        delete accumulate;
    accumulate = NULL;

    if(sw < w ){ //clamp output dimensions to file dimensions
        w = sw;
        dprintf("Clamped output width to %d\n", w);
    }
    if(sh < h ){
        h = sh;
        dprintf("Clamped output height to %d\n", h);
    }
    if (sd < d){
        d = sd;
        dprintf("Clamped output depth to %d\n", d);
    }

    if(average)
        delete average;
    average = NULL;
        
    accumulate = new Datacube (w, h, d);

    iteration = 0;
    iterationMax = -1;

    current = -1;

	abort = false;
    pause = false;
    
    itrX = 0;
    itrY = 0;
    itrZ = 0;

    if(x < 0) 
    	x = 0;
    
    if(y < 0) 
    	y = 0;
    
    if(z < 0)     	
    	z = 0;

    if(x + sw > cube->width()) { //check sub region falls within boundaries of file dimensions
    	dprintf("Clamped subregion to cube edge (X %d)\n", sw);
        sw = cube->width() - x;
    }
    
    if(y + sh > cube->height()) {
    	dprintf("Clamped subregion to cube edge (Y %d)\n", sh);
        sh = cube->height() - y;
    }
    
    if(z + sd > cube->depth()) {
    	sd = cube->depth() - z;
        dprintf("Clamped subregion to cube edge (Z %d)\n", sd);
    }

    chunkWidth = sw;
    chunkHeight = sh;
    chunkDepth = 1;

    this->x = x;
    this->y = y;
    this->z = z;
    this->sw = sw;
    this->sh = sh; 
    this->sd = sd;
    this->w = w;
    this->h = h; 
    this->d = d;
    numChunksX = sw/chunkWidth;
    numChunksY = sh/chunkHeight;
    numChunksZ = sd/chunkDepth;
    subChunkWidth = sw/(float)w;
    subChunkHeight = sh/(float)h;
    subChunkDepth = sd/(float)d;

    skipAmount = 3;
    dprintf("skipAmount %f", skipAmount);
    currentOffset = 0;

    lastCont = 0;
    lastContCount = 1;

    if(tempSlice)
        delete tempSlice;

    tempSlice = new Datacube (w,h,1);

    scatterIteration = 0;
    scatterIterationMax = s;

    sliceIteration = 0;
    sliceIterationMax = sd;

    iteration = 0;
    iterationMax = scatterIterationMax + sliceIterationMax;

    dprintf("StartDownsampling(%d, %d, %d, %d, %d, %d, %d, %d, %d, %d)\n", x,y,z ,sw,sh,sd, w,h,d, s);

    /*
    dprintf("iterationMax %d\n", iterationMax);
    dprintf("chunkWidth %f sw: %d w: %d\n", chunkWidth, sw, w);
    dprintf("chunkHeight %f sh: %d h: %d\n", chunkHeight, sh, h);
    dprintf("chunkDepth %f sd: %d d: %d\n", chunkDepth, sd, d);
    dprintf("numChunksX %d \n", numChunksX);
    dprintf("numChunksY %d \n", numChunksY);
    dprintf("numChunksZ %d \n", numChunksZ);
    */

    iterationThread = boost::thread(&Downsampler::runIterations, this);

    emit startedDS ();

    return 0;
}

void Downsampler::StopDownsampling(){
    
    if(started){
        abort = true;
        cube->abort();
        iterationThread.join();
    }
}

void Downsampler::Join(){
    
        iterationThread.join();
}

void Downsampler::runIterations () { //runs all the iterations in a separate thread

    info->start_timer();

    started = true;

    for(int i = 0; i < scatterIterationMax; i++){

         DoScatterRead();

        if(abort){
            if(cube){
                cube->abort();
                cube->resetRegion();
            }

            {
                lock_guard<mutex> lg (busyLock);
                Downsampler::dsBusy = NULL;
            }
            EmitEnd();
            return;
        }
    }

    if(scatterIterationMax > 0){ //average out the accumulation buffer before doing sequential

        for(int i = 0; i < accumulate->width * accumulate->height * accumulate->depth; i++){

            accumulate->getFloats()[i] /= (scatterIteration+1);

            if(accumulate->getFloats()[i] > accumulate->max) //set min and max for the accumulation datacube
                accumulate->max = accumulate->getFloats()[i];

            if(accumulate->getFloats()[i] < accumulate->min)
                    accumulate->min = accumulate->getFloats()[i];

            }
    }

    for(int i = 0; i < sliceIterationMax; i++){

        DoSliceRead();

        if(abort){
            if(cube){
                cube->abort();
                cube->resetRegion();
            }

            {
                lock_guard<mutex> lg (busyLock);
                Downsampler::dsBusy = NULL;
            }
            EmitEnd();
            return;
            
        }
    }

    {
        lock_guard<mutex> lg (busyLock);
        Downsampler::dsBusy = NULL;
    }
    Emit(-2);
    EmitEnd();

    info->stop_timer();
}

void Downsampler::DoScatterRead (){

    dprintf("Starting Scatter read %d/%d\n", scatterIteration+1, scatterIterationMax);

    ScatterPreFetch ();

    ThreadPause();

    dc = cube->getData();

    if(dc == NULL){ //if the getdata returns null, abort downsampling
        abort = true;
        Clean ();
        return;
    }

    ThreadPause();
    if(abort){
        Clean ();
        return;
    }
    if(average)
        delete average;
    average = new Datacube (w, h, d);
    
    for(int i = 0; i < dc->width * dc->height * dc->depth; i++){

        accumulate->getFloats()[i] += dc->getFloats()[i];
        average->getFloats()[i] = accumulate->getFloats()[i] / (scatterIteration+1);

        if(average->getFloats()[i] > average->max) //set min and max for the output datacube
                average->max = average->getFloats()[i];
            
        if(average->getFloats()[i] < average->min)
                average->min = average->getFloats()[i];
    }

    boost::thread * t = new boost::thread [numThreads];

    float subArrayLength = average->width * average->height * average->depth / (float)numThreads;

    for(int i = 0; i < numThreads; i ++){ //need to normalise the smooth and rough data separately (they have very different min and max's, not sure why)
        t[i] = boost::thread(&Downsampler::Normalise, this, i, subArrayLength, true, average);
    }

    for(int i = 0; i < numThreads; i ++){
        t[i].join();
    }

    delete [] t;

    ThreadPause();
    if(abort){
        Clean ();
        return;
    }

    PushResult(average);

    Emit(-1);
    EmitProgress(iteration / (float)iterationMax);
    
    delete dc;
    dc = NULL;

    scatterIteration++;
    iteration++;
}

void Downsampler::DoSliceRead (){
    
    //dprintf("Processing slice %d/%d\n", sliceIteration+1, sliceIterationMax); 

    ThreadPause();

    if(sliceIteration == 0 && cube != NULL) //used for double buffering slice reads (hides processing time behind reads)
    cube->readSlice(x, y, sw, sh, z + sliceIteration);

    if(iteration != iterationMax-1 && cube != NULL)
    cube->readSlice(x, y, sw, sh, z + sliceIteration+skipAmount);

    if(cube != NULL) {
        dc = cube->getSliceData ();
    }

    if(dc == NULL){
        //dprintf("dc = null\n");
        abort = true;
        Clean ();
        return;
    }
    
    ThreadPause();
    if(abort){
        Clean ();
        return;
    }
    
    if(average)
        delete average;
    average = new Datacube (w, h, d);
    Downsample(dc, average);

    PushResult(average);

    if(readyToEmit){
        Emit(current-1);
        if(sliceIteration == sliceIterationMax-1) {
            Emit(current);
        }
        readyToEmit = false;
    }
    EmitProgress(iteration / (float)iterationMax);

    ThreadPause();
    if(abort){
        Clean ();
        return;
    }

    delete dc;
    dc = NULL;
    
    sliceIteration += skipAmount;

    if(sliceIteration > sliceIterationMax){

        sliceIteration = ++currentOffset;
    }

    iteration++; //let world know what iteration we are on
}

void Downsampler::Emit (double c){
    emit iterationComplete (c);
}

void Downsampler::EmitProgress (float p){
    emit progress (p);
}

void Downsampler::EmitEnd () {
    emit endedDS ();
}

void Downsampler::ScatterPreFetch () {

    cube->begin(
            w,
            h,
            d,
            1,
            1,
            1);
    
   for(int i = 0; i < d; i++){
        for(int j = 0; j < h; j++){
            for(int k = 0; k < w ; k++){
                
                ThreadPause ();
                
                float takeX = itrX + k * sw / (float)w; //find coordinate to request chunk at
                float takeY = itrY + j * sh / (float)h;
                float takeZ = itrZ + i * sd / (float)d;

                cube->ready(x + floor(takeX), y + floor(takeY), z + floor(takeZ));
                
            }
        }
    }
    cube->end ();

    IncrementItr();
}

void Downsampler::IncrementItr(){ //function for incrementing a 1d counter in a 3d fashion
    
    (itrX)++;
    
    if(itrX >= numChunksX){
        itrX = 0;
        (itrY)++;
    }
    
    if(itrY >= numChunksY){
        itrY = 0;
        (itrZ)++;
    }
    
}

void Downsampler::Downsample (Datacube * input, Datacube * output) {
    
    int outputSlice = (d / (float)sd) * sliceIteration;

    double perSliceCont = sd / (double)d;

    double step = 1 / perSliceCont;
    current = sliceIteration / perSliceCont;
    double last = (sliceIteration > 0 ? sliceIteration-1 : 0) / perSliceCont;
    double next = (sliceIteration+1) / perSliceCont;

    for(int j = 0; j < output->height; j++){
        for(int k = 0; k < output->width; k++){
            
            float takeX = k * subChunkWidth; //find coordinate of chunk within datacube coordinates
            float takeY = j * subChunkHeight;
            
            
            float subChunkX = floor(takeX + ceil(subChunkWidth)) - floor(takeX); //deals with the fractional subChunks
            float subChunkY = floor(takeY + ceil(subChunkHeight)) - floor(takeY);
            
            float accumulation = 0;
            float count = 0;
            
            for(int y = 0; y < subChunkY; y++){ //accumulate all the pixels in the subchunk
                for(int x = 0; x < subChunkX; x++) {
                    accumulation += input->getPixel(takeX+x,takeY+y, 0);
                    count++;
                }
            }

            accumulation /= count;
            
            if((int)last != (int)current || sliceIteration >= sliceIterationMax-1){
                accumulate->setPixel(k,j,current-1, tempSlice->getPixel(k,j,0)/(float)lastContCount);
                tempSlice->setPixel(k,j,0, accumulation);
                accumulate->setPixel(k,j,current, tempSlice->getPixel(k,j,0));
                readyToEmit = true;
            }
            else {
                tempSlice->setPixel(k,j,0, tempSlice->getPixel(k,j,0) + accumulation); //accumulate into a temporary place so that the min and max doesnt get skewed
            }
        }
    }


    if((sliceIteration >= sliceIterationMax-1) || scatterIterationMax == 0)
        findMinMax(output->min, output->max, accumulate->getFloats(), accumulate->width * accumulate->height * accumulate->depth);

    boost::thread * t = new boost::thread [numThreads];

    float subArrayLength = accumulate->width * accumulate->height * accumulate->depth / (float)numThreads;

    for(int i = 0; i < numThreads; i ++){ //normalise the result split over multiple threads
        t[i] = boost::thread(&Downsampler::Normalise, this, i, subArrayLength, ((sliceIteration >= sliceIterationMax-1) || scatterIterationMax == 0), output);
    }

    for(int i = 0; i < numThreads; i ++){
        t[i].join();
    }

    delete [] t;

    output->max = 1;
    output->min = 0;

    if(lastCont == (int)current && current != 0) //keeps track which slices contribute where
        lastContCount++;
    else{
        lastCont = (int)current;
        lastContCount = 1;
    }

}

void Downsampler::Normalise (int ID, float subArrayLength, bool useNewMinMax, Datacube * output){ //c++11 threading magic
    
    int startAt = ceil(ID * subArrayLength);

    for (int i = startAt; i < startAt + floor(subArrayLength); i++){
        
        if(useNewMinMax){

            float normalised = (accumulate->getFloats()[i] - output->min)/(output->max - output->min);
            output->getFloats()[i] = normalised;
        }
        else {
            float normalised = (accumulate->getFloats()[i] - accumulate->min)/(accumulate->max - accumulate->min);
            output->getFloats()[i] = normalised;
        }
    }
}

void Downsampler::scanMinMax (threadParams * &tp){
    
    int startAt = ceil(tp->ID * tp->subArrayLength);

    for(int i = startAt; i < startAt + tp->subArrayLength; i ++){
        if(tp->array[i] > tp->max) 
            tp->max = tp->array[i];

        if(tp->array[i] < tp->min)
            tp->min = tp->array[i];
    }
}

void Downsampler::findMinMax(float &min, float &max, float * array, int arrayLength) { //parallelizes min max finding
    
    float threadPortion = arrayLength / numThreads;

    threadParams * tp = new threadParams [numThreads];

    min = FLT_MAX;
    max = FLT_MIN;

    for(int i = 0; i < numThreads; i++){
        
        tp[i].ID = i;
        tp[i].min = FLT_MAX;
        tp[i].max = FLT_MIN;
        tp[i].array = array;
        tp[i].subArrayLength = threadPortion;

        minMaxThreads[i] = boost::thread(&Downsampler::scanMinMax, this, &tp[i]);
    }

    for(int i = 0; i < numThreads; i++){
        
        minMaxThreads[i].join();

        if(tp[i].max > max) 
            max = tp[i].max;

        if(tp[i].min < min)
            min = tp[i].min;

    }

    delete [] tp;
}

void Downsampler::Clean (){
    
    if(cube)
        cube->abort();

    if(accumulate){
        delete accumulate;
        accumulate = NULL;
    }
    
    if(dc){
        delete dc;
        dc = NULL;
    }
}
    
void Downsampler::ThreadPause () {
    unique_lock<mutex> lg (pauseLock);
    while(pause)
        pauseWait.wait(lg);

}

void Downsampler::Pause (){
    unique_lock<mutex> lg (pauseLock);
    cube->pause();
    pause = true;
    dprintf("Downsampler paused\n");
}
    
void Downsampler::Resume (){
    unique_lock<mutex> lg (pauseLock);
    cube->resume();
    pause = false;
    dprintf("Downsampler resumed\n");
    pauseWait.notify_one();
}

Datacube * Downsampler::GetLatestResult () {

    lock_guard<mutex> lg(resultLock);
    Datacube * dc = result;

    result = NULL;

    return dc;
}

void Downsampler::PushResult (Datacube * &dc){

    lock_guard<mutex> lg(resultLock);
    if(result != NULL)
        delete result;

	result = dc;

    dc = NULL;
}
