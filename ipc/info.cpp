#include <cstdio>
#include <vector>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include "../fits/Datacube.h"
#include "info.h"

#include "../common.h"

Info::Info () {
    time = 0;
    beginTime= 0;
    endTime = 0;
    average = 0;
}

Info::~Info () {
   
    for(int n = 0; n < datacubes.size(); n++){
        delete datacubes[n];
    }
    datacubes.clear();
    
}

void Info::Add (Datacube * dc) {
    
    Datacube * temp = new Datacube(dc->width, dc->height, dc->depth);
    
    for(int i = 0; i < dc->depth; i++)
        for(int j = 0; j < dc->height; j++)
            for(int k = 0; k < dc->width; k++){
                temp->setPixel(k,j,i, dc->getPixel(k,j,i));
            }
    
    datacubes.push_back(temp);
}

void Info::print_info () {
    
    int width = datacubes[0]->width;
    int height = datacubes[0]->height;
    int depth = datacubes[0]->depth;
    
    Datacube * lastItr = datacubes.back();
    
    double normalize [datacubes.size()-1];
    
    printf("Iteration differences:\n");
    
    for(int n = 0; n < datacubes.size()-1; n++){
        
        double temp = 0;
        
        for(int i = 0; i < depth; i++){
            for(int j = 0; j < height; j++) {
                for(int k = 0; k < width; k++){
                    
                    temp += fabs(lastItr->getPixel(k,j,i) - datacubes[n]->getPixel(k,j,i)); //difference between last iteration and current one
                    
                }
            }
        }
        
        normalize[n] = temp;
        
        
    }
    
    double max = 0;
    for(int n = 0; n < datacubes.size()-1; n++)
        max = normalize[n] > max ? normalize[n] : max;

    for(int n = 0; n < datacubes.size()-1; n++)
        normalize[n] /= max;
    
    for(int n = 0; n < datacubes.size()-1; n++)
    dprintf("i%d:\t%.3f\n",n, normalize[n]);
    
    for(int n = 0; n < datacubes.size(); n++){
        delete datacubes[n];
    }
    datacubes.clear();
    
}

void Info::start_timer () {
    gettimeofday(&start, NULL);
    dprintf("## timer started\n");
}

void Info::stop_timer () {
    gettimeofday(&stop, NULL);
    gettimeofday(&end, NULL);
    
    time = (stop.tv_sec - start.tv_sec)*1000 + (stop.tv_usec - start.tv_usec)/1000;
    
    times.push_back(time);
    average += time;
    endTime = (end.tv_sec - begin.tv_sec)*1000 + (end.tv_usec - begin.tv_usec)/1000;
    
    dprintf("## time: %f\n", time/1000);
    dprintf("## total time: %f\n", endTime/1000);
}

void Info::begin_timer () {
    gettimeofday(&begin, NULL);
    dprintf("## begin timer started\n");
}

void Info::end_timer () {
    gettimeofday(&end, NULL);
    endTime = (end.tv_sec - begin.tv_sec)*1000 + (end.tv_usec - begin.tv_usec)/1000;
    dprintf("## end total time: %f\n", endTime/1000);
    average /= datacubes.size();
    dprintf("## oteration average time: %f\n", average/1000);
    
    for(int i = 0; i < times.size(); i++)
        dprintf("## time i%d: %f\n", i, times[i]/1000);
}