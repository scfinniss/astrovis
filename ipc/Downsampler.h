#ifndef DOWNSAMPLER
#define DOWNSAMPLER


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <queue>
#include <QtGui>
#include <boost/thread.hpp>
#include "../fits/FitsFileHandler.h"
#include "../fits/Datacube.h"
#include "info.h"
#include "../common.h"

using namespace boost;

class Downsampler : public QObject {
    
    Q_OBJECT

public:

    FitsFileHandler * cube;

    Downsampler (FitsFileHandler * cube, int header);
    ~Downsampler ();
    
    int StartDownsampling (int x, int y, int z, int sw, int sh, int sd, int w, int h, int d, int s);
    
    void StopDownsampling();
    void Join();
    void Pause ();
    void Resume ();
    Datacube * GetLatestResult ();

    bool started;

    int iteration;
    int iterationMax;

    int scatterIteration;
    int scatterIterationMax;

    int sliceIteration;
    int sliceIterationMax;

    static Downsampler* dsBusy;
    
private:

    Info * info;

    struct threadParams {
        
        int ID;
        float min;
        float max;
        float * array;
        float subArrayLength;
    };

    void Normalise (int ID, float subArrayLength, bool useNewMinMax, Datacube * output);
    void scanMinMax (threadParams * &tp);
    void findMinMax(float &min, float &max, float * array, int arrayLength);
    void DoScatterRead ();
    void DoSliceRead ();
    void IncrementItr();
    void Clean ();
    void ThreadPause ();
    void Downsample (Datacube * input, Datacube * output);
    void ScatterPreFetch ();
    void MinMax (Datacube * input, Datacube * output);
    void PushResult (Datacube * &dc);
    void runIterations ();

    void Emit (double c);
    void EmitProgress (float p);
    void EmitEnd ();

    
    bool readyToEmit;
    
    long maxMemory;
    
    Datacube * average;
    Datacube * dc;
    Datacube * accumulate;
    Datacube * result;
    Datacube * tempSlice;

    int lastCont;
    int lastContCount;

    int itrX;
    int itrY;
    int itrZ;
    
    bool abort;
    bool pause;

    float subChunkWidth;
    float subChunkHeight;
    float subChunkDepth;
    int x;
    int y;
    int z;
    int sw;
    int sh; 
    int sd;
    int w;
    int h; 
    int d;
    float chunkWidth;
    float chunkHeight;
    float chunkDepth;
    int numChunksX;
    int numChunksY;
    int numChunksZ;

    float skipAmount;
    int currentOffset;

    double current;

    condition_variable pauseWait;
    mutex pauseLock;
    boost::thread iterationThread;
    mutex busyLock;
    mutex resultLock;
    boost::thread * minMaxThreads;

    int numThreads;

    signals:
    void iterationComplete (double c);
    void progress (float p);

    void startedDS ();
    void endedDS ();
};



#endif