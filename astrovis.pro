QT += core gui opengl
TARGET = astrovis
TEMPLATE = app

OBJECTS_DIR = build/obj
MOC_DIR = build/moc
RCC_DIR = buid/rcc
UI_DIR = build/ui

win32 {
	LIBS = ./lib/win32/libboost_thread.lib ./lib/win32/glew32s.lib ./lib/win32/cfitsio.lib
    INCLUDEPATH = ./include
}
unix {
	LIBS = -lboost_thread -lcfitsio -lGLU -lGLEW
}
macx {
	LIBS = -L./lib/macx -lboost_thread -lcfitsio -lGLEW	
    INCLUDEPATH = ./include
}

HEADERS += ui_astrovis.h
HEADERS += astrovis.h
SOURCES += astrovis.cpp
HEADERS += headerviewer.h
SOURCES += headerviewer.cpp
HEADERS += common.h
SOURCES += main.cpp

HEADERS += fits/Datacube.h
SOURCES += fits/Datacube.cpp
HEADERS += fits/FitsFileHandler.h
SOURCES += fits/FitsFileHandler.cpp
HEADERS += fits/FitsImageHeader.h
SOURCES += fits/FitsImageHeader.cpp
HEADERS += fits/FitsTreeWidget.h
SOURCES += fits/FitsTreeWidget.cpp
HEADERS += fits/ViewTestWidget.h
SOURCES += fits/ViewTestWidget.cpp
HEADERS += fits/ViewTestWindow.h
SOURCES += fits/ViewTestWindow.cpp

HEADERS += gui/Context.h
SOURCES += gui/Context.cpp
HEADERS += gui/DetailContext.h
SOURCES += gui/DetailContext.cpp
HEADERS += gui/DetailRender.h
SOURCES += gui/DetailRender.cpp
HEADERS += gui/InfoBox.h
SOURCES += gui/InfoBox.cpp
HEADERS += gui/RefineDialog.h
SOURCES += gui/RefineDialog.cpp
HEADERS += gui/ValueDisplay.h
SOURCES += gui/ValueDisplay.cpp
HEADERS += gui/ValueSlider.h
SOURCES += gui/ValueSlider.cpp
HEADERS += gui/VolRender.h
SOURCES += gui/VolRender.cpp
HEADERS += gui/VolumeContext.h
SOURCES += gui/VolumeContext.cpp
HEADERS += gui/ShaderObject.h
SOURCES += gui/ShaderObject.cpp

HEADERS += ipc/Downsampler.h
SOURCES += ipc/Downsampler.cpp
HEADERS += ipc/info.h
SOURCES += ipc/info.cpp
