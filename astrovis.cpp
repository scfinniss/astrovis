#include "astrovis.h"
#include "ui_astrovis.h"
#include "gui/VolRender.h"
#include "gui/DetailRender.h"
#include "gui/ValueSlider.h"
#include "headerviewer.h"
#include "fits/FitsFileHandler.h"
#include "fits/FitsTreeWidget.h"
#include "fits/ViewTestWindow.h"

#include "gui/RefineDialog.h"

#include <QtGui>

Astrovis::Astrovis(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Astrovis)
{
    ui->setupUi(this);
    
    hv = new Headerviewer (this);
    vtw = new ViewTestWindow (this);
    
    #ifdef __APPLE__
    Shader = new ShaderObject("./astrovis.app/Contents/Resources/Basic.vsh", "./astrovis.app/Contents/Resources/Basic.fsh");
    #else
    Shader = new ShaderObject("Basic.vsh", "Basic.fsh");
    #endif


    tree = new FitsTreeWidget(this);
    tree -> setColumnCount (2);
    QStringList sl;
    sl << "Name";
    sl << "Type";
    tree -> setHeaderLabels (sl);
    ui->TreeViewLayout->addWidget (tree);

    vr = new VolRender (this,tree,Shader);
    ui->VolRenderLayout->addWidget(vr);
    dr = new DetailRender(this, tree,vr, ui->DetailViewLayout,Shader);
    //ui->DetailViewLayout->addWidget(dr);
    colourSlider = new ValueSlider(this,1,1,1,0);
    
    connect(colourSlider, SIGNAL(sliderChange(float**,int*,int)),vr,SLOT(setTransferMap(float**,int*,int)));
    connect(colourSlider, SIGNAL(sliderChange(float**,int*,int)),dr,SLOT(setTransferMap(float**,int*,int)));
   
    
    ui->ColourRangeLayout->addWidget(colourSlider);
    QColor c [5];
    c[0]=QColor(0,0,0);
    c[1]=QColor(0,0,255);
    c[2]=QColor(255,0,0);
    c[3]=QColor(255,255,0);
    c[4]=QColor(255,255,255);
    /*c[0]=QColor(0,0,0);
    c[1]=QColor(0,0,255);
    c[2]=QColor(0,255,0);
    c[3]=QColor(255,255,255);*/

    colourSlider->initValues(5,c);
    transSlider = new ValueSlider(this,0,0,0,1);

    QColor c2 [5];
    c2[0]=QColor(0, 0, 0 ,0);
    c2[1]=QColor(0, 0, 0 ,0);
    c2[2]=QColor(127,127,127,127);
    c2[3]=QColor(255,255,255,255);
    c2[4]=QColor(255,255,255,255);
    transSlider->initValues(5,c2);

    connect(transSlider, SIGNAL(sliderChange(float**,int*,int)),vr,SLOT(setTransferMap(float**,int*,int)));

    ui->OpacityRangeLayout->addWidget(transSlider);

    rd = new RefineDialog (this);

    rd->setFileDimensions(960, 720, 300);
    
    rd_refine = new RefineDialog (this);

    infoBox = new InfoBox(this);
    ui->InfoBoxLayout->addWidget(infoBox);
    
    vr->connectInfoBox(infoBox);


    CreateMenus ();
}

Astrovis::~Astrovis()
{
    delete ui;
    delete hv;
}

void Astrovis::CreateMenus () {
    
    QMenu * fileMenu = menuBar()->addMenu("File");

    QAction * open = new QAction("Open", this);
    connect(open, SIGNAL(triggered()), tree, SLOT(openFile()));
    fileMenu->addAction(open);


    QAction * viewHeader = new QAction("View Header", this);
    connect(viewHeader, SIGNAL(triggered()), hv, SLOT(DisplayHeader()));
    connect(tree, SIGNAL(itemClicked(QTreeWidgetItem *, int)), hv, SLOT(GetHeader(QTreeWidgetItem *)));
    fileMenu->addAction(viewHeader);

    QAction * viewVTW = new QAction("View Debug Viewer", this);
    connect(viewVTW, SIGNAL(triggered()), vtw, SLOT(DisplayViewTest()));
    //connect(tree, SIGNAL(itemClicked(QTreeWidgetItem *, int)), vtw, SLOT(GetHeader(QTreeWidgetItem *)));
    fileMenu->addAction(viewVTW);

    QMenu * volumeMenu = menuBar()->addMenu("Volume Renderer");

    QAction * frames = new QAction("Show Frame", this);
    frames->setCheckable(true);
    frames->setChecked(true);
    connect(frames, SIGNAL(triggered()), vr, SLOT(toggleFrames()));
    volumeMenu->addAction(frames);

    QAction * transparent = new QAction("Transparency", this);
    transparent->setCheckable(true);
    transparent->setChecked(true);
    connect(transparent, SIGNAL(triggered()), vr, SLOT(toggleTransparent()));
    volumeMenu->addAction(transparent);
    
    QAction * refine = new QAction("Refine Volume", this);
    connect(refine,SIGNAL(triggered()), vr, SLOT(refineMenuTrigger()));
    connect(vr, SIGNAL(refineDialog(QTreeWidgetItem*,int,int,int)), rd_refine, SLOT(setFitsFileHandler(QTreeWidgetItem *,int,int,int)));
    connect(rd_refine, SIGNAL(refine(QTreeWidgetItem*, int, int, int, int)), vr, SLOT(refineVolumeRequest(QTreeWidgetItem*,int, int, int, int)));
    volumeMenu->addAction(refine);

    QMenu * sliderMenu = menuBar()->addMenu("Value Slider");

    QAction * blendMode = new QAction("Blend Values", this);
    blendMode->setCheckable(true);
    blendMode->setChecked(true);
    connect(blendMode, SIGNAL(triggered()), colourSlider, SLOT(blendToggle()));
    connect(blendMode, SIGNAL(triggered()), transSlider, SLOT(blendToggle()));
    sliderMenu->addAction(blendMode);

    connect(tree,SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), rd, SLOT(setFitsFileHandler(QTreeWidgetItem*)));
    connect(rd, SIGNAL(refine(QTreeWidgetItem*, int, int, int, int)), vr, SLOT(fitsImageHeaderLoadRequest(QTreeWidgetItem*, int, int, int, int)));
}
