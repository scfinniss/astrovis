#ifndef SIGGI_COMMON
#define SIGGI_COMMON

#define dprintf(...) do { printf("[file %s, line %d]: ", __FILE__, __LINE__); printf(__VA_ARGS__);} while (0)
#define eprintf(...) do { fprintf(stderr, "[file %s, line %d]: ", __FILE__, __LINE__); fprintf(stderr, __VA_ARGS__);} while (0)

#endif
