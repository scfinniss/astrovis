/********************************************************************************
** Form generated from reading UI file 'astrovis.ui'
**
** Created: Wed Feb 8 12:48:11 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ASTROVIS_H
#define UI_ASTROVIS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Astrovis
{
public:
    QAction *action;
    QWidget *centralWidget;
    QGridLayout *centralWidgetLayout;
    QFrame *TreeViewFrame;
    QGridLayout *TreeViewLayout;
    QFrame *InfoBoxFrame;
    QGridLayout *InfoBoxLayout;
    QFrame *DetailViewFrame;
    QGridLayout *DetailViewLayout;
    QFrame *VolRenderFrame;
    QGridLayout *VolRenderLayout;
    QFrame *SliderFrame;
    QVBoxLayout *SliderFrameLayout;
    QFrame *ColourRange;
    QGridLayout *ColourRangeLayout;
    QFrame *OpacityRange;
    QGridLayout *OpacityRangeLayout;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Astrovis)
    {
        if (Astrovis->objectName().isEmpty())
            Astrovis->setObjectName(QString::fromUtf8("Astrovis"));
        Astrovis->resize(1336, 852);
        action = new QAction(Astrovis);
        action->setObjectName(QString::fromUtf8("action"));
        centralWidget = new QWidget(Astrovis);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidgetLayout = new QGridLayout(centralWidget);
        centralWidgetLayout->setSpacing(6);
        centralWidgetLayout->setContentsMargins(11, 11, 11, 11);
        centralWidgetLayout->setObjectName(QString::fromUtf8("centralWidgetLayout"));
        TreeViewFrame = new QFrame(centralWidget);
        TreeViewFrame->setObjectName(QString::fromUtf8("TreeViewFrame"));
        TreeViewFrame->setFrameShape(QFrame::StyledPanel);
        TreeViewFrame->setFrameShadow(QFrame::Raised);
        TreeViewLayout = new QGridLayout(TreeViewFrame);
        TreeViewLayout->setSpacing(0);
        TreeViewLayout->setContentsMargins(0, 0, 0, 0);
        TreeViewLayout->setObjectName(QString::fromUtf8("TreeViewLayout"));

        centralWidgetLayout->addWidget(TreeViewFrame, 0, 0, 1, 1);

        InfoBoxFrame = new QFrame(centralWidget);
        InfoBoxFrame->setObjectName(QString::fromUtf8("InfoBoxFrame"));
        InfoBoxFrame->setFrameShape(QFrame::StyledPanel);
        InfoBoxFrame->setFrameShadow(QFrame::Raised);
        InfoBoxLayout = new QGridLayout(InfoBoxFrame);
        InfoBoxLayout->setSpacing(0);
        InfoBoxLayout->setContentsMargins(0, 0, 0, 0);
        InfoBoxLayout->setObjectName(QString::fromUtf8("InfoBoxLayout"));

        centralWidgetLayout->addWidget(InfoBoxFrame, 0, 1, 1, 1);

        DetailViewFrame = new QFrame(centralWidget);
        DetailViewFrame->setObjectName(QString::fromUtf8("DetailViewFrame"));
        DetailViewFrame->setFrameShape(QFrame::StyledPanel);
        DetailViewFrame->setFrameShadow(QFrame::Raised);
        DetailViewLayout = new QGridLayout(DetailViewFrame);
        DetailViewLayout->setSpacing(0);
        DetailViewLayout->setContentsMargins(0, 0, 0, 0);
        DetailViewLayout->setObjectName(QString::fromUtf8("DetailViewLayout"));

        centralWidgetLayout->addWidget(DetailViewFrame, 0, 2, 2, 1);

        VolRenderFrame = new QFrame(centralWidget);
        VolRenderFrame->setObjectName(QString::fromUtf8("VolRenderFrame"));
        VolRenderFrame->setFrameShape(QFrame::StyledPanel);
        VolRenderFrame->setFrameShadow(QFrame::Raised);
        VolRenderLayout = new QGridLayout(VolRenderFrame);
        VolRenderLayout->setSpacing(0);
        VolRenderLayout->setContentsMargins(0, 0, 0, 0);
        VolRenderLayout->setObjectName(QString::fromUtf8("VolRenderLayout"));

        centralWidgetLayout->addWidget(VolRenderFrame, 1, 0, 1, 2);

        SliderFrame = new QFrame(centralWidget);
        SliderFrame->setObjectName(QString::fromUtf8("SliderFrame"));
        SliderFrame->setFrameShape(QFrame::StyledPanel);
        SliderFrame->setFrameShadow(QFrame::Raised);
        SliderFrameLayout = new QVBoxLayout(SliderFrame);
        SliderFrameLayout->setSpacing(6);
        SliderFrameLayout->setContentsMargins(11, 11, 11, 11);
        SliderFrameLayout->setObjectName(QString::fromUtf8("SliderFrameLayout"));
        SliderFrameLayout->setContentsMargins(-1, 5, -1, 5);
        ColourRange = new QFrame(SliderFrame);
        ColourRange->setObjectName(QString::fromUtf8("ColourRange"));
        ColourRange->setAutoFillBackground(false);
        ColourRangeLayout = new QGridLayout(ColourRange);
        ColourRangeLayout->setSpacing(0);
        ColourRangeLayout->setContentsMargins(0, 0, 0, 0);
        ColourRangeLayout->setObjectName(QString::fromUtf8("ColourRangeLayout"));

        SliderFrameLayout->addWidget(ColourRange);

        OpacityRange = new QFrame(SliderFrame);
        OpacityRange->setObjectName(QString::fromUtf8("OpacityRange"));
        OpacityRangeLayout = new QGridLayout(OpacityRange);
        OpacityRangeLayout->setSpacing(0);
        OpacityRangeLayout->setContentsMargins(0, 0, 0, 0);
        OpacityRangeLayout->setObjectName(QString::fromUtf8("OpacityRangeLayout"));

        SliderFrameLayout->addWidget(OpacityRange);


        centralWidgetLayout->addWidget(SliderFrame, 2, 0, 1, 3);

        centralWidgetLayout->setRowStretch(0, 4);
        centralWidgetLayout->setRowStretch(1, 10);
        centralWidgetLayout->setRowStretch(2, 1);
        centralWidgetLayout->setColumnStretch(0, 1);
        centralWidgetLayout->setColumnStretch(1, 1);
        centralWidgetLayout->setColumnStretch(2, 2);
        Astrovis->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Astrovis);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1336, 25));
        Astrovis->setMenuBar(menuBar);
        statusBar = new QStatusBar(Astrovis);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Astrovis->setStatusBar(statusBar);

        retranslateUi(Astrovis);

        QMetaObject::connectSlotsByName(Astrovis);
    } // setupUi

    void retranslateUi(QMainWindow *Astrovis)
    {
        Astrovis->setWindowTitle(QApplication::translate("Astrovis", "Astrovis", 0, QApplication::UnicodeUTF8));
        action->setText(QApplication::translate("Astrovis", "View Header", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Astrovis: public Ui_Astrovis {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ASTROVIS_H
