#include "headerviewer.h"
#include "fits/FitsFileHandler.h"
#include "fits/FitsTreeWidget.h"
#include "fits/FitsImageHeader.h"
#include <string>

#include <QtGui>

Headerviewer::Headerviewer( QMainWindow * parent) : QDialog (parent){


	setWindowTitle(QString::fromUtf8("File Header"));
	resize(500, 400);
	setContentsMargins (1, 1, 1, 1);
	
	table = new QTableWidget(this);
	table->setColumnCount(3);
	table->horizontalHeader()->setStretchLastSection(true);
	table->horizontalHeader()->setDefaultSectionSize(150);
	table->verticalHeader()->hide();
	table->show();

	layout = new QGridLayout (this);
	layout->addWidget(table);

	sl << "Name";
	sl << "Value";
	sl << "Comment";

	table->setHorizontalHeaderLabels(sl);

	cube = NULL;

}

void Headerviewer::DisplayHeader() {
	
	this->setVisible(true);
}

void Headerviewer::GetHeader(QTreeWidgetItem* i){
	
	FitsFileHandler * cube;

	if(i->type()==1001){
    	
    	cube = (FitsFileHandler*)(i);
    }
    else if(i->type()==1002){
        FitsImageHeader * f = (FitsImageHeader*)(i);
        cube = f->getParent();
    }
    else return;
	
	std::vector<Card> v = *cube->getHeader();

	table->setRowCount(v.size());

	for(unsigned int i = 0; i < v.size(); i++){

		QString name (v[i].name.c_str());
		table->setItem(i,0, new QTableWidgetItem (name));

		QString value (v[i].value.c_str());
		table->setItem(i,1, new QTableWidgetItem (value));

		QString comment (v[i].comment.c_str());
		table->setItem(i,2, new QTableWidgetItem (comment));
	}
}

Headerviewer::~Headerviewer(){

	delete table;
	delete layout;
}