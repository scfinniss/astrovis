#ifndef IMAGE_H
#define IMAGE_H

class Datacube {
    private:
        float * datacube;

    public:
        int width;
        int height;
        int depth;

        float min;
        float max;

        Datacube (int width, int height, int depth, float * datacube);

        Datacube (int width, int height, int depth);

        ~Datacube ();

        float getPixel(int x, int y, int z);

        bool setPixel (int x, int y, int z, float v);

        float* getFloats ();
};

#endif
