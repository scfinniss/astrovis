#ifndef SIGGI_IMAGEHEADER
#define SIGGI_IMAGEHEADER

#include <QtGui>
#include "FitsFileHandler.h"

class FitsImageHeader : public QTreeWidgetItem {
private:
	int index;
	FitsFileHandler* parentCube;
	int width, height, depth;
	char* name;

public:
	FitsImageHeader (int index, QTreeWidgetItem* parent);
	~FitsImageHeader ();

	int getWidth ();
	int getHeight ();
	int getDepth ();

	int getIndex ();

	FitsFileHandler* getParent ();
};

#endif