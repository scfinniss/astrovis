#include <QtGui>
#include "ViewTestWindow.h"
#include "ViewTestWidget.h"
#include "FitsFileHandler.h"
#include "FitsImageHeader.h"

ViewTestWindow::ViewTestWindow( QMainWindow * parent): QDialog (parent) {
	setWindowTitle(QString::fromUtf8("Debug Viewer"));
	resize(800, 600);

	vt = new ViewTestWidget (this);
	layout = new QGridLayout (this);
	layout->setContentsMargins (0, 0, 0, 0);
	layout->addWidget(vt);
}

void ViewTestWindow::DisplayViewTest () {
	setVisible(true);
}

void ViewTestWindow::DisplayDatacube (Datacube* d) {
	vt -> display (d);
}

void ViewTestWindow::GetHeader(QTreeWidgetItem* i){
	/*
	FitsFileHandler * cube;

	if(i->type()==1001){
    	
    	cube = (FitsFileHandler*)(i);
    }
    if(i->type()==1002){
        FitsImageHeader * f = (FitsImageHeader*)(i);
        cube = f->getParent();
    }
	
	Datacube *d = cube -> readRegion (
		0, 0, 0, 
		cube->width(), cube->height(), cube->depth(), 
		4, 4, 4);

	vt -> display (d);
	vt -> update ();
	delete d;
	*/
}