#include <cstdlib>
#include <cstdio>
#include <errno.h>
#include <queue>
#include <algorithm>
#include <vector>
#include <string>
#include <cstring>
#include <float.h>
#include "fitsio.h"
extern "C" {
    #include "fitsio2.h"
}

#include "FitsFileHandler.h"
#include "FitsImageHeader.h"
#include "ViewTestWidget.h"

#include "Datacube.h"
#include "../common.h"

using namespace boost;

void FitsFileHandler::init (const char* name) {
    dprintf ("Opening file %s.\n", name);

    filename = new char [2048];
    memset (filename, '\0', 2048);
    sprintf (filename, "%s", name);

    int status = 0;
    customRegion = false;

    if (fits_open_file(&file, filename, READONLY, &status)) {
        dprintf ("Failed to open file %s. ", filename);
        if (status) fits_report_error (stderr, status);
        else printf ("\n");

        failToOpen = true;
        return;
    }
    failToOpen = false;

    int hdunum, hdutype;
    fits_get_num_hdus(file, &hdunum, &status);
    for (int i = 1; i <= hdunum; i++) {
        fits_movabs_hdu(file, i, NULL, &status);
        fits_get_hdu_type (file, &hdutype, &status);
        if (hdutype == IMAGE_HDU) {
            img.push_back(i);
        }
    }

    dataSize = 0;
    jobSize = 0;
    jid = -1;

    header = new std::vector <Card> ();

    paused = false;
    aborting = false;
    abortingRegion = false;
    prog = 0.0f;

    o = NULL;
    naxes = NULL;
    rt = NULL;

    currentHeader = 0;
    changeHeader (currentHeader);
}

FitsFileHandler::FitsFileHandler (const char* name) {
    init (name);
}

FitsFileHandler::FitsFileHandler (const char* name, QTreeWidget * parent)
:QTreeWidgetItem (parent, 1001) {
    init (name);

    if (failToOpen) {
        emit openFailed ();
    }
    else {
        size_t found;
        std::string filename = std::string (name);
        found = filename.find_last_of("/\\");

        setText (1, "FITS File");
        setText (0, filename.substr(found+1).c_str());

        for (unsigned int i = 0; i < img.size(); i++) {
            new FitsImageHeader (i, this);
        }
    }
}

FitsFileHandler::~FitsFileHandler () {
    abort ();
    regionAbort ();
    if (rt != NULL) rt -> join ();
    int status = 0;
    fits_close_file (file, &status);
    if (o) free (o);
    if (naxes) free (naxes);
    header->clear ();
    delete header;
    delete [] filename;
}

void FitsFileHandler::selectRegion (int x, int y, int z, int w, int h, int d) {
    char buffer [2048];
    memset (buffer, '\0', 2048);
    sprintf (buffer, "%s[%d:%d:%d, %d:%d:%d, %d:%d:%d]", filename, x+1, x+w, 1, y+1, y+h, 1, z+1, z+d, 1);
    int status = 0;
    fits_close_file (file, &status);
    dprintf ("Opening file %s.\n", buffer);

    if (fits_open_file(&file, buffer, READONLY, &status)) {
        dprintf ("Failed to open file %s. ", buffer);
        if (status) fits_report_error (stderr, status);
        else printf ("\n");
    }

    customRegion = true;
    changeHeader (currentHeader);
}

void FitsFileHandler::resetRegion () {
    if (!customRegion) return;
    dprintf ("resetRegion ()\n");
    int status = 0;
    fits_close_file (file, &status);
    if (fits_open_file(&file, filename, READONLY, &status)) {
        dprintf ("Failed to open file %s. ", filename);
        if (status) fits_report_error (stderr, status);
        else printf ("\n");
    }    
    customRegion = false;
    changeHeader (currentHeader);
}

void FitsFileHandler::changeHeader (unsigned int n) {
    //resetRegion ();
    lock_guard <mutex> flg (fitsMutex);
    int status = 0;
    if (n >= 0 && n < img.size()) {
        fits_movabs_hdu(file, img[n], NULL, &status);

        fits_get_img_dim (file, &naxis, &status);
        if (naxes) free (naxes);
        naxes = (long*) malloc ((naxis > 3 ? naxis : 3) * sizeof(long));
        naxes[0] = naxes[1] = naxes[2] = 1;
        fits_get_img_size (file, naxis, naxes, &status);
        fits_get_img_equivtype (file, &bitpix, &status);

        header->clear ();
        if (o) free (o);
        o = (long*) malloc (naxis > 3 ? naxis * sizeof(long) : 3 * sizeof(long));

        for (int i = 0; i < (naxis > 3 ? naxis : 3); i++) o[0] = 1;

        int hdupos, nkeys, ii;
        char card[FLEN_CARD];

        fits_get_hdu_num(file, &hdupos);
        fits_get_hdrspace(file, &nkeys, NULL, &status);

        for (ii = 1; ii <= nkeys; ii++) {
            if (fits_read_record(file, ii, card, &status)) break;
            
            std::string s (card);
            if (s.size() > 8) {
                Card c;
                c.name = std::string (s.begin(), s.begin()+8);
                c.value = std::string (s.begin()+8, s.end());
                if (c.value[0] == '=') c.value = std::string (c.value.begin()+1, c.value.end());
                size_t k = c.value.find (" /");
                if (k != std::string::npos) {
                    c.comment = std::string (c.value.begin()+k+1, c.value.end());
                    c.value = std::string (c.value.begin(), c.value.begin()+k);
                }
                else c.comment = std::string ("");
                header->push_back (c);
            }
        }

        if (status == END_OF_FILE)  status = 0;

        currentHeader = n;
    }
}

std::vector <Card>* FitsFileHandler::getHeader () {
    return header;
}

int FitsFileHandler::getNumImg () {
    return img.size();
}

Datacube* FitsFileHandler::getData () {
	float* data = NULL;
	prog = 0.0f;

    lock_guard <mutex> gl (abortMutex);
	if (!threadQueue.empty()) {
        threadQueue.front() -> join ();
        data = readResults.front();
        readResults.pop();
        delete threadQueue.front();
	    threadQueue.pop();
        threadIdQueue.pop();
        readResourceCondition.notify_one ();
	}

	Datacube *d = new Datacube (chunk[0], chunk[1], chunk[2], data);
	return d;
}

void FitsFileHandler::ready (int xx, int yy, int zz) {
    for (int y = 0; y < subchunk[1]; y++)
        for (int z = 0; z < subchunk[2]; z++) {
            jid++;

            job* t = &jobs[jid];

            t->p = 0;
            t->p += origin[2] * chunk[1] * chunk[0] * subchunk[2]; //Z
            t->p += origin[1] * chunk[0] * subchunk[1]; //Y
            t->p += origin[0] * subchunk[0]; //X
            t->p += z * chunk[0] * chunk[1]; //z
            t->p += y * chunk[0]; //y

            t->o = xx + (yy+y) * naxes[0] + (zz+z) * naxes[0] * naxes[1];
        }

    origin[0] ++;
    if (origin[0] >= chunk[0]/subchunk[0]) {
        origin[0] = 0;
        origin[1] ++;
        if (origin[1] >= chunk[1]/subchunk[1]) {
            origin[1] = 0;
            origin[2] ++;
        }
    }
}

void FitsFileHandler::begin (int cw, int ch, int cd, int sw, int sh, int sd) {
	aborting = false;
	//dprintf ("%d %d %d %d %d %d\n", cw, ch, cd, sw, sh, sd);
	origin[0] = origin[1] = origin[2] = 0;

    chunk[0] = cw * sw;
    chunk[1] = ch * sh;
    chunk[2] = cd * sd;

    subchunk[0] = sw;
    subchunk[1] = sh;
    subchunk[2] = sd;

    dataSize = chunk[0] * chunk[1] * chunk[2] * sizeof(float);
    jobSize = cw * ch * cd * sh * sd * sizeof (job);

    if (dataSize >= jobSize) {
    	shareMem = malloc (dataSize);
        memset (shareMem, 0, dataSize);
    	jobs = (job*) ((char*)shareMem + dataSize - jobSize);
    }
    else {
    	shareMem = malloc (jobSize);
        memset (shareMem, 0, jobSize);
    	jobs = (job*) shareMem;
    }

    /*
    dprintf ("       Data Size: %10.2fMiB\n", dataSize / 1048576.0);
    dprintf ("        Job Size: %10.2fMiB\n", jobSize / 1048576.0);
    dprintf ("  Old Total Size: %10.2fMiB\n", (dataSize + jobSize) / 1048576.0);
    dprintf ("     Memory Used: %10.2fMiB\n", shareSize / 1048576.0);
    dprintf ("Percent Decrease: %10.0f%%\n", (1.0 - (double)shareSize/(double)(dataSize + jobSize)) * 100.0);
    */
}

void FitsFileHandler::end () {
    boost::thread* t = new boost::thread (&FitsFileHandler::threadEnd, this, jobs, jobSize, dataSize, shareMem);
    threadQueue.push (t);
    threadIdQueue.push (t->get_id());
    jobs = NULL;
    jobSize = 0;
    jid = -1;
}

void FitsFileHandler::readLine (long o, long p, float* data) {
    lock_guard <mutex> flg (fitsMutex);
    int status = 0;
    static char cdummy;
    ffgcle(
            file,
            2,
            1,
            o,
            subchunk[0],
            1,
            1,
            0,
            data + p,
            &cdummy,
            NULL,
            &status);
}

void FitsFileHandler::threadEnd (job* jobs, size_t jobSize, size_t dataSize, void* shareMem) {
    //eprintf ("FitsFileHandler::threadEnd (job* jobs = %p, size_t jobSize = %lu, size_t dataSize = %lu, void* shareMem = %p)\n", jobs, jobSize, dataSize, shareMem);
	std::sort (jobs, jobs + jobSize/sizeof(job));

    {
        unique_lock <mutex> ul (readResourceMutex);
    	while (this_thread::get_id() != threadIdQueue.front()) {
            readResourceCondition.wait (ul);
    		if (aborting) break;
    	}
    }

	float* data = (float*) shareMem;
	size_t total = 0;

	for (unsigned int i = 0; i < jobSize/sizeof(job); i++) {
		if (aborting) break;
		readLine (jobs[i].o, jobs[i].p, data);
		total += subchunk[0];

        {
            lock_guard <mutex> lg (progressMutex);
    		prog = (float)total/(dataSize/sizeof(float));
        }

        unique_lock <mutex> ul (pauseMutex);
		while (paused) pauseCondition.wait (ul);
	}

    readResults.push(data);
    //eprintf ("~FitsFileHandler::threadEnd (job* jobs = %p, size_t jobSize = %lu, size_t dataSize = %lu, void* shareMem = %p)\n", jobs, jobSize, dataSize, shareMem);
	return;
}

int FitsFileHandler::width () {
	return naxes[0];
}

int FitsFileHandler::height () {
	return naxes[1];
}

int FitsFileHandler::depth () {
	return naxes[2];
}

void FitsFileHandler::pause () {
    dprintf ("[%s] paused\n", filename);
    /*
    lock_guard <mutex> lg (pauseMutex);
    paused = true;
    */
}

void FitsFileHandler::resume () {
    dprintf ("[%s] resumed\n", filename);
    /*
    lock_guard <mutex> lg (pauseMutex);
    paused = false;
    pauseCondition.notify_one ();
    */
}

void FitsFileHandler::abort () {
    aborting = true;
    lock_guard <mutex> lg (abortMutex);
    while (!threadQueue.empty()) {
        readResourceCondition.notify_one();
        threadQueue.front() -> join ();
        delete threadQueue.front();
    	threadQueue.pop();
        threadIdQueue.pop();
        while (!readResults.empty()) {
            float* data = readResults.front();
            readResults.pop();
            free (data);
        }
    }

    while (!threadSliceQueue.empty()) {
        sliceCondition.notify_one ();
        threadSliceQueue.front() -> join ();
        delete threadSliceQueue.front();
        threadSliceQueue.pop();
        threadSliceIdQueue.pop();
        while (!sliceResults.empty()) {
            Datacube* data = sliceResults.front();
            sliceResults.pop();
            delete data;
        }
    }
}

void FitsFileHandler::writeFile (char* name, int x, int y, int z, int w, int h, int d) {
    fitsfile *fptr;
    int status;
    char buffer [2048];
    memset (buffer, '\0', 2048);
    sprintf (buffer, "%i:%i, %i:%i, %i:%i", x+1, x+w, y+1, y+h, z+1, z+d);

    status = 0;
    fits_create_file(&fptr, name, &status);
    fits_copy_image_section(file, fptr, buffer, &status);
    fits_close_file(fptr, &status);
    fits_report_error(stderr, status);
}

float FitsFileHandler::progress () {
    lock_guard <mutex> mt (progressMutex);
	return prog;
}

void FitsFileHandler::readSlice (int x1, int y1, int w, int h, int z1) {
    aborting = false;
    boost::thread* slice = new boost::thread (&FitsFileHandler::sliceThread, this, x1, y1, w, h, z1);
    threadSliceQueue.push (slice);
    threadSliceIdQueue.push (slice->get_id());
}

Datacube* FitsFileHandler::getSliceData () {
    lock_guard<mutex> mt (abortMutex);
    boost::thread* slice = threadSliceQueue.front ();
    Datacube* d = NULL;
    slice -> join ();
    threadSliceQueue.pop();
    threadSliceIdQueue.pop();
    d = sliceResults.front ();
    sliceResults.pop();
    delete slice;
    sliceCondition.notify_one ();
    return d;
}

void FitsFileHandler::sliceThread (int x1, int y1, int w, int h, int z1) {
    //ßeprintf ("FitsFileHandler::sliceThread (int x1 = %d, int y1 = %d, int w = %d, int h = %d, int z1 = %d)\n", x1, y1, w, h, z1);
    int naxis = this->naxis > 3 ? this->naxis : 3;
    int status = 0;

    float* data = (float*) malloc (naxes[0] * naxes[1] * sizeof(float));
    long* fpixel = new long [naxis];
    long* lpixel = new long [naxis];
    long* inc = new long [naxis];

    for (int i = 0; i < naxis; i++) {
        fpixel[i] = 1;
        lpixel[i] = 1;
        inc [i] = 1;
    }

    fpixel[0] = x1 + 1;
    lpixel[0] = x1 + w;
    fpixel[1] = y1 + 1;
    lpixel[1] = y1 + h;
    fpixel[2] = z1 + 1;
    lpixel[2] = z1 + 1;

    inc[0] = 1;
    inc[1] = 1;
    inc[2] = 1;

    /*
    {
        unique_lock<mutex> ul (sliceMutex);
        while (this_thread::get_id() != threadSliceIdQueue.front()) {
            sliceCondition.wait (ul);
        }
    }
    */

    if (!aborting) {
        lock_guard <mutex> flg (fitsMutex);
        fits_read_subset(file, TFLOAT, fpixel, lpixel, inc, NULL, data, NULL, &status);
    }

    delete [] fpixel;
    delete [] lpixel;
    delete [] inc;

    if (status) fits_report_error (stderr, status);

    lock_guard <mutex> lg(srMutex);
    sliceResults.push(new Datacube (w, h, 1, data));
    //eprintf ("~FitsFileHandler::sliceThread (int x1 = %d, int y1 = %d, int w = %d, int h = %d, int z1 = %d)\n", x1, y1, w, h, z1);
}

void FitsFileHandler::readRegion (int x, int y, int z, int w, int h, int d) {
    regionAbort();    

    {
        lock_guard<mutex> lg (abortRegionMutex);
        abortingRegion = false;
    }
    rt = new boost::thread (&FitsFileHandler::regionThread, this, x, y, z, w, h, d);
}

void FitsFileHandler::regionAbort () {
    //eprintf ("FitsFileHandler::regionAbort ()\n");
    {
        lock_guard<mutex> lg (abortRegionMutex);
        abortingRegion = true;
    }

    if (rt != NULL) {
        rt -> join ();
        delete rt;
        rt = NULL;
    }
}

void FitsFileHandler::regionThread (int x, int y, int z, int w, int h, int d) {
    //eprintf ("FitsFileHandler::regionThread (int x = %d, int y = %d, int z = %d, int w = %d, int h = %d, int d = %d)\n", x, y, z, w, h, d);
    int naxis = this->naxis > 3 ? this->naxis : 3;

    if (w == 0 || h == 0 || d == 0) return;
    int status = 0;
    
    long* fpixel = new long [naxis];
    long* lpixel = new long [naxis];
    long* inc = new long [naxis];

    for (int i = 0; i < naxis; i++) {
        fpixel[i] = 1;
        lpixel[i] = 1;
        inc [i] = 1;
    }

    float min = FLT_MAX;
    float max = FLT_MIN;

    for (int i = 1; i <= d; i++) {
        float* data = (float*) malloc (w * h * sizeof(float));

        fpixel[0] = x + 1;
        lpixel[0] = x + w;
        fpixel[1] = y + 1;
        lpixel[1] = y + h;
        fpixel[2] = z + i;
        lpixel[2] = z + i;

        {
            lock_guard <mutex> flg (fitsMutex);
            fits_read_subset(file, TFLOAT, fpixel, lpixel, inc, NULL, data, NULL, &status);
            if (status) fits_report_error (stderr, status);
        }

        Datacube *result = NULL;
        result = new Datacube (w, h, 1, data);

        if (w == 1 || h == 1) {
            for (int i = 0; i < w * h; i++) {
                if (data[i] < min) min = data[i];
                if (data[i] > max) max = data[i];
            }
            result -> min = min;
            result -> max = max;
        }

        lock_guard<mutex> lg (abortRegionMutex);
        if (abortingRegion) break;
        else {
            emit sliceDone (i, result);
        }
    }

    delete [] fpixel;
    delete [] lpixel;
    delete [] inc;

    //eprintf ("~FitsFileHandler::regionThread (int x = %d, int y = %d, int z = %d, int w = %d, int h = %d, int d = %d)\n", x, y, z, w, h, d);
}

Datacube* FitsFileHandler::readRegion2 (int x, int y, int z, int w, int h, int d) {
    int naxis = this->naxis > 3 ? this->naxis : 3;
    if (w == 0 || h == 0 || d == 0) return NULL;
    int status = 0;

    float* data = (float*) malloc (w * h * d * sizeof(float));
    long* fpixel = new long [naxis];
    long* lpixel = new long [naxis];
    long* inc = new long [naxis];

    for (int i = 0; i < naxis; i++) {
        fpixel[i] = 1;
        lpixel[i] = 1;
        inc [i] = 1;
    }

    fpixel[0] = x + 1;
    lpixel[0] = x + w;
    fpixel[1] = y + 1;
    lpixel[1] = y + h;
    fpixel[2] = z + 1;
    lpixel[2] = z + d;

    inc[0] = 1;
    inc[1] = 1;
    inc[2] = 1;

    float min = FLT_MAX;
    float max = FLT_MIN;

    {
        lock_guard <mutex> flg (fitsMutex);
        fits_read_subset(file, TFLOAT, fpixel, lpixel, inc, NULL, data, NULL, &status);
    }

    delete [] fpixel;
    delete [] lpixel;
    delete [] inc;

    if (status) fits_report_error (stderr, status);

    Datacube *result = NULL;
    result = new Datacube (w, h, d, data);

    if (w == 1 || h == 1 || d == 1) {
        for (int i = 0; i < w * h * d; i++) {
            if (data[i] < min) min = data[i];
            if (data[i] > max) max = data[i];
        }
        result -> min = min;
        result -> max = max;
    }

    return result;
}
