#include <vector>
#include <string>

#include "FitsImageHeader.h"
#include "FitsFileHandler.h"

#define IMAGEHEADER 1002

FitsImageHeader::FitsImageHeader (int i, QTreeWidgetItem* parent):
QTreeWidgetItem (parent, 1002) {
    index = i;
    name = new char [2048];
    memset (name, '\0', 2048);
    parentCube = (FitsFileHandler*) parent;

    parentCube -> changeHeader (i);
    width = parentCube -> width();
    height = parentCube -> height();
    depth = parentCube -> depth();

    bool nameTest = false;
    std::vector<Card> tempVector = *parentCube->getHeader();
    for (unsigned int ii = 0; ii < tempVector.size(); ii++) {
    	if (tempVector[ii].name.compare ("OBJECT  ") == 0) {
    		sprintf (name, "%s", tempVector[ii].value.c_str());
    		nameTest = true;
    	}
    }

    setText (1, "Image Header");
    if (nameTest)
    	setText (0, name);
	else {
		sprintf (name, "Header %d", i + 1);
		setText (0, name);
	}
}

FitsImageHeader::~FitsImageHeader () {
	delete [] name;
}

int FitsImageHeader::getWidth () {
	return width;
}

int FitsImageHeader::getHeight () {
	return height;
}

int FitsImageHeader::getDepth () {
	return depth;
}

int FitsImageHeader::getIndex () {
	return index;
}

FitsFileHandler* FitsImageHeader::getParent () {
	return parentCube;
}
