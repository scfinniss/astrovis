#ifndef SIGGI_VTWI_H
#define SIGGI_VTWI_H

#include "ViewTestWidget.h"
#include "Datacube.h"

#include <QtGui>

class ViewTestWindow : public QDialog {
	
	Q_OBJECT

private:
	ViewTestWidget* vt;

public:

	ViewTestWindow( QMainWindow * parent);

	QGridLayout * layout;

public slots:
	void DisplayViewTest ();
	void DisplayDatacube (Datacube* d);
	void GetHeader(QTreeWidgetItem* i);

};

#endif