#ifndef SIGGI_VTW_H
#define SIGGI_VTW_H

#define GL_GLEXT_PROTOTYPES

#include <GL/glew.h>
#include <QtGui>
#include <QGLWidget>

#ifdef __APPLE__
#include <OpenGL/glext.h>
#include <OpenGL/glu.h>
#else
#include <GL/glext.h>
#include <GL/glu.h>
#endif

#include "Datacube.h"
#include <pthread.h>

class ViewTestWidget : public QGLWidget {
	Q_OBJECT

public:
	ViewTestWidget (QWidget *parent);
	void display (Datacube* d);
	static ViewTestWidget* getInstance();

protected:
	unsigned char* cArray;
	float* pArray;
	size_t pSize;

	double camx;
	double camy;
	double dist;
	double rot;

	pthread_mutex_t display_mutex;

	GLuint cVbo;
	GLuint pVbo;

	bool change;
	bool mouseDown;
	float cx, cy, px, py;

	int maxdim;

	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void mousePressEvent ( QMouseEvent * e );
	void mouseReleaseEvent( QMouseEvent * e );
	void mouseMoveEvent ( QMouseEvent * e );

	static bool instanceFlag;
	static ViewTestWidget* _instance;

};

#endif

#define test_display(A) \
do { \
	ViewTestWidget* temp_ViewTestWidget_instance = NULL; \
	temp_ViewTestWidget_instance = ViewTestWidget::getInstance(); \
	if (temp_ViewTestWidget_instance != NULL) temp_ViewTestWidget_instance -> display(A); \
} while (0)
