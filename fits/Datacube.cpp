#include <cstdio>
#include <cstdlib>
#include "Datacube.h"
#include "../common.h"

Datacube::Datacube (int width, int height, int depth, float * datacube) {
    this->width = width;
    this->height = height;
    this->depth = depth;
    this->datacube = datacube;

    min = 0.0f;
    max = 0.0f;
}

Datacube::Datacube (int width, int height, int depth) {
    if(width > 0 && height > 0 && depth > 0) {
        this->width = width;
        this->height = height;
        this->depth = depth;
        datacube = (float*) malloc (width * height * depth * sizeof(float));
        for(int i = 0; i < width*height*depth; i++) {
            datacube[i] = 0;
        }
    }
    else {
        //dprintf("WARNING: datacube must have positive dimensions\n");
        exit (EXIT_FAILURE);
    }

    min = 0.0f;
    max = 0.0f;
}

Datacube::~Datacube () {
    free (datacube);
}

float Datacube::getPixel(int x, int y, int z){
    if(x >= 0 && x < width && y >= 0 && y < height && z >= 0 && z < depth){
        return datacube[x + y*width + z*width*height];
    }
    else {
        //dprintf("WARNING: pixel access out of bounds when accessing (%d, %d, %d)\n", x, y, z);
        return -1;
    }
}

bool Datacube::setPixel (int x, int y, int z, float v) {
    if(x >= 0 && x < width && y >= 0 && y < height && z >= 0 && z < depth){
        datacube[x + y*width + z*width*height] = v;
        return true;
    }
    else {
        //dprintf("WARNING: pixel access out of bounds when accesing (%d, %d, %d)\n", x, y, z);
        return false;
    }
}

float* Datacube::getFloats () {
    return this->datacube;
}

#include <cmath>
static float dist (float ax, float ay, float az, float bx, float by, float bz) {
    float dx = ax - bx;
    float dy = ay - by;
    float dz = az - bz;
    float result = dx*dx + dy*dy + dz*dz;
    return result;
}
