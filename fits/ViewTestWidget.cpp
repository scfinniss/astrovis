#define GL_GLEXT_PROTOTYPES

#include <float.h>

#include "ViewTestWidget.h"
#include "Datacube.h"

ViewTestWidget* ViewTestWidget::_instance=NULL;
bool ViewTestWidget::instanceFlag=false;

ViewTestWidget::ViewTestWidget(QWidget *parent) {
    setMouseTracking(true);

	cArray = NULL;
	pArray = NULL;
	pSize = 0;

	camx = 0.0;
	camy = 0.0;
	dist = -10.0;
	rot = 7.9;

    mouseDown = false;
    px = py = cx = cy = 0.0f;

	//display_mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_init (&display_mutex, NULL);

	cVbo = 0;
	pVbo = 0;

	change = false;
	
	maxdim = 0;

    _instance = this;
    instanceFlag = true;
}

void ViewTestWidget::initializeGL() {
	makeCurrent();
	glDisable(GL_POINT_SMOOTH);

    //glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                        
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.01f);

    glClearColor (0.0, 0.0, 0.0, 1.0);
}

void ViewTestWidget::resizeGL(int w, int h) {
	makeCurrent();
	glViewport (0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(40.0, (float)w/(float)h, 0.1, 10000.0);
    glMatrixMode(GL_MODELVIEW);
}

void ViewTestWidget::paintGL() {
    makeCurrent();

    if (dist > -0.1) dist = -0.1;
    glPointSize(-10000.f/dist / maxdim);
    camx = sin(rot) * dist;
    camy = cos(rot) * dist;

    if (pSize) {
        pthread_mutex_lock (&display_mutex);
        if (change) {
            change = false;
            if (cVbo != 0) glDeleteBuffersARB(1,  &cVbo);
            if (pVbo != 0) glDeleteBuffersARB(1,  &pVbo);
            glGenBuffersARB(1, &cVbo);
            glBindBufferARB(GL_ARRAY_BUFFER_ARB, cVbo);
            glBufferDataARB(GL_ARRAY_BUFFER_ARB, pSize * 4 * sizeof(unsigned char), cArray, GL_STATIC_DRAW_ARB);

            glGenBuffersARB(1, &pVbo);
            glBindBufferARB(GL_ARRAY_BUFFER_ARB, pVbo);
            glBufferDataARB(GL_ARRAY_BUFFER_ARB, pSize * 3 * sizeof(float), pArray, GL_STATIC_DRAW_ARB);

            //glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
            //glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);

            free (cArray);
            cArray = NULL;

            free (pArray);
            pArray = NULL;
        }
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        gluLookAt ( camy, 0.0, camx,
                0.0, 0.0, 0.0,
                0.0, 1.0, 0.0);

        glBindBufferARB(GL_ARRAY_BUFFER, pVbo);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, 0);

        glBindBufferARB(GL_ARRAY_BUFFER, cVbo);
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(4, GL_UNSIGNED_BYTE, 0, 0);

        glDrawArrays(GL_POINTS, 0, pSize);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        
        glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
        glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);

        //glFlush();
        pthread_mutex_unlock (&display_mutex);
    }	
}

void ViewTestWidget::display (Datacube* d) {
    if (d == NULL || !parentWidget ()->isVisible()) {
        return;
    }

    float* p = NULL;
    unsigned char* c = NULL;
    size_t ps = 0;

    double min = DBL_MAX;
    double max = DBL_MIN;

    for (int i = 0; i < d->width * d->height * d->depth; i++) {
        float p = d -> getFloats() [i];
        if (p < min) min = p;
        if (p > max) max = p;
    }

    //if (d-> min < min) min = d->min;
    //if (d-> max > max) max = d->max;

    double scale = max - min;
    p = (float*) malloc (d->width * d->height * d->depth * 3 * sizeof(float));
    c = (unsigned char*) malloc (d->width * d->height * d->depth * 4 * sizeof(unsigned char));

    
    if (maxdim < d->width) maxdim = d->width;
    if (maxdim < d->height) maxdim = d->height;
    if (maxdim < d->depth) maxdim = d->depth;
    srand (time(NULL));

    for (int x = 0; x < d->width; x++)
        for (int y = 0; y < d->height; y++)
            for (int z = 0; z < d->depth; z++) {
                //if (d-> getPixel (x, y, z)) {
                //float opac = (d -> getPixel (x, y, z) - min) / scale;
                float opac = (d -> getFloats() [x + y * d->width + z * d->width * d->height] - min) / scale;
                float inc = 0.1;
                float td = opac + inc;
                float ti = 1.0f + inc;
                float dopac = (td*td*td) / (ti*ti*ti);
                if (((x == 0 || y == 0 || x == d->width -1 || y == d->height -1) && (z == 0 || z == d->depth-1)) || 
                (x == 0 && y == 0) ||
                (x == d->width-1 && y == 0) ||
                (y == d->height-1 && x == 0) ||
                (x == d->width-1 && y == d->height-1)
                ) {
                    c[ps * 4 + 0] = (unsigned char)(1.0f * 255);// * opac + 0.0f * (1.0f - opac);
                    c[ps * 4 + 1] = (unsigned char)(0.0f * 255);// * opac + 0.5f * (1.0f - opac);
                    c[ps * 4 + 2] = (unsigned char)(0.0f * 255);// * opac + 1.0f * (1.0f - opac);
                    c[ps * 4 + 3] = (unsigned char)(1.0f * 255);//dopac;

                    p[ps * 3 + 0] = (float)(d->width - x) / maxdim * 5.0 - ((d->width*0.5) / maxdim) * 5.0;// + ((float)rand() / (float)RAND_MAX) * 0.005f;
                    p[ps * 3 + 1] = (float)y / maxdim * 5.0 - ((d->height*0.5) / maxdim) * 5.0;// + ((float)rand() / (float)RAND_MAX) * 0.005f;
                    p[ps * 3 + 2] = (float)z / maxdim * 5.0 - ((d->depth*0.5) / maxdim) * 5.0;// + ((float)rand() / (float)RAND_MAX) * 0.005f;
                }
                else {
                    c[ps * 4 + 0] = (unsigned char)((1.0f * opac + 0.0f * (1.0f - opac)) * 255);
                    c[ps * 4 + 1] = (unsigned char)((1.0f * opac + 0.5f * (1.0f - opac)) * 255);
                    c[ps * 4 + 2] = (unsigned char)((0.0f * opac + 1.0f * (1.0f - opac)) * 255);
                    c[ps * 4 + 3] = (unsigned char)(dopac * 255);

                    p[ps * 3 + 0] = (float)(d->width - x) / maxdim * 5.0 - ((d->width*0.5) / maxdim) * 5.0 + ((float)rand() / (float)RAND_MAX) * 0.005f;
                    p[ps * 3 + 1] = (float)y / maxdim * 5.0 - ((d->height*0.5) / maxdim) * 5.0 + ((float)rand() / (float)RAND_MAX) * 0.005f;
                    p[ps * 3 + 2] = (float)z / maxdim * 5.0 - ((d->depth*0.5) / maxdim) * 5.0 + ((float)rand() / (float)RAND_MAX) * 0.005f;
                }
                //c[ps * 4 + 3] = 2.0f / d->depth;

                ps ++;
                //}
            }

    pthread_mutex_lock (&display_mutex);

    if (pArray != NULL) free (pArray);
    if (cArray != NULL) free (cArray);

    pArray = p;
    cArray = c;
    pSize = ps;

    change = true;

    pthread_mutex_unlock (&display_mutex);

    update();
}

void ViewTestWidget::mousePressEvent ( QMouseEvent * e ) {
    if(e->button()==Qt::RightButton) mouseDown = true;
}

void ViewTestWidget::mouseReleaseEvent( QMouseEvent * e ) {
    if(e->button()==Qt::RightButton) mouseDown = false;
}

void ViewTestWidget::mouseMoveEvent ( QMouseEvent * e ) {
    px = cx;
    py = cy;
    cx = e->x();
    cy = e->y();

    if (mouseDown) {
        rot += (cx - px)/100.0f;
        dist += (py - cy)/70.0f;
        update ();
    }
}

ViewTestWidget* ViewTestWidget::getInstance() {
    if (instanceFlag) return _instance;
    else return NULL;
}