#pragma once

#include <QtGui>

class FitsTreeWidget : public QTreeWidget {
	
	Q_OBJECT

public:
	FitsTreeWidget ( QMainWindow * parent = 0 );
private slots:
	void openFile ();
	void fileToOpen (const QString &selected);
	void filesToOpen (const QStringList &selected);
	void openDialogDone (int);
private:
	bool opening;
protected:
	void resizeEvent ( QResizeEvent * event );
};