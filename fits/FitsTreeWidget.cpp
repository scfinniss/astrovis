#include <QtGui>
#include <QFileDialog>
#include "FitsTreeWidget.h"
#include "FitsFileHandler.h"

FitsTreeWidget::FitsTreeWidget ( QMainWindow * parent) : QTreeWidget (parent) {
    opening = false;
}

void FitsTreeWidget::openFile () {
	if (opening) return;
	opening = true;
	QFileDialog* dialog = new QFileDialog ( this, "Open FITS File", ".", "*.fits");

	dialog -> setFileMode (QFileDialog::ExistingFiles);
	dialog -> setVisible (true);

	//connect(dialog, SIGNAL(fileSelected (const QString)), this, SLOT(fileToOpen (const QString)));
	connect(dialog, SIGNAL(filesSelected (const QStringList)), this, SLOT(filesToOpen (const QStringList)));
	connect(dialog, SIGNAL(finished(int)), this, SLOT(openDialogDone (int)));
}

void FitsTreeWidget::fileToOpen (const QString & filename) {
	//char* temp = (char*) malloc (2048 * sizeof(char));
	char temp [2048];
	memset (temp, '\0', 2048);
	sprintf (temp, "%s", filename.toStdString ().c_str());
	new FitsFileHandler (temp, this);
	//free (temp);
	resizeColumnToContents (0);
	resizeColumnToContents (1);
}

void FitsTreeWidget::filesToOpen (const QStringList & selected) {
	for (int i = 0; i < selected.size(); i++)
		fileToOpen(selected.at(i));
}

void FitsTreeWidget::openDialogDone (int n) {
	opening = false;
}

void FitsTreeWidget::resizeEvent ( QResizeEvent * event ) {
	//printf ("%i\n", width());
	resizeColumnToContents (0);
	resizeColumnToContents (1);
	//setColumnWidth (0, (int)((float)width()/0.0075f));
	//setColumnWidth (1, (int)((float)width()/0.0025f));
}