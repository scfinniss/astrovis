#ifndef SIGGI_DATACUBE
#define SIGGI_DATACUBE

#include "fitsio.h"
#include "Datacube.h"
#include <queue>
#include <vector>
#include <string>

#include <QtGui>
#include <boost/thread.hpp>

#define FITSHANDLER 1001

struct job {
	long p;
	long o;

	bool operator< (const job &b) const {
		return o < b.o;
	}       
};

class Card {
public:
	std::string name;
	std::string value;
	std::string comment;	
};

class FitsFileHandler : public QObject, public QTreeWidgetItem {

	Q_OBJECT

public:
	FitsFileHandler (const char* filename);
	FitsFileHandler (const char* filename, QTreeWidget * parent);
	~FitsFileHandler ();

	Datacube* getData ();
	void ready  (int x, int y, int z);
	void begin (int cw, int ch, int cd, int sw, int sh, int sd);
	void end ();

	int width ();
	int height ();
	int depth ();
	void pause ();
	void resume ();
	void abort ();
	void writeFile (char* name, int x, int y, int z, int w, int h, int d);
	void bigFile (char* name, int x, int y, int z, int w, int h, int d);
	float progress ();
	size_t memUsage (int cw, int ch, int cd, int sw, int sh, int sd);

	void selectRegion (int x, int y, int z, int w, int h, int d);
	void resetRegion ();

    std::vector <Card>* header;
    std::vector <Card>* getHeader ();

    void changeHeader (unsigned int n);
    int getNumImg ();
    char* filename;

    void readRegion (int x1, int y1, int z1, int x2, int y2, int z2);
  
    void readSlice (int x1, int y1, int w, int h, int z1);
    Datacube* getSliceData ();

    Datacube* readRegion2 (int x, int y, int z, int w, int h, int d);

    void regionAbort ();
private:
	fitsfile* file;
	bool customRegion;
	bool failToOpen;

	int bitpix;
	int naxis;
	long* naxes;

	size_t dataSize;
	size_t jobSize;
	int jid;
	job* jobs;

	boost::mutex pauseMutex;
	boost::condition_variable pauseCondition;

	boost::mutex progressMutex;

	boost::mutex readResourceMutex;
	boost::condition_variable readResourceCondition;

	boost::mutex sliceMutex;
	boost::condition_variable sliceCondition;

	boost::mutex abortMutex;
	boost::mutex abortRegionMutex;

	boost::mutex fitsMutex;
	boost::mutex srMutex;

	boost::thread* rt;

	std::queue<boost::thread*> threadQueue;
	std::queue<boost::thread::id> threadIdQueue;
	std::queue<float*> readResults;

	std::queue<boost::thread*> threadSliceQueue;
	std::queue<boost::thread::id> threadSliceIdQueue;
	std::queue<Datacube*> sliceResults;

	bool paused;
	bool aborting;
	bool abortingRegion;

	long* o;

	long chunk[3];
    long subchunk[3];
    long origin[3];

    float prog;
    void* shareMem;

    std::vector<int> img;
    void init (const char* name);

    int currentHeader;

    void regionThread (int x, int y, int z, int w, int h, int d);
	void sliceThread (int x1, int y1, int w, int h, int z1);
	void readLine (long o, long p, float* data);
	void threadEnd (job* jobs, size_t jobSize, size_t dataSize, void* shareMem);

signals:
	void openFailed ();
	void regionDone (Datacube*);
	void regionSliceDone (Datacube*);
	void sliceDone (int, Datacube*);
};

#endif
