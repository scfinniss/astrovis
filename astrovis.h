#ifndef ASTROVIS_H
#define ASTROVIS_H

#include "gui/VolRender.h"
#include "gui/DetailRender.h"
#include "gui/ValueSlider.h"
#include "headerviewer.h"
#include "fits/FitsFileHandler.h"
#include "fits/FitsTreeWidget.h"
#include "fits/ViewTestWindow.h"
#include "gui/RefineDialog.h"
#include "gui/ShaderObject.h"
#include "gui/InfoBox.h"
#include <QMainWindow>

namespace Ui {
    class Astrovis;
}

class Astrovis : public QMainWindow
{
    Q_OBJECT

public:
    explicit Astrovis(QWidget *parent = 0);
    ~Astrovis();
    void CreateMenus ();


    Headerviewer * hv;
    ViewTestWindow * vtw;
    FitsTreeWidget * tree;
    VolRender * vr;
    DetailRender * dr;
    ValueSlider * colourSlider;
    ValueSlider * transSlider;
    RefineDialog * rd;
    RefineDialog * rd_refine;
    InfoBox * infoBox;
    ShaderObject * Shader;

private slots:

private:
    Ui::Astrovis *ui;
};

#endif // ASTROVIS_H
