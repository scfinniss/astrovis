#include "ValueSlider.h"
#include <cstdio>
#include "../common.h"

ValueSlider::ValueSlider(QMainWindow * parent, int r, int g, int b, int a)
:QWidget(parent)
{
	blendDisplay = true;
	selectDivider = 0;
	mouseDrag = false;

	rgba[0] = r;
	rgba[1] = g;
	rgba[2] = b;
	rgba[3] = a;

	transferSize = 256;

	rgbaMap = new float*[r + g + b + a];

	for(int i = 0; i < r + g + b + a; i++) {
		float* temp = new float[transferSize];
		for (int j = 0; j < transferSize; j++) temp[j] = 0.0f;
		rgbaMap[i] = temp;
	}
}

ValueSlider::~ValueSlider()
{
}

void ValueSlider::resizeEvent(QResizeEvent *e)
{
	
}

void ValueSlider::paintEvent(QPaintEvent *e)
{
	QPainter paint(this);
	paint.setPen(Qt::green);
	if(blendDisplay)
	{
		
		for(int i=0;i<transferSize;i++)
		{
			for(int x=1;x<Color.size();x++)
	        {
	            if(i/(float)transferSize <= Position[x])
	            {
	            	QColor b = blendColour(Color[x-1], Color[x], ((i/(float)transferSize)-Position[x-1]) / (Position[x]-Position[x-1]));
	            	paint.fillRect((i/(float)transferSize)*width(),0,ceil(width()/(float)transferSize),height(), b.rgb());

					if(rgba[0]) rgbaMap[0][i] = b.red()/255.0f;
					if(rgba[1]) rgbaMap[rgba[0]][i] = b.green()/255.0f;
					if(rgba[2]) rgbaMap[rgba[0] + rgba[1]][i] = b.blue()/255.0f;
					if(rgba[3]) rgbaMap[rgba[0] + rgba[1] + rgba[2]][i] = pow(b.alpha()/255.0f,2.5);
	            	break;
	            }
	        }
		}
		for(int i = 0; i * 2 + 1 < Position.size() - 1; i++)
		{
    		paint.drawLine(Position[i*2+1] * width(), 0, Position[i*2+1] * width(), height());
			paint.drawLine(Position[i*2+1] * width() - 1, 0, Position[i*2+1] * width() - 1, height());
		}
	}
	else
	{
		for(int i=0;i<Color.size()-1;i++)
		{
			paint.fillRect(Position[i]*width(),0,Position[i+1]*width(),height(),Color[i].rgb());
			paint.drawLine(Position[i]*width(),0,Position[i]*width(),height());
			paint.drawLine(Position[i+1]*width()-1,0,Position[i+1]*width()-1,height());
			solidTable();
		}
	}
	emit(sliderChange(rgbaMap,rgba,transferSize));
}

int ValueSlider::blendColour(int c1,int c2,float scale)
{
    return c1*(1.0-scale)+c2*(scale);
}

QColor ValueSlider::blendColour(QColor c1,QColor c2,float scale)
{
    return QColor(c1.red()*(1.0f-scale)+c2.red()*(scale),c1.green()*(1.0f-scale)+c2.green()*(scale),c1.blue()*(1.0f-scale)+c2.blue()*(scale),c1.alpha()*(1.0f-scale)+c2.alpha()*(scale));
}

void ValueSlider::initValues(int count, QColor * colours)
{
	Color.clear();
	Position.clear();
	addDivider(colours[0], 0.0f);

	//dprintf ("%u %u\n", count, Color.size());

	if(blendDisplay)
	{
		for(int i = 1; i < count * 2 - 2; i++)
		{
			if (i%2) addDivider(blendColour(colours[i/2],colours[i/2+1],0.5f),(1.0f/(count*2-2))*i);
			else addDivider(colours[i/2],(1.0f/(count*2-2))*i);
		}
		addDivider(colours[count-1],1.0f);
	}
	else
	{
		for(int i=1;i<count;i++)
		{
			addDivider(colours[i],(1.0f/(count))*i);
		}
		addDivider(colours[count-1],1.0f);
	}
	update();
}

void ValueSlider::addDivider(QColor c, float p)
{
	Color.append(c);
	Position.append(p);
}

void ValueSlider::removeColour(int i)
{
	
}

void ValueSlider::createColourTable()
{	
    if(blendDisplay)
    	blendTable();
    else
    	solidTable();

    
}

void ValueSlider::blendToggle()
{
	blendDisplay=!blendDisplay;
	if(blendDisplay)
	{
		dprintf("%d\n",Color.mid(0,Color.size()-1).size());
		initValues(Color.size()-1,Color.mid(0,Color.size()-1).data());
	}
	else
	{
		QColor c [(Color.size()+1)/2];
		for(int i=0;i<(Color.size()+1)/2;i++)
		{
			c[i]=Color[i*2];
		}
		initValues((Color.size()+1)/2,c);
	}
}

void ValueSlider::blendTable()
{
	
}

void ValueSlider::solidTable()
{
	for(int i=0;i<transferSize;i++)
    {
        float num=(float)i/transferSize;

        for(int x=1;x<Color.size();x++)
        {
            if(num<=Position[x])
            {
                if(rgba[0])
                    rgbaMap[0][i]=Color[x-1].red()/255.0f;
                if(rgba[1])
                    rgbaMap[rgba[0]][i]=Color[x-1].green()/255.0f;
                if(rgba[2])
                    rgbaMap[rgba[0]+rgba[1]][i]=Color[x-1].blue()/255.0f;
                if(rgba[3])
                    rgbaMap[rgba[0]+rgba[1]+rgba[2]][i]=pow(Color[x-1].alpha()/255.0f,3);//num*num*num;
                break;
            }
        }
    }
}

void ValueSlider::mouseMoveEvent(QMouseEvent *e)
{	
	if(selectDivider > 0 && selectDivider < Position.size())
	{
		Position[selectDivider]=std::min(Position[selectDivider+1]-6/(float)width(),
													std::max(Position[selectDivider-1]+6/(float)width(),
														e->x()/(float)width()));
		if(blendDisplay) 
		{
			if(selectDivider >= 2)
			{
				Position[selectDivider - 1] = (Position[selectDivider] + Position[selectDivider-2]) / 2.0f;
			}
			if(Color.size() - 3 >= selectDivider)
			{
				Position[selectDivider + 1] = (Position[selectDivider] + Position[selectDivider+2]) / 2.0f;
			}		
		}
		update();
		createColourTable();
	}
	else if(mouseDrag)
		selectDivider=dividerCollision(e->x());
}

int ValueSlider::dividerCollision(int p)
{
	if(blendDisplay)
	{
		//for(int i=0;i<Color.size()-1;i++)
		for(int i = 0; i * 2 + 1 < Position.size() - 1; i++)
		{
			if(abs(p-Position[i*2+1]*width())<6)
				return i*2+1;
		}
		return 0;
	}
	else
	{
		for(int i=1;i<Color.size()-1;i++)
		{
			if(abs(p-Position[i]*width())<6)
				return i;
		}
		return 0;
	}
}

void ValueSlider::mousePressEvent(QMouseEvent *e)
{
	mouseDrag=true;
}

void ValueSlider::mouseReleaseEvent(QMouseEvent *e)
{
	mouseDrag=false;
	selectDivider=0;
}

void ValueSlider::mouseDoubleClickEvent(QMouseEvent *e)
{
	
}





