
#include <QtGui>
#include "RefineDialog.h"
#include "../fits/FitsFileHandler.h"
#include "../fits/FitsImageHeader.h"

RefineDialog::RefineDialog( QMainWindow * parent) : QDialog (parent){

	setWindowTitle(QString::fromUtf8("Output Resolution"));

	init ();

	setFixedSize(this->sizeHint());

	fileWidth = -1;
	fileHeight = -1;
	fileDepth = -1;

	connect(radioHighRes, SIGNAL(clicked(bool)), this, SLOT(setHighRes()));
	connect(radioMediumRes, SIGNAL(clicked(bool)), this, SLOT(setMediumRes()));
	connect(radioLowRes, SIGNAL(clicked(bool)), this, SLOT(setLowRes()));

	connect(this, SIGNAL(resChangeW(int)), spinWidth, SLOT(setValue(int)));
	connect(this, SIGNAL(resChangeH(int)), spinHeight, SLOT(setValue(int)));
	connect(this, SIGNAL(resChangeD(int)), spinDepth, SLOT(setValue(int)));

	connect(cancelButton, SIGNAL(released()), this, SLOT(hide()));
	connect(acceptButton, SIGNAL(released()), this, SLOT(accept()));
}

RefineDialog::~RefineDialog() {
	

}

void RefineDialog::setFileDimensions (int w, int h, int d){
	
	fileWidth = w;
	fileHeight = h;
	fileDepth = d;

	spinWidth->setMaximum(w);
	spinHeight->setMaximum(h);
	spinDepth->setMaximum(d);

	spinScatterReads->setValue(1);

	WidthValue->setText(QApplication::translate("Astrovis", QString::number(w).toStdString ().c_str(), 0, QApplication::UnicodeUTF8));
	HeightValue->setText(QApplication::translate("Astrovis", QString::number(h).toStdString ().c_str(), 0, QApplication::UnicodeUTF8));
	DepthValue->setText(QApplication::translate("Astrovis", QString::number(d).toStdString ().c_str(), 0, QApplication::UnicodeUTF8));
}

void RefineDialog::setHighRes (){
	
	int width = fileWidth;
	int height = fileHeight;

	while(width > 512 || height > 512){
		width /= 2;
		height /= 2;
	}

	emit resChangeW (width);
	emit resChangeH (height);
	emit resChangeD (fileDepth);

}

void RefineDialog::setMediumRes (){
	int width = fileWidth;
	int height = fileHeight;

	while(width > 256|| height > 256){
		width /= 2;
		height /= 2;
	}

	emit resChangeW (width);
	emit resChangeH (height);
	emit resChangeD (fileDepth);
}

void RefineDialog::setLowRes (){
	int width = fileWidth;
	int height = fileHeight;

	while(width > 128 || height > 128){
		width /= 2;
		height /= 2;
	}

	emit resChangeW (width);
	emit resChangeH (height);
	emit resChangeD (fileDepth);
}

void RefineDialog::setFitsFileHandler (QTreeWidgetItem* i){

	item = i;

	if(i->type() == 1002){
        FitsImageHeader * f = (FitsImageHeader*)(i);
        ffh = f->getParent();
    }
    else if (i->type() == 1003){
    	emit refine (item, -1, -1, -1, -1);
    	return;
    }
    else {
    	return;
    }


    setFileDimensions(ffh->width(), ffh->height(), ffh->depth());

    setMediumRes();

    radioMediumRes->toggle();

    this->show();
}

void RefineDialog::setFitsFileHandler (QTreeWidgetItem* i, int w, int h, int d){

	item = i;

    setFileDimensions(w, h, d);

    setMediumRes();

    radioMediumRes->toggle();

    this->show();
}

void RefineDialog::hide () {
	
	this->setVisible(false);
}

void RefineDialog::accept (){
	
	emit refine (item, spinWidth->value(), spinHeight->value(), spinDepth->value(), spinScatterReads->value());

	this->setVisible(false);
}