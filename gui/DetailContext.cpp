#include "DetailContext.h"

DetailContext::DetailContext(FitsImageHeader * data):Context(data,1004)
{
	fileName=data->getParent()->filename;
	index=data->getIndex();
	this->setHidden(true);
	imageItem=data;
	fitsFile=data->getParent();

	downsampler = new Downsampler (fitsFile,index);

	initValues();
}

DetailContext::DetailContext(FitsFileHandler * data,int n):Context(NULL,1004)
{
	downsampler = new Downsampler (data,n);

	fitsFile=data;
	//fitsFile->changeHeaderIndex(0);

	fileName=data->filename;
	index=n;

	initValues();	
}

void DetailContext::initValues()
{
	displayCube=NULL;
	datawidth=downsampler->cube->width();
	dataheight=downsampler->cube->height();
	datadepth=downsampler->cube->depth();

	xbound = ybound = zbound = 0;
	wbound = datawidth;
	hbound = dataheight;
	dbound = datadepth;

	orientation=-1;
	setOrientation(0);
	sliceposition=0;
	pShift[0].setX(0);pShift[0].setY(0);
	pShift[1].setX(0);pShift[1].setY(0);
	pShift[2].setX(0);pShift[2].setY(0);



	setPosition(0.0f,0.0f);
	reverse=1;

	zoomFactor=1.0f;
}

void DetailContext::setBounds(int x,int y, int z,int w, int h, int d)
{
	xbound=x;
	ybound=y;
	zbound=z;
	wbound=w;
	hbound=h;
	dbound=d;
}

void DetailContext::setPosition(float x, float y)
{
    xposition = std::min(texturewidth/2.0f, std::max(x , -texturewidth/2.0f ));
    yposition = std::min(textureheight/2.0f , std::max(y , -textureheight/2.0f ));
}

//Return 1 if change detected, otherwise 0
int DetailContext::setSlicePosition(int n)
{
	int newslice = 0;
	switch(orientation)
	{
		case 0:
		newslice=std::min(std::max(n,0-pShift[2].x()), dbound-1-pShift[2].y());
		break;
		case 1:
		newslice=std::min(std::max(n,0-pShift[0].x()), datawidth-1-pShift[0].y());
		break;
		case 2:
		newslice=std::min(std::max(n,0-pShift[1].x()), dataheight-1-pShift[1].y());
		break;
	}	
	if(sliceposition==newslice)
		return 0;
	else
	{
		sliceposition=newslice;
		return 1;
	}
}
//Return 1 if change detected, otherwise 0
int DetailContext::setOrientation(int n)
{
	if(orientation==n)
		return 0;
	else
	{
		orientation=n;
		setTextureDim();
		return 1;
	}
	
}

void DetailContext::setTextureDim()
{
	switch(orientation)
	{
		case 0:
			texturewidth=wbound+pShift[0].x()-pShift[0].y();
			textureheight=hbound+pShift[1].x()-pShift[1].y();
		break;
		case 1:
			texturewidth=dbound+pShift[2].x()-pShift[2].y();
			textureheight=hbound+pShift[1].x()-pShift[1].y();
		break;
		case 2:
			texturewidth=wbound+pShift[0].x()-pShift[0].y();
			textureheight=dbound+pShift[2].x()-pShift[2].y();
		break;
	}
}

void DetailContext::setShift(int plane,int side, int shift)
{
    if(side==0)
    {
        pShift[plane].setX(shift);
    }
    else
    {
        pShift[plane].setY(shift);
    }
    //dprintf("%d %d\n", pShift[0].x(),pShift[0].y());
    //dprintf("%d %d\n", pShift[1].x(),pShift[1].y());
    setTextureDim();
}

void DetailContext::setReverse(int i)
{
	/*if(i==0)
	reverse=1;
	else
	reverse=-1;*/
}

void DetailContext::getTexCoords(double * t)
{
	switch(orientation)
	{
		case 0:
			t[0]=0.0;t[1]=0.0;t[2]=0.5;
			t[3]=reverse*1.0;t[4]=0.0;t[5]=0.5;
			t[6]=reverse*1.0;t[7]=1.0;t[8]=0.5;
			t[9]=0.0;t[10]=1.0;t[11]=0.5;
		break;
		case 1:
			t[0]=0.5;t[1]=0.0;t[2]=0.0;
			t[3]=0.5;t[4]=0.0;t[5]=reverse*1.0;
			t[6]=0.5;t[7]=1.0;t[8]=reverse*1.0;
			t[9]=0.5;t[10]=1.0;t[11]=0.0;
		break;
		case 2:
			t[0]=0.0;t[1]=0.5;t[2]=0.0;
			t[3]=reverse*1.0;t[4]=0.5;t[5]=0.0;
			t[6]=reverse*1.0;t[7]=0.5;t[8]=1.0;
			t[9]=0.0;t[10]=0.5;t[11]=1.0;
		break;
	}
}

int DetailContext::getDimension(int n)
{
	switch(n)
	{
		case 0:
			return datawidth+pShift[0].x()-pShift[0].y();
		break;
		case 1:
			return dataheight+pShift[1].x()-pShift[1].y();
		break;
		case 2:
			return datadepth+pShift[2].x()-pShift[2].y();
		break;
	}
	return 0;
}

int DetailContext::getDataSizeByPlane(int p)
{
	switch(p)
	{
		case 0:
			return datawidth;
		break;
		case 1:
			return dataheight;
		break;
		case 2:
			return datadepth;
		break;
	}
	return -1;
}

float DetailContext::getValue(int x,int y)
{
	if(displayCube)
	{
		switch(orientation)
		{
			case 0:
				return displayCube->getFloats()[y*displayCube->width+x];
			break;
			case 1:
				return displayCube->getFloats()[y+x*displayCube->height];
			break;
			case 2:
				return displayCube->getFloats()[x+y*displayCube->width];
			break;
		}
		return 0.f;
	}
	else
	{
		return 0.f;
	}
	
}

Datacube * DetailContext::getCube()
{
	return displayCube;
}

void DetailContext::setCube(Datacube* dc)
{
	if(dc)
	{
		if(displayCube)
		{
			boost::lock_guard <boost::mutex> flg (displayCubeMutex);
			delete displayCube;
			displayCube=NULL;
		}
		displayCube = dc;
		minValue=dc->min;
		maxValue=dc->max;
	}	
}

void DetailContext::insertData(Datacube * dc, int i)
{
	for(int x=0;x<dc->width*dc->height*dc->depth;x++)
	{
		float temp = dc->getFloats()[x];
		{
			boost::lock_guard <boost::mutex> flg (displayCubeMutex);
			displayCube->getFloats()[(i-1)*displayCube->width*displayCube->height+x] = temp;
		}
		minValue = std::min(minValue, dc->getFloats()[x]);
		maxValue= std::max(maxValue, dc->getFloats()[x]);
	}
	delete dc;
    dc=NULL; 
}

int DetailContext::startDownsampler()
{
	fitsFile->regionAbort ();
	setTextureDim();

	int x = 0;
	int y = 0;
	int z = 0;
	int w = 0;
	int h = 0;
	int d = 0;
	switch(orientation)
	{
		case 0:
			x = xbound - pShift[0].x();
			y = ybound + pShift[1].y();
			z = zbound + sliceposition;
			w = wbound + pShift[0].x() - pShift[0].y();
			h = hbound + pShift[1].x() - pShift[1].y();
			d = 1;
		break;
		case 1:
			x = xbound + sliceposition; 
			y = ybound + pShift[1].y(); 
			z = zbound - pShift[2].x();
			w = 1; 
			h = hbound + pShift[1].x() - pShift[1].y(); 
			d = dbound + pShift[2].x() - pShift[2].y();
		break;
		case 2:
			x = xbound - pShift[0].x();
			y = ybound + sliceposition;
			z = zbound - pShift[2].x();
			w = wbound + pShift[0].x() - pShift[0].y(); 
			h = 1; 
			d = dbound + pShift[2].x() - pShift[2].y();
		break;
	}

	Datacube * dc = new Datacube(w,h,d);
	setCube(dc);

	fitsFile->readRegion(x,y,z,w,h,d);

	return 0;
}

void DetailContext::stopDownsampler()
{
	downsampler->StopDownsampling();
}

void DetailContext::pauseDownsampler()
{
	
}

void DetailContext::resumeDownsampler()
{
	
}

void DetailContext::waitDownsampler()
{
	
}

FitsFileHandler* DetailContext::getFileHandler()
{
	return fitsFile;
}

Downsampler * DetailContext::getDownsampler()
{
	return downsampler;
}

Datacube * DetailContext::getTextureData()
{
	return downsampler->GetLatestResult();
}

DetailContext::~DetailContext()
{
	
}