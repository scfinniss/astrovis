#include "Context.h"
#include <QtGui>

Context::Context(QTreeWidgetItem * parent, int id):QTreeWidgetItem(parent, id)
{
	if(parent!=NULL)
		parent->addChild(this);
}

bool Context::compare(char* f,int i)
{
	if(strcmp(fileName,f))
		return false;
	if(index!=i)
		return false;
	return true;
}

Context::~Context()
{
	
}
