#ifndef VOLRENDER_H
#define VOLRENDER_H

#define GL_GLEXT_PROTOTYPES

#include <float.h>
#include <GL/glew.h>

#ifdef __APPLE__
#include <OpenGL/glext.h>
#include <OpenGL/glu.h>
#else
#include <GL/glext.h>
#include <GL/glu.h>
#endif

#include "VolumeContext.h"
#include "InfoBox.h"
#include "../fits/FitsFileHandler.h"
#include "../fits/FitsImageHeader.h"
#include "../fits/FitsTreeWidget.h"
#include "ShaderObject.h"

#define BUFSIZE 512

const double Pi = 3.14159265358979323846;

enum {LEFTCLICK,RIGHTCLICK,LEFTRELEASE,RIGHTRELEASE,LEFTDOUBLE,RIGHTDOUBLE,MOVE,WHEEL,KEY};


struct inputEvent
{
    int Type;
    QPoint Position;
    QPoint Displacement;
};

class VolRender : public QGLWidget
{
	Q_OBJECT

	public:
		VolRender(QMainWindow * parent, FitsTreeWidget * tree);
		VolRender(QMainWindow * parent, FitsTreeWidget * tree, ShaderObject* shader);
		~VolRender();
		//Detail window content change trigger
		QAction * viewMove;

		//Review these
		void setTex (int);
		void initTex();
		void newTex();
		void subTex(int);
		float* extractVolumeSlice(int,int,int);
		void setTransferTexture(float*, int,int);

		void connectInfoBox(InfoBox*);
		void setInfoLabels();
		
		float blendSetting;
		void leftDoubleClick(inputEvent*);

	public slots:
		//Menu Actions
		void toggleFrames();
		void toggleTransparent();

		void fitsImageHeaderLoadRequest(QTreeWidgetItem* i, int,int,int,int);
		void refineVolumeRequest(QTreeWidgetItem*,int,int,int,int);
		void DownsamplerComplete(double);
		void pauseDownsampler();
		void resumeDownsampler();
		void setTransferMap(float**,int*,int);
		void selectorDimensionChange(int w,int h);
		void selectorMove(double x,double y,double z);

		void refineMenuTrigger();

		void moveSelector(float,float,float);

	signals:
		void selectorPlaced(int orientation, int slice, double x,double y);
		void planeShifted(int,int,double);

		void refineDialog(QTreeWidgetItem*,int,int,int);

		void newContextActivated(VolumeContext*);
	

        
	protected:
		//OpenGL Functions
		void initializeGL();
		void resizeGL(int w, int h);

		void paintGL();
		//Draw Calls
		void clearProjection();
		void PerspectiveTransform();
		void clearModelView();
		void draw3D();
		void applyRotation();
		void drawVolume();
		void drawFrame(double,double,double,int,QPoint);
		void drawSelectionFrame();
		void drawSelector();
		void resizeSelector();

		double* translateCoordinate(double *&,double ,double ,double ,double ,double ,double);
		double* translateCoordinate(double *&v,Coordinate c, double ,double );
		Coordinate addCoordinate(Coordinate,Coordinate,bool);
		Coordinate mulCoordinate(Coordinate,double);

		void OrthoTransform();
		void draw2D();
		void drawHUD(bool);
		void drawProgress();

		void updateGL();
		void updateInput();
		void updateProgress();

		void HUDActions();
		int HUDHitDetection(int,int);
		int volumeHitDetection();
		void initSelectMode(int,int);
		int endSelectMode();

		void volumeEdgeCollision(Coordinate c);
		void volumeEdgeDrag(int,int);

		void shiftPlane(int ,int,int);

		void changeCircularRotation(float);
		void changeVerticalRotation(float);

		Coordinate volumeIntersection();

		int findNameHit (GLint hits, GLuint buffer[]);
		Coordinate planeIntersection(int);
		double intersectionCalculation(double *,double *,double *, double *);
		double dotProduct(double*,double*);

		void volumeSelectRender();
		void drawFace(int,bool);
		void drawPrimitive(GLenum primitive, int vertexCount,float* points,float* texCoords,bool reverse);

		//Event Functions
		void keyPressEvent( QKeyEvent *e );

		void mousePressEvent ( QMouseEvent * e );
		void leftPress(inputEvent*);
		void rightPress(inputEvent*);

		void mouseReleaseEvent( QMouseEvent * e );
		void leftRelease(inputEvent* e);
		void rightRelease(inputEvent* e);

		void mouseDoubleClickEvent ( QMouseEvent * e);
		
		void rightDoubleClick(inputEvent*);

		void mouseMoveEvent ( QMouseEvent * e );
		void mouseDrag(inputEvent*);
		void leftMouseDrag(inputEvent*);
		void rightMouseDrag(inputEvent*);

		void wheelEvent ( QWheelEvent * e );
		void wheelScroll(inputEvent*);

		void enterEvent(QEvent * e);
		void leaveEvent(QEvent * e);

		void updateDisplay();
		void progressUpdate();
		void timerEvent(QTimerEvent *e);

		//Display Management Calls
		void setClip();
		void clipToggle(bool);

		void activateNewContext(VolumeContext* c);
		void activateExistingContext(VolumeContext* c);

		void setSelectorSize();
		void setSelectorPosition(Coordinate);

	private:

		bool contains(int,int,int,int,int,int);

		void InitializeValues();
		void createDefaultContext();
		void setRenderContext(VolumeContext *);
		void createShaders(QString vshader, QString fshader);

		std::vector<VolumeContext*> ContextList;

		int calculateNearPlane(float,float);
		
		//Menu Actions

		//Camera variables
		int zoomAmount;
		float circular,vertical;
		float maxZoom,minZoom;

		//Mouse management variables
		QPoint mousePoint;
		bool leftDown;
		bool rightDown;
		bool edgeDrag;

		//User input even queue
		std::queue<inputEvent*> inputQueue;

		//Plane Shift Variables
		QPoint planeShift [3];

		inputEvent dragDisplacement;

		//Vertical Rotation
		float rotationMatrix[16];

		//Transform Matrices
		double model[16], proj[16] ;
    	int view[4];

    	//
    	int HUDElement;

    	//Display Variables
    	int nearPlane;
    	int selectPlane;
    	QPoint selectedEdge;
    	int displayTimerID;
    	int progressTimerID;
    	bool frameVisible;

    	bool selectorDisplayVisible;

    	bool displayHasChanged;

		//Shader Variables
		//QGLShader *fragmentShader,*vertexShader;
		//QGLShaderProgram *shaderProgram;

		//ShaderObject
		ShaderObject *shader;

		//Info Labels
		QLabel* imagewidth,*imageheight,*imagedepth;
		QLabel* widthcov,*heightcov,*depthcov;

		//Texture reference
		GLuint planeTexture;

		GLuint colourMap[4];
		float * rgbaMap;

		//Progress Draw
		float progressOpacity;
		int progressCount;


		//Default Context display
		VolumeContext * defaultContext;

		//Rendered Context
		VolumeContext * renderContext;

		//Min and max depths of view
		double zmin,zmax;

		GLuint selectBuffer[BUFSIZE];

		int transferSize;

		bool resultWaiting;
};

#endif
