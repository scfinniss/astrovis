#include "DetailRender.h"

DetailRender::DetailRender(QMainWindow *parent, FitsTreeWidget * tree,QGridLayout *DetailViewLayout, ShaderObject* s)
:QGLWidget(QGLFormat(QGL::SampleBuffers) , parent)
{
    createFrame(DetailViewLayout);
    shader=s;
	connect(tree,SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(fitsImageHeaderLoadRequest(QTreeWidgetItem*)));
    InitializeValues();

}

DetailRender::DetailRender(QMainWindow *parent, FitsTreeWidget * tree, VolRender * volume)
:QGLWidget(QGLFormat(QGL::SampleBuffers) , parent)
{
    //addButtons();
    connect(tree,SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(fitsImageHeaderLoadRequest(QTreeWidgetItem*)));
    InitializeValues();

    volumeConnect(volume);
}

DetailRender::DetailRender(QMainWindow *parent, FitsTreeWidget * tree, VolRender * volume, QGridLayout *DetailViewLayout)
:QGLWidget(QGLFormat(QGL::SampleBuffers) , parent) {
    connect(tree,SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(fitsImageHeaderLoadRequest(QTreeWidgetItem*)));

    createFrame(DetailViewLayout);
    InitializeValues();

    volumeConnect(volume);
}

DetailRender::DetailRender(QMainWindow *parent, FitsTreeWidget * tree, VolRender * volume, QGridLayout *DetailViewLayout, ShaderObject* s)
:QGLWidget(QGLFormat(QGL::SampleBuffers) , parent) {
    connect(tree,SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(fitsImageHeaderLoadRequest(QTreeWidgetItem*)));
    shader=s;

    createFrame(DetailViewLayout);

    InitializeValues();

    volumeConnect(volume);
}

void DetailRender::createFrame(QGridLayout *DetailViewLayout)
{
    detailLayout = new QBoxLayout (QBoxLayout::TopToBottom, this);
    detailFrame = new QFrame (this);


    detailFrame->setLayout (detailLayout);
    DetailViewLayout->addWidget (detailFrame);
    detailLayout->addWidget (this);
    detailLayout->setSpacing(0);
    detailLayout->setContentsMargins(0, 0, 0, 0);
    addButtons();
}

void DetailRender::volumeConnect(VolRender * volume)
{
    //Connect DetailRender resize event to VolumeRender resize selector
    connect(this,SIGNAL(windowResize(int,int)), volume, SLOT(selectorDimensionChange(int,int)));
    //Connect VolRender selector placed event to DetailRender placefocus 
    connect(volume,SIGNAL(selectorPlaced(int,int,double,double)), this, SLOT(placeFocus(int,int,double,double)));
    //Connect VolRender plane shifted event to DetailRender shiftPlane
    connect(volume,SIGNAL(planeShifted(int,int,double)), this, SLOT(shiftPlane(int,int,double)));
    //Connect DetailRender selector moved to VolRender moveSelector
    connect(this,SIGNAL(selectorMoved(float,float,float)), volume, SLOT(moveSelector(float,float,float)));
    //Connect newcontextactivated  to new volumecontext
    connect(volume,SIGNAL(newContextActivated(VolumeContext*)), this, SLOT(newVolumeContext(VolumeContext*)));
    //Connect pause and resume volume downsampler
    connect(this,SIGNAL(pauseVolume()), volume, SLOT(pauseDownsampler()));
    connect(this,SIGNAL(resumeVolume()), volume, SLOT(resumeDownsampler()));
}

void DetailRender::initializeGL()
{
	dprintf ("GL Init.\n");
    makeCurrent();
    glewInit();
    glClearColor(0.25f, 0.266f, 0.301f, 0.0f);

    glEnable(GL_MULTISAMPLE);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0,width(),0,height(),-1.0,1.0);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();   
}

void DetailRender::resizeGL(int w,int h)
{
	makeCurrent();

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,w,0,h,-1.0,1.0);

    moveButtons();

    emit(windowResize(w/renderContext->zoomFactor,h/renderContext->zoomFactor));
}

void DetailRender::addButtons()
{
    //QPalette* p = new QPalette(Qt::white);

    stepForward =new QPushButton("+", this);
    stepForward->setFixedWidth (24);
    stepBackward =new QPushButton("-", this);
    stepBackward->setFixedWidth (24);
    sliceLabel = new QLabel("/ 0",this);
    slicePositionEdit = new QLineEdit("1", this);
    slicePositionEdit->setFixedWidth (100);
    
    //setFixedHeight

    //edit->setFrame(false);
    //slicePositionEdit->setPalette(*p);
    //slicePositionEdit->setAutoFillBackground(true);
    //slicePositionEdit->setFixedSize(50,25);
    //slicePositionEdit->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

    //sliceLabel->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    //sliceLabel->setFixedSize(100,40);
    //sliceLabel->setBuddy(slicePositionEdit);
    //sliceLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    //sliceLabel->setPalette(*p);
    //sliceLabel->setAutoFillBackground(true);

    connect(stepForward, SIGNAL(clicked()), this, SLOT(incrementSlicePosition()));
    connect(stepBackward, SIGNAL(clicked()), this, SLOT(decrementSlicePosition()));


   
    orientationMenu = new QMenu(this);
    QAction * front = new QAction("Front", orientationMenu);
    QAction * side = new QAction("Side", orientationMenu);
    QAction * top = new QAction("Top", orientationMenu);
    orientationMenu->addAction(front);
    connect(front, SIGNAL(triggered()), this, SLOT(setFrontOrientation()));
    orientationMenu->addAction(side);
    connect(side, SIGNAL(triggered()), this, SLOT(setSideOrientation()));
    orientationMenu->addAction(top);
    connect(top, SIGNAL(triggered()), this, SLOT(setTopOrientation()));

    moveButtons();

    QFrame *ButtonFrame = new QFrame (this);
    //ButtonFrame->setFrameShape(QFrame::StyledPanel);
    //ButtonFrame->setFrameShadow(QFrame::Raised);
    
    ButtonFrame->setAutoFillBackground(true);
    detailLayout->addWidget (ButtonFrame);
    QBoxLayout* buttLayout = new QBoxLayout (QBoxLayout::LeftToRight, ButtonFrame);
    buttLayout->addWidget (stepBackward, 1);
    buttLayout->addWidget (stepForward, 1);
    buttLayout->addWidget (slicePositionEdit, 1);
    buttLayout->addWidget (sliceLabel, 1);
    buttLayout->setSpacing(1);
    buttLayout->setContentsMargins(1, 1, 1, 1);
    ButtonFrame->setFixedHeight (56);
    //ButtonFrame->setFixedWidth (200);
    ButtonFrame->setLayout (buttLayout);
}

void DetailRender::moveButtons()
{
    //stepForward->move(width()*0.25,height()*0.9);
    //stepBackward->move(width()*0.50,height()*0.9);
    //sliceLabel->move(width()*0.75,height()*0.9);
}

void DetailRender::setSliceLabel(int n)
{
    QString str; 
    sliceLabel->setText(QString("/ ") + str.setNum(n));
}

void DetailRender::incrementSlicePosition()
{
    if(setSlicePosition(renderContext->sliceposition+1))
        renderContext->startDownsampler();
}

void DetailRender::decrementSlicePosition()
{
    if(setSlicePosition(renderContext->sliceposition-1))
        renderContext->startDownsampler();
}

//Return 1 if change detected, otherwise 0
int DetailRender::setSlicePosition(int n)
{   
    QString str;
    int offset=0;
    int limit=0;
    switch(renderContext->orientation)
    {
        case 0:
            offset=renderContext->zbound;
            limit=renderContext->dbound;
        break;
        case 1:
            offset=renderContext->xbound;
            limit=renderContext->wbound;
        break;
        case 2:
            offset=renderContext->ybound;
            limit=renderContext->hbound;
        break;
    }
    int sliceNum = std::min(std::max(n,0),limit-1);
    slicePositionEdit->setText(str.setNum(sliceNum+offset+1));

    updateDisplay();

    return renderContext->setSlicePosition(n);
}
//Return 1 if change detected, otherwise 0
int DetailRender::setSliceOrientation(int n)
{
    switch(n)
    {
        case 0:
            setSliceLabel(renderContext->datadepth);
        break;
        case 1:
            setSliceLabel(renderContext->datawidth);
        break;
        case 2:
            setSliceLabel(renderContext->dataheight);
        break;
    }

    QString str; 
    slicePositionEdit->setText(str.setNum(1));

    centreView();

    updateDisplay();

    return renderContext->setOrientation(n);
}

void DetailRender::setTopOrientation()
{
    if(setSliceOrientation(2))
        renderContext->startDownsampler();
    
}
void DetailRender::setFrontOrientation()
{
    if(setSliceOrientation(0));
        renderContext->startDownsampler();
}
void DetailRender::setSideOrientation()
{
    if(setSliceOrientation(1));
        renderContext->startDownsampler();
}

void DetailRender::placeFocus(int orientation,int slice,double x, double y)
{
    int check=setSliceOrientation(orientation/2)+setSlicePosition(slice);
    if(check)
        renderContext->startDownsampler();
    renderContext->setReverse(orientation%2);
    renderContext->setPosition(renderContext->reverse*x,-y);
    
    updateDisplay();
}

//move select plane shift units
void DetailRender::shiftPlane(int plane,int side,double shift)
{
    //dprintf("%d %d %f\n", plane,side,shift);
    
    //renderContext->setShift(plane,side,(int)(shift*renderContext->getDataSizeByPlane(plane)));

    //update();
}

void DetailRender::setTransferMap(float** tex,int * rgba, int t)
{
    int count=0;
    for(int i=0;i<4; i++)
    {
        if(rgba[i])
        {
            setTransferTexture(tex[count], i, t);
            count++;
        }
            
    }  
    updateDisplay();
}

void DetailRender::centreView()
{
    renderContext->setPosition(0.0f,0.0f);
}

//
void DetailRender::setTransferTexture(float* tex, int c,int t)
{
    makeCurrent();
    //glDeleteTextures(1, &colourMap[c]);
    glGenTextures(1, &colourMap[c]);
    glActiveTextureARB( GL_TEXTURE1+c);
   
    glBindTexture(GL_TEXTURE_1D, colourMap[c]);

    glTexImage1D(GL_TEXTURE_1D, 0, GL_INTENSITY, t, 0, GL_RED, GL_FLOAT, tex);

    glTexParameteri( GL_TEXTURE_1D,GL_TEXTURE_WRAP_S, GL_CLAMP);
    
    glTexParameteri( GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    shader->shaderProgram->bind();

    switch(c)
    {
        case 0:
            shader->shaderProgram->setUniformValue("redLUT",  1+c );
        break;
        case 1:
            shader->shaderProgram->setUniformValue("greenLUT",  1+c );
        break;
        case 2:
            shader->shaderProgram->setUniformValue("blueLUT",  1+c );
        break;
        case 3:
            shader->shaderProgram->setUniformValue("alphaLUT",  1+c );
        break;
    }
    
    shader->shaderProgram->release();
    glActiveTextureARB(GL_TEXTURE0);
    updateDisplay();
}

//Set Render Texture
void DetailRender::setTex () 
{
    makeCurrent();

    float* texture;

    float scale = renderContext->maxValue - renderContext->minValue;

    texPiecesW = (renderContext->texturewidth/texMaxSize+1);
    texPiecesH = (renderContext->textureheight/texMaxSize+1);

    texturePieces = new GLuint[texPiecesW*texPiecesH];
    for(int y=0;y<texPiecesH;y++)
    {
        for(int x=0;x<texPiecesW;x++)
        {
            int width = std::min(texMaxSize,renderContext->texturewidth-texMaxSize*x);
            int height = std::min(texMaxSize,renderContext->textureheight-texMaxSize*y);

            //dprintf("%d %d\n", width,height);

            texture = new float[width*height];
            for(int j = 0;j<height;j++)
            {
                for(int i = 0;i<width;i++)
                {
                    texture [i+j*width] = (renderContext->getCube()->getFloats()[(i + j*renderContext->texturewidth) + (x*texMaxSize) + (y*texMaxSize*renderContext->texturewidth)]- renderContext->minValue) / scale;
                }
            }
            glEnable (GL_TEXTURE_3D);

            glGenTextures(1, &texturePieces[y*texPiecesW+x]);

            glActiveTextureARB(GL_TEXTURE5);

            glBindTexture(GL_TEXTURE_3D, texturePieces[y*texPiecesW+x]);

            switch(renderContext->orientation)
            {
                case 0:
                    glTexImage3D(GL_TEXTURE_3D, 0, GL_INTENSITY, width,height, 1, 0, GL_RED, GL_FLOAT, texture);
                break;
                case 1:
                    glTexImage3D(GL_TEXTURE_3D, 0, GL_INTENSITY, 1,height, width, 0, GL_RED, GL_FLOAT, texture);
                break;
                case 2:
                    glTexImage3D(GL_TEXTURE_3D, 0, GL_INTENSITY, width, 1, height, 0, GL_RED, GL_FLOAT, texture);
                break;
            }

            glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_R, GL_REPEAT );
            glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_S, GL_REPEAT );
            glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_T, GL_REPEAT );
            
            glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_MIN_FILTER, GL_NEAREST );
            glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_MAG_FILTER, GL_NEAREST );

            shader->shaderProgram->bind();
            shader->shaderProgram->setUniformValue("texture", 5);
            shader->shaderProgram->release();

            glDisable (GL_TEXTURE_3D);

            delete [] texture;
            texture=NULL;
        }
    }
}

void DetailRender::paintGL()
{
    updateInput();
    if(displayHasChanged)
    {
        //dprintf("Update Called %d\n", time(0));
        drawDisplay();
        displayHasChanged=false;
    }
}

void DetailRender::drawDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity(); 


    glPushMatrix();
    glScalef(renderContext->zoomFactor,renderContext->zoomFactor,1.0f);

    glTranslated(-renderContext->texturewidth/2.0,
                    -renderContext->textureheight/2.0,
                        0);

    glTranslated(width()/2.0/renderContext->zoomFactor,
                    height()/2.0/renderContext->zoomFactor,
                        0);

    glTranslated(renderContext->xposition,
                    renderContext->yposition,
                        0);
    
    shader->shaderProgram->bind(); //enable shader

    double texCoords [12];
    renderContext->getTexCoords(texCoords);

    if (texturePieces != NULL)
        for(int y=0;y<texPiecesH;y++)
        {
            for(int x=0;x<texPiecesW;x++)
            {
                glBindTexture(GL_TEXTURE_3D, texturePieces[y*texPiecesW+x]);
                int width = std::min(texMaxSize,renderContext->texturewidth-texMaxSize*x);
                int height = std::min(texMaxSize,renderContext->textureheight-texMaxSize*y);

                glPushMatrix();

                glTranslated(x*texMaxSize, y*texMaxSize, 0);

                glBegin(GL_QUADS);

                glTexCoord3d(texCoords[0], texCoords[1], texCoords[2]);
                glVertex2d(0,0);

                glTexCoord3d(texCoords[3], texCoords[4], texCoords[5]);
                glVertex2d(width,0);

                glTexCoord3d(texCoords[6], texCoords[7], texCoords[8]);
                glVertex2d(width,height);

                glTexCoord3d(texCoords[9], texCoords[10], texCoords[11]);
                glVertex2d(0,height);

                glEnd();

                glPopMatrix();
            }
        }
    shader->shaderProgram->release(); //disable shader
    glPopMatrix();
    glActiveTextureARB(GL_TEXTURE0);
    glDisable(GL_TEXTURE_3D);
    drawDisplayInfo();

    drawLabel();
    glActiveTextureARB(GL_TEXTURE5);
}

void DetailRender::drawDisplayInfo()
{
    glPointSize(10);
    glColor4f(1.0,0.0,0.0,1.0);
    glBegin(GL_POINTS);
    glVertex2d(width()/2.0,height()/2.0);
    glEnd();

    glColor4f(0.0,0.0,0.0,1.0);
    renderText(10.0,height()-20.0,0.0,viewString[renderContext->orientation],f,1);
    f.setPointSize(10);
    QString str;
    str.setNum(renderContext->zoomFactor).truncate(4);
    str.append("x");
    renderText(width()-100,height()-20.0,0.0,str,f,1);
}

void DetailRender::drawLabel()
{
    float x=0.f;
    float y=0.f;
    float z=0.f;
    switch(renderContext->orientation)
    {
        case 0:
            x=((mousePoint.x()-width()/2.0+(renderContext->texturewidth)*renderContext->zoomFactor/2.0)/renderContext->zoomFactor-renderContext->xposition);
            y=((height()-mousePoint.y()-height()/2.0+(renderContext->textureheight)*renderContext->zoomFactor/2.0)/renderContext->zoomFactor-renderContext->yposition);
            z=renderContext->zbound+renderContext->sliceposition+1;
        break;
        case 1:
            z=((mousePoint.x()-width()/2.0+(renderContext->texturewidth)*renderContext->zoomFactor/2.0)/renderContext->zoomFactor-renderContext->xposition);
            y=((height()-mousePoint.y()-height()/2.0+(renderContext->textureheight)*renderContext->zoomFactor/2.0)/renderContext->zoomFactor-renderContext->yposition);
            x=renderContext->xbound+renderContext->sliceposition+1;
        break;
        case 2:
            x=((mousePoint.x()-width()/2.0+(renderContext->texturewidth)*renderContext->zoomFactor/2.0)/renderContext->zoomFactor-renderContext->xposition);
            z=((height()-mousePoint.y()-height()/2.0+(renderContext->textureheight)*renderContext->zoomFactor/2.0)/renderContext->zoomFactor-renderContext->yposition);
            y=renderContext->ybound+renderContext->sliceposition+1;
        break;
    }

    float widthPos=((mousePoint.x()-width()/2.0+(renderContext->texturewidth)*renderContext->zoomFactor/2.0)/renderContext->zoomFactor-renderContext->xposition);
    float heightPos=((height()-mousePoint.y()-height()/2.0+(renderContext->textureheight)*renderContext->zoomFactor/2.0)/renderContext->zoomFactor-renderContext->yposition);
    
    if(((widthPos>=0&&widthPos<=renderContext->texturewidth)&&(heightPos>=0&&heightPos<=renderContext->textureheight))&&leftDown)
    {
        glTranslated(mousePoint.x(),height()-mousePoint.y(),0.0);
        glColor4f(0.0,0.0,0.0,1.0);
        int boxw=150;int boxh=45;
        glBegin(GL_QUADS);
        glVertex2d(-2,-2);
        glVertex2d(boxw+2,-2);
        glVertex2d(boxw+2,boxh+2);
        glVertex2d(-2,boxh+2);
        glEnd();

        glColor4f(1.0,0.9,0.5,1.0);
        glBegin(GL_QUADS);
        glVertex2d(0,0);
        glVertex2d(boxw,0);
        glVertex2d(boxw,boxh);
        glVertex2d(0,boxh);
        glEnd();
        glColor4f(0.0,0.0,0.0,1.0);
        renderText(5.0,34,0.0,QString("x: ")+QString::number(x, 'f', 2 ),f,1);
        renderText(5.0,23,0.0,QString("y: ")+QString::number(y, 'f', 2 ),f,1);
        renderText(5.0,12,0.0,QString("z: ")+QString::number(z, 'f', 2 ),f,1);
        renderText(5.0,1.0,0.0,QString("value: ")+QString::number(renderContext->getValue((int)widthPos,(int)heightPos), 'f', 6 ),f,1);
    }
}

//Handle Input Queue
void DetailRender::updateInput()
{
    while(inputQueue.size()>0)
    {
        switch(inputQueue.front()->Type)
        {
            //Left Press
            case 0:
                leftPress(inputQueue.front());
            break;
            //Right Press
            case 1:
                rightPress(inputQueue.front());
            break;
            //Left Release
            case 2:
                leftRelease(inputQueue.front());
            break;
            //Right Release
            case 3:
                rightRelease(inputQueue.front());
            break;
            //Left Double Click
            case 4:
                leftDoubleClick(inputQueue.front());
            break;
            //Right Double Click
            case 5:
                rightDoubleClick(inputQueue.front());
            break;
            //Mouse Move
            case 6:
                mouseDrag(inputQueue.front());
                if(rightDown)
                    rightMouseDrag(inputQueue.front());
                else if(leftDown)
                    leftMouseDrag(inputQueue.front());
            break;
            //Mouse Wheel
            case 7:
                wheelScroll(inputQueue.front());
            break;
            //Keyboard Press
            case 8:

            break;
        }
        delete inputQueue.front();
        inputQueue.pop();
    }
}

//Mouse Press Event
void DetailRender::mousePressEvent ( QMouseEvent * e )
{
    mousePoint=QPoint(e->x(),e->y());
    inputEvent * event = new inputEvent;
    if(e->button()==Qt::LeftButton)
        event->Type=LEFTCLICK;
    else if(e->button()==Qt::RightButton)
        event->Type=RIGHTCLICK;
    event->Position=QPoint(mousePoint.x(),mousePoint.y());
    inputQueue.push(event);
}

//Mouse Press Action
//Left
void DetailRender::leftPress(inputEvent * e)
{
    //dprintf("Left Mouse Clicked\n");
    leftDown=true;
}
//Right
void DetailRender::rightPress(inputEvent * e)
{
    //dprintf("Right Mouse Clicked\n");
    rightDown=true;
}

//Mouse Release Event
void DetailRender::mouseReleaseEvent( QMouseEvent * e )
{
    mousePoint=QPoint(e->x(),e->y());
    inputEvent * event = new inputEvent;
    if(e->button()==Qt::LeftButton)
        event->Type=LEFTRELEASE;
    else if(e->button()==Qt::RightButton)
        event->Type=RIGHTRELEASE;
    event->Position=QPoint(mousePoint.x(),mousePoint.y());
    inputQueue.push(event);
}

//Mouse Release Action
//Left
void DetailRender::leftRelease(inputEvent * e)
{
    //dprintf("Left Mouse Released\n");
    leftDown=false;
    if(e->Position.x()<65&&e->Position.y()<25)
    {
        dprintf("menu triggered\n");
        orientationMenu->popup(this->mapToGlobal(e->Position));
        orientationMenu->setVisible(true);
        updateDisplay();
    }
        
}
//Right
void DetailRender::rightRelease(inputEvent * e)
{
    //dprintf("Right Mouse Released\n");
    rightDown=false;
}

//Double Click Event
void DetailRender::mouseDoubleClickEvent ( QMouseEvent * e )
{
    mousePoint=QPoint(e->x(),e->y());
    inputEvent * event = new inputEvent;
    if(e->button()==Qt::LeftButton)
        event->Type=LEFTDOUBLE;
    else if(e->button()==Qt::RightButton)
        event->Type=RIGHTDOUBLE;
    event->Position = QPoint(mousePoint.x(),mousePoint.y());
    inputQueue.push(event);
}

//Mouse Double Click Action
//Left
void DetailRender::leftDoubleClick(inputEvent * e)
{

}
//Right
void DetailRender::rightDoubleClick(inputEvent * e)
{
    //dprintf("Right Mouse Double Clicked\n");
}

//Mouse Move Event
//Left Click - Selection
//Right Click - Rotation-Movement
void DetailRender::mouseMoveEvent ( QMouseEvent * e )
{
    inputEvent * event = new inputEvent;
    event->Type=MOVE;
    event->Displacement = QPoint(mousePoint.x()-e->x(),mousePoint.y()-e->y());
    mousePoint=QPoint(e->x(),e->y());
    event->Position = QPoint(mousePoint.x(),mousePoint.y());
    inputQueue.push(event); 
}

//Mouse Move Action
//Plain Move
void DetailRender::mouseDrag(inputEvent * e)
{
    mousePoint = QPoint(e->Position.x(),e->Position.y());
}
//Left Drag
void DetailRender::leftMouseDrag(inputEvent * e)
{
    updateDisplay();
}
//Right Drag
void DetailRender::rightMouseDrag(inputEvent * e)
{
    renderContext->setPosition(renderContext->xposition-e->Displacement.x()/renderContext->zoomFactor,
                                    renderContext->yposition+e->Displacement.y()/renderContext->zoomFactor);
    int adjust=0;
    if(renderContext->reverse==-1)
        adjust=1;
    switch(renderContext->orientation)
    {
        case 0:
            emit(selectorMoved( renderContext->xposition/renderContext->wbound*renderContext->reverse,
                                    -renderContext->yposition/renderContext->hbound,
                                        (float)(renderContext->sliceposition+adjust)/(renderContext->dbound-1)-0.5));
        break;
        case 1:
            
            emit(selectorMoved((float)((renderContext->wbound-1)-renderContext->sliceposition+adjust)/(renderContext->wbound-1)-0.5,
                                    -renderContext->yposition/renderContext->hbound ,
                                         -renderContext->xposition/renderContext->dbound*renderContext->reverse));
        break;
        case 2:
            emit(selectorMoved( renderContext->xposition/renderContext->wbound*renderContext->reverse,
                                    (float)(renderContext->sliceposition+adjust)/(renderContext->hbound-1)-0.5,
                                        -renderContext->yposition/renderContext->dbound ));
        break;
    } 
    updateDisplay();
}


//Wheel Event
void DetailRender::wheelEvent ( QWheelEvent * e )
{
    inputEvent * event = new inputEvent;
    event->Type=WHEEL;
    event->Displacement = QPoint(e->delta(),0);
    inputQueue.push(event); 
}

//Mouse Wheel Scroll Action
void DetailRender::wheelScroll(inputEvent * e)
{
    if(e->Displacement.x()>0)
    {
        renderContext->zoomFactor=std::min(32.0f, renderContext->zoomFactor*1.2f);
    }
    else
    {
        renderContext->zoomFactor=std::max(0.125f, renderContext->zoomFactor/1.2f);
    }
    updateDisplay();
    emit(windowResize(width()/renderContext->zoomFactor,height()/renderContext->zoomFactor));
}

//Key Press Events
void DetailRender::keyPressEvent( QKeyEvent *e )
{

}

void DetailRender::enterEvent(QEvent *e)
{
    
    //timerID=startTimer(16);
    //setFocus();
}

void DetailRender::leaveEvent(QEvent *e)
{
    //killTimer(timerID);
}

void DetailRender::updateDisplay()
{
    displayHasChanged=true;
}

void DetailRender::timerEvent(QTimerEvent *e)
{
    if(e->timerId() == displayTimerID)
    {
        makeCurrent();
        update();
    }
}

void DetailRender::newVolumeContext(VolumeContext* v)
{
    DetailContext * context = new DetailContext(v->getImage());
    context->setBounds(v->xbound,v->ybound, v->zbound, v->wbound, v->hbound, v->dbound);  
    activateNewContext(context);
    v->setDetail(renderContext);
    updateDisplay();
}

void DetailRender::fitsImageHeaderLoadRequest(QTreeWidgetItem* i)
{
    if(i->type()==1001)
    {}
    else if(i->type()==1002)
    {
        /*FitsImageHeader * f = (FitsImageHeader*)(i);
        for(int i=0;i<ContextList.size();i++)
        {
            if(ContextList[i]->compare(f->getParent()->filename, f->getIndex()))
            {
                activateExistingContext(ContextList[i]);
                break;
            }
            else if(i==ContextList.size()-1)
            {
                DetailContext * context = new DetailContext(f);
                activateNewContext(context);
                break;
            }
        }*/
    }
    else if(i->type()==1003)
    {
        activateExistingContext(((VolumeContext*)(i))->getDetail());
    }
}

void DetailRender::activateNewContext(DetailContext* c)
{
    setRenderContext(c);
    ContextList.push_back(c);
    //emit(pauseVolume());
    renderContext->startDownsampler();

    updateDisplay();
}
void DetailRender::activateExistingContext(DetailContext* c)
{
    setRenderContext(c);
    emit(windowResize(width()/renderContext->zoomFactor,height()/renderContext->zoomFactor));
    renderContext->resumeDownsampler();

    updateDisplay();
}

void DetailRender::dataRequestComplete(Datacube* dc)
{
    /*
    if(dc!=NULL)
    {
        renderContext->setCube(dc);
        setTex();
        update();   
    }
    emit(resumeVolume());
    */
}

void DetailRender::dataSliceComplete(int i,Datacube* dc)
{
    if(dc!=NULL)
    {
        renderContext->insertData(dc, i);
        if(!texPending)
        {
            QTimer::singleShot(100, this, SLOT(textureUpdateTimer()));
            texPending=true;
        }           
    }
    //emit(resumeVolume());
}

void DetailRender::textureUpdateTimer()
{
    setTex();
    updateDisplay();
    texPending=false;
}

void DetailRender::createDefaultContext()
{
    #ifdef __APPLE__
    FitsFileHandler * f =new FitsFileHandler("./astrovis.app/Contents/Resources/astrovis.fits", NULL);
    #else
    FitsFileHandler * f =new FitsFileHandler("astrovis.fits", NULL);
    #endif
    defaultContext = new DetailContext(f, 0);

    setRenderContext(defaultContext);
    ContextList.push_back(defaultContext);

    defaultContext->startDownsampler();
}

void DetailRender::setRenderContext(DetailContext *c)
{
    if(renderContext!=NULL)
    {
        renderContext->pauseDownsampler();
        //disconnect(renderContext->getFileHandler(), SIGNAL(regionDone(Datacube *)),this,SLOT(dataRequestComplete(Datacube*)));
        disconnect(renderContext->getFileHandler(), SIGNAL(sliceDone(int, Datacube *)),this,SLOT(dataSliceComplete(int,Datacube*)));
    }
    renderContext=c;
    //connect(renderContext->getFileHandler(), SIGNAL(regionDone(Datacube *)),this,SLOT(dataRequestComplete(Datacube*)));
    connect(renderContext->getFileHandler(), SIGNAL(sliceDone(int,Datacube *)),this,SLOT(dataSliceComplete(int,Datacube*)));
    if(renderContext->getCube()!=NULL)
    {
        setTex();     
    }

    setSliceLabel(renderContext->datadepth);
}

void DetailRender::InitializeValues()
{
    setMouseTracking(true);
	//Shader Init
    shader->shaderProgram = NULL;
    shader->vertexShader = shader->fragmentShader = NULL;
    #ifdef __APPLE__
    createShaders("./astrovis.app/Contents/Resources/Basic.vsh", "./astrovis.app/Contents/Resources/Basic.fsh");
    #else
    createShaders("./Basic.vsh", "./Basic.fsh");
    #endif

    renderContext = NULL;
    
    createDefaultContext();

    viewString[0] = QString("Front");
    viewString[1] = QString("Side");
    viewString[2] = QString("Top");

    leftDown = false;
    rightDown = false;

    f.setPointSize(15);
    f.setBold(true);

    texPending = false;

    planeTexture = 0;
    colourMap[0] = 0;
    colourMap[1] = 0;
    colourMap[2] = 0;
    colourMap[3] = 0;

    texMaxSize = 2048;

    texPiecesW = 1;
    texPiecesH = 1;
    texturePieces = NULL;

    displayTimerID=startTimer(33);
    displayHasChanged=false;

}

//Create Shader Program
//Parameters - vertex shader file, fragment shader file
void DetailRender::createShaders(QString vshader, QString fshader)
{
	makeCurrent();

    if(shader->shaderProgram)
    {
        shader->shaderProgram->release();
        shader->shaderProgram->removeAllShaders();
    }
    else shader->shaderProgram = new QGLShaderProgram;

    if(shader->vertexShader)
    {
        delete shader->vertexShader;
        shader->vertexShader = NULL;
    }

    if(shader->fragmentShader)
    {
        delete shader->fragmentShader;
        shader->fragmentShader = NULL;
    }

    // load and compile vertex shader
    QFileInfo vsh(vshader);
    if(vsh.exists())
    {
        shader->vertexShader = new QGLShader(QGLShader::Vertex);
        if(shader->vertexShader->compileSourceFile(vshader))
            shader->shaderProgram->addShader(shader->vertexShader);
        else qWarning() << "Vertex Shader Error" << shader->vertexShader->log();
    }
    else qWarning() << "Vertex Shader source file " << vshader << " not found.";


    // load and compile fragment shader
    QFileInfo fsh(fshader);
    if(fsh.exists())
    {
        shader->fragmentShader = new QGLShader(QGLShader::Fragment);
        if(shader->fragmentShader->compileSourceFile(fshader))
            shader->shaderProgram->addShader(shader->fragmentShader);
        else qWarning() << "Fragment Shader Error" << shader->fragmentShader->log();
    }
    else qWarning() << "Fragment Shader source file " << fshader << " not found.";

    if(!shader->shaderProgram->link())
    {
        qWarning() << "Shader Program Linker Error" << shader->shaderProgram->log();
    }
}

DetailRender::~DetailRender()
{
	
}