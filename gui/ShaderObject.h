#ifndef SHADEROBJECT_H
#define SHADEROBJECT_H

#include <QtGui>
#include <QGLShader>
#include <QGLShaderProgram>

class ShaderObject
{

	public:
		ShaderObject(QString vshader, QString fshader);
		void loadShader(QString vshader, QString fshader);

		~ShaderObject();

		QGLShaderProgram *shaderProgram;
		QGLShader *fragmentShader,*vertexShader;

	private:
		
};

#endif