#ifndef INFOBOX
#define INFOBOX

#include <QtGui>

class InfoBox : public QFrame {
	
	Q_OBJECT

public:

	InfoBox (QMainWindow * parent);

	void addLabel (QLabel * label, int x, int y);
	void addLabel (QLabel * label);
	void skipLine ();

	QGridLayout * gridLayout;

	int curX;
	int curY;

	int numCol;

	void init (){

	gridLayout = new QGridLayout(this);
	gridLayout->setSpacing(6);
    gridLayout->setContentsMargins(10, 10, 0 ,0);

	}

};

#endif