#ifndef CONTEXT_H
#define CONTEXT_H

#include <cstring>
#include <algorithm>
#include <QtGui>

struct Coordinate
{
	bool Valid;
	int Plane;
	double x;
	double y;
	double z;
	Coordinate()
	{
		Valid=false;
		//printf("lol\n");
	}
};

struct Area
{
	Coordinate Point;
	Coordinate Width;
	Coordinate Height;
};

class Context : public QTreeWidgetItem
{
	public:
		Context(QTreeWidgetItem *, int);
		~Context();

	char * fileName;
	int index;
	bool compare(char*, int);

};


//int VolumeContext::selectorHeight=0;

#endif