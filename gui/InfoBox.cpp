#include <QtGui>
#include "InfoBox.h"

InfoBox::InfoBox (QMainWindow * parent) : QFrame (parent) {
	
	init();

	curX = 0;
	curY = 0;

	numCol = 2;
}

void InfoBox::addLabel (QLabel * label, int x, int y){

	gridLayout->addWidget(label, y, x);
}

void InfoBox::addLabel (QLabel * label){

	gridLayout->addWidget(label, curY, curX);

	curX++;

	if(curX >= numCol){
		curX = 0;
		curY++;
	}


}

void InfoBox::skipLine (){

	if(curX != 0){
		curX = 0;
		curY++;
	}

}