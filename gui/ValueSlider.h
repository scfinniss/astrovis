#ifndef VALUESLIDER_H
#define VALUESLIDER_H

#include <QtGui>

class ValueSlider: public QWidget
{
	Q_OBJECT

	public:
	ValueSlider(QMainWindow *,int,int,int,int);
	~ValueSlider();

	virtual void paintEvent(QPaintEvent*);

	virtual void resizeEvent(QResizeEvent * e);

	void drawDisplayBlend(QPainter);
	void drawDisplaySolid(QPainter);

	void mousePressEvent ( QMouseEvent * e );
	void mouseDoubleClickEvent(QMouseEvent * e);
	void mouseMoveEvent ( QMouseEvent * e );
	void mouseReleaseEvent( QMouseEvent * e );

	void initValues(int ,QColor*);
	void addDivider(QColor, float);
	void removeColour(int);
	int dividerCollision(int);

	void createColourTable();
	void solidTable();
	void blendTable();
	int blendColour(int,int,float);
	QColor blendColour(QColor,QColor,float);

	signals:
		void sliderChange(float**,int *,int);

	public slots:
		void blendToggle();
	
	private:
	QVector<QColor> Color;
	QVector<float> Position;
	bool mouseDrag;
	int selectDivider;
	bool blendDisplay;
	int rgba[4];
	int transferSize;
	float ** rgbaMap;
};

#endif
