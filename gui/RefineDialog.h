#ifndef REFINEDIALOG
#define REFINEDIALOG

#include <QtGui>
#include "../fits/FitsFileHandler.h"

class RefineDialog : public QDialog {
	
	Q_OBJECT

public:

	RefineDialog (QMainWindow * parent);
	~RefineDialog ();

	QGridLayout *gridLayout;
	QRadioButton *radioHighRes;
	QSpinBox *spinWidth;
	QLabel *labelWidth;
	QLabel *WidthValue;
	QRadioButton *radioMediumRes;
	QSpinBox *spinHeight;
	QLabel *labelHeight;
	QLabel *HeightValue;
	QRadioButton *radioLowRes;
	QSpinBox *spinDepth;
	QLabel *labelDepth;
	QLabel *DepthValue;
	QPushButton *cancelButton;
	QPushButton *acceptButton;
	QLabel *labelScatterReads;
        QSpinBox *spinScatterReads;

	int fileWidth;
	int fileHeight;
	int fileDepth;

	FitsFileHandler * ffh;

	QTreeWidgetItem* item;

	void init (){
		
        gridLayout = new QGridLayout(this);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));

        radioHighRes = new QRadioButton(this);
        radioHighRes->setObjectName(QString::fromUtf8("radioHighRes"));

        gridLayout->addWidget(radioHighRes, 0, 0, 1, 1);

        spinWidth = new QSpinBox(this);
        spinWidth->setObjectName(QString::fromUtf8("spinWidth"));

        gridLayout->addWidget(spinWidth, 0, 1, 1, 2);

        labelWidth = new QLabel(this);
        labelWidth->setObjectName(QString::fromUtf8("labelWidth"));

        gridLayout->addWidget(labelWidth, 1, 1, 1, 1);

        WidthValue = new QLabel(this);
        WidthValue->setObjectName(QString::fromUtf8("WidthValue"));

        gridLayout->addWidget(WidthValue, 1, 2, 1, 1);

        radioMediumRes = new QRadioButton(this);
        radioMediumRes->setObjectName(QString::fromUtf8("radioMediumRes"));

        gridLayout->addWidget(radioMediumRes, 2, 0, 1, 1);

        spinHeight = new QSpinBox(this);
        spinHeight->setObjectName(QString::fromUtf8("spinHeight"));

        gridLayout->addWidget(spinHeight, 2, 1, 1, 2);

        labelHeight = new QLabel(this);
        labelHeight->setObjectName(QString::fromUtf8("labelHeight"));

        gridLayout->addWidget(labelHeight, 3, 1, 1, 1);

        HeightValue = new QLabel(this);
        HeightValue->setObjectName(QString::fromUtf8("HeightValue"));

        gridLayout->addWidget(HeightValue, 3, 2, 1, 1);

        radioLowRes = new QRadioButton(this);
        radioLowRes->setObjectName(QString::fromUtf8("radioLowRes"));

        gridLayout->addWidget(radioLowRes, 4, 0, 1, 1);

        spinDepth = new QSpinBox(this);
        spinDepth->setObjectName(QString::fromUtf8("spinDepth"));

        gridLayout->addWidget(spinDepth, 4, 1, 1, 2);

        labelDepth = new QLabel(this);
        labelDepth->setObjectName(QString::fromUtf8("labelDepth"));

        gridLayout->addWidget(labelDepth, 5, 1, 1, 1);

        DepthValue = new QLabel(this);
        DepthValue->setObjectName(QString::fromUtf8("DepthValue"));

        gridLayout->addWidget(DepthValue, 5, 2, 1, 1);

        labelScatterReads = new QLabel(this);
        labelScatterReads->setObjectName(QString::fromUtf8("labelScatterReads"));

        gridLayout->addWidget(labelScatterReads, 6, 0, 1, 1);

        spinScatterReads = new QSpinBox(this);
        spinScatterReads->setObjectName(QString::fromUtf8("spinScatterReads"));

        gridLayout->addWidget(spinScatterReads, 6, 1, 1, 2);

        cancelButton = new QPushButton(this);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        gridLayout->addWidget(cancelButton, 7, 0, 1, 1);

        acceptButton = new QPushButton(this);
        acceptButton->setObjectName(QString::fromUtf8("acceptButton"));

        gridLayout->addWidget(acceptButton, 7, 1, 1, 2);

        gridLayout->setColumnStretch (0, 2);
        gridLayout->setColumnStretch (1, 3);
        gridLayout->setColumnStretch (2, 3);

        spinWidth->setMaximum(9999);
		spinHeight->setMaximum(9999);
		spinDepth->setMaximum(9999);

		spinWidth->setMinimum(1);
		spinHeight->setMinimum(1);
		spinDepth->setMinimum(1);

        radioHighRes->setText(QApplication::translate("Astrovis", "High Res", 0, QApplication::UnicodeUTF8));
        labelWidth->setText(QApplication::translate("Astrovis", "Width", 0, QApplication::UnicodeUTF8));
        WidthValue->setText(QApplication::translate("Astrovis", "TextLabel", 0, QApplication::UnicodeUTF8));
        radioMediumRes->setText(QApplication::translate("Astrovis", "Medium Res", 0, QApplication::UnicodeUTF8));
        labelHeight->setText(QApplication::translate("Astrovis", "Height", 0, QApplication::UnicodeUTF8));
        HeightValue->setText(QApplication::translate("Astrovis", "TextLabel", 0, QApplication::UnicodeUTF8));
        radioLowRes->setText(QApplication::translate("Astrovis", "LowRes", 0, QApplication::UnicodeUTF8));
        labelDepth->setText(QApplication::translate("Astrovis", "Depth", 0, QApplication::UnicodeUTF8));
        DepthValue->setText(QApplication::translate("Astrovis", "TextLabel", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("Astrovis", "Cancel", 0, QApplication::UnicodeUTF8));
        acceptButton->setText(QApplication::translate("Astrovis", "Accept", 0, QApplication::UnicodeUTF8));

        #ifndef QT_NO_TOOLTIP
        labelScatterReads->setToolTip(QApplication::translate("Astrovis", "Number of estimates taken from the file (will slow the refinement process)", 0, QApplication::UnicodeUTF8));
		#endif // QT_NO_TOOLTIP
        labelScatterReads->setText(QApplication::translate("Astrovis", "Scatter Reads:", 0, QApplication::UnicodeUTF8));

	}

	public slots:

	void setFileDimensions (int w, int h, int d);

	void setHighRes ();

	void setMediumRes ();

	void setLowRes ();

	void setFitsFileHandler (QTreeWidgetItem* i);
        void setFitsFileHandler (QTreeWidgetItem* i, int w, int h, int d);

	void hide ();

	void accept();

	signals:

	void resChangeW (int w);
	void resChangeH (int h);
	void resChangeD (int d);

	void refine (QTreeWidgetItem * i, int w, int h, int d, int s);


};


#endif