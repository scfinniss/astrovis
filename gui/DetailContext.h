#ifndef DETAILCONTEXT_H
#define DETAILCONTEXT_H

#include "../fits/FitsImageHeader.h"
#include "../fits/FitsFileHandler.h"
#include "../fits/Datacube.h"
#include "../ipc/Downsampler.h"
#include "Context.h"
#include <boost/thread.hpp>

class DetailContext : public Context
{

	public:
		DetailContext(FitsImageHeader *);
		DetailContext(FitsFileHandler* i,int);
		~DetailContext();

		void initValues();

		int startDownsampler();
		void stopDownsampler();
		void pauseDownsampler();
		void resumeDownsampler();
		void waitDownsampler();

	 	Downsampler * getDownsampler();

	 	FitsFileHandler* getFileHandler();

	 	void setBounds(int,int,int,int,int,int);

	 	void setPosition(float,float);

	 	int setSlicePosition(int);
	 	int setOrientation(int);

	 	void setTextureDim();
	 	void setShift(int,int,int);

	 	void setReverse(int);

	 	void getTexCoords(double*);

	 	int getDimension(int );

	 	int getDataSizeByPlane(int);

	 	float getValue(int x,int y);

		Datacube * getTextureData();

		Datacube * getCube();
		void setCube(Datacube*);

		void insertData(Datacube*,int);

		int texturewidth,textureheight;

		float xposition,yposition;

		float minValue,maxValue;

		float zoomFactor;

		int datawidth,dataheight,datadepth;

		int xbound,ybound,zbound,wbound,hbound,dbound;

		//Orientation
		//0 = xy
		//1 = zy
		//2 = xz
		int orientation;
		int sliceposition;
		int reverse;

		QPoint pShift [3];

		

	private:
		boost::mutex displayCubeMutex;
		Downsampler * downsampler;
		FitsImageHeader * imageItem;
		FitsFileHandler * fitsFile;
		Datacube* displayCube;


		


};

#endif