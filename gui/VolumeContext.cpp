#include "VolumeContext.h"
#include <float.h>
#include "../common.h"

VolumeContext::VolumeContext(FitsImageHeader * i):Context(i,1003)
{
	imageItem=i;

	this->setHidden(false);
	QString str;
	str.setNum(i->childCount()-1);
	this->setText(0,"Volume"+str);
	//Type
	this->setText(1,"Volume Display");

	fileName=i->getParent()->filename;
	index=i->getIndex();

	downsampler = new Downsampler (i->getParent(),index);

	xbound=ybound=zbound=0;
	wbound=downsampler->cube->width();
	hbound=downsampler->cube->height();
	dbound=downsampler->cube->depth();

	initValues();

	//setBoundaries();
}

VolumeContext::VolumeContext(FitsFileHandler* f,int h):Context(NULL,1003)
{
	downsampler = new Downsampler (f,h);

	fileName=f->filename;
	index=h;

	xbound=ybound=zbound=0;
	wbound=downsampler->cube->width();
	hbound=downsampler->cube->height();
	dbound=downsampler->cube->depth();

	initValues();

	//setBoundaries();
}

VolumeContext::VolumeContext(VolumeContext* duplicate):Context(duplicate,1003)
{

	imageItem=duplicate->getImage();

	this->setHidden(false);
	QString str;

	str.setNum(imageItem->childCount()-1);

	this->setText(0,"Volume"+str);
	//Type
	this->setText(1,"Volume Display");

	fileName=duplicate->fileName;
	index=duplicate->index;

	
	downsampler = new Downsampler (imageItem->getParent(),index);

	initValues();
}

void VolumeContext::initValues()
{
	pShift[0]=QPoint(0,0);
	pShift[1]=QPoint(0,0);
	pShift[2]=QPoint(0,0);

	datax=datay=dataz=0;
	datawidth=downsampler->cube->width();
	dataheight=downsampler->cube->height();
	datadepth=downsampler->cube->depth();

	focalArea.Point.Valid=false;

	volumewidth=volumeheight=volumedepth=1;
	displayCube=NULL;

	zoomAmount=200;
}

int VolumeContext::selectorWidth=0; 
int VolumeContext::selectorHeight=0; 

int VolumeContext::startDownsampler()
{
	return downsampler->StartDownsampling(xbound,ybound,zbound,
											wbound,hbound,dbound,
												inputwidth,inputheight,inputdepth, scatterCount);
}
void VolumeContext::stopDownsampler()
{
	downsampler->StopDownsampling();
}
void VolumeContext::pauseDownsampler()
{
	downsampler->Pause();
}
void VolumeContext::resumeDownsampler()
{
	downsampler->Resume();
}

void VolumeContext::waitDownsampler()
{
	downsampler->Join();
}

Downsampler * VolumeContext::getDownsampler()
{
	return downsampler;
}

void VolumeContext::moveSelector(double x,double y,double z)
{
	double i,j,k;
	i=std::min(1.0-focalArea.Width.x,std::max(focalArea.Point.x+x,0.0));
	j=std::min(1.0-focalArea.Height.y,std::max(focalArea.Point.y+y,0.0));
	k=std::min(1.0,std::max(focalArea.Point.z+z,0.0));
	setSelectorPoint(i,j,k);
}

void VolumeContext::setBoundaries(VolumeContext* c)
{
	xbound = c->xbound - (c->pShift[0].x()*(c->wbound/c->volumewidth));
	ybound = c->ybound + (c->pShift[1].y()*(c->hbound/c->volumeheight));
	zbound = c->zbound + (c->pShift[2].y()*(c->dbound/c->volumedepth));

	wbound = c->wbound + (c->pShift[0].x()*(c->wbound/c->volumewidth)) - (c->pShift[0].y()*(c->wbound/c->volumewidth));
	hbound = c->hbound - (c->pShift[1].y()*(c->hbound/c->volumeheight)) + (c->pShift[1].x()*(c->hbound/c->volumeheight));
	dbound = c->dbound - (c->pShift[2].y()*(c->dbound/c->volumedepth)) + (c->pShift[2].x()*(c->dbound/c->volumedepth));
}

void VolumeContext::setBoundaries()
{
	xbound=xbound-(pShift[0].x()*(wbound/volumewidth));
	ybound=ybound+(pShift[1].y()*(hbound/volumeheight));
	zbound=zbound+(pShift[2].y()*(dbound/volumedepth));
	wbound=wbound-((pShift[0].x()*(wbound/volumewidth)))-(pShift[0].y()*(wbound/volumewidth));
	hbound=hbound-((pShift[1].y()*(hbound/volumeheight)))+(pShift[1].x()*(hbound/volumeheight));
	dbound=dbound-((pShift[2].y()*(dbound/volumedepth)))+(pShift[2].x()*(dbound/volumedepth));
}
void VolumeContext::setBoundaries(int x,int y,int z,int w,int h,int d)
{
	xbound=x;
	ybound=y;
	zbound=z;
	wbound=w;
	hbound=h;
	dbound=d;
}

void VolumeContext::setSelectorWidth(double x,double y,double z)
{
	focalArea.Width.x = std::min(x,1.0-(-pShift[0].x()+pShift[0].y())/(double)volumewidth);
	focalArea.Width.y = std::min(y,1.0-(-pShift[1].x()+pShift[1].y())/(double)volumeheight);
	focalArea.Width.z = std::min(z,1.0-(-pShift[2].x()+pShift[2].y())/(double)volumedepth);
}

void VolumeContext::setSelectorHeight(double x,double y,double z)
{
	focalArea.Height.x = std::min(x,1.0-(-pShift[0].x()+pShift[0].y())/(double)volumewidth);
	focalArea.Height.y = std::min(y,1.0-(-pShift[1].x()+pShift[1].y())/(double)volumeheight);
	focalArea.Height.z = std::min(z,1.0-(-pShift[2].x()+pShift[2].y())/(double)volumedepth);
}

void VolumeContext::setSelectorPoint(double x,double y,double z)
{
	focalArea.Point.x = std::min(std::max(x, -0.5+focalArea.Width.x/2.0+pShift[0].y()/(double)volumewidth), 0.5-focalArea.Width.x/2.0+pShift[0].x()/(double)volumewidth);
	focalArea.Point.y = std::min(std::max(y, -0.5+focalArea.Height.y/2.0+pShift[1].y()/(double)volumeheight), 0.5-focalArea.Height.y/2.0+pShift[1].x()/(double)volumeheight);
	focalArea.Point.z = std::min(std::max(z, -0.5+(focalArea.Width.z/2.0+focalArea.Height.z/2.0)+pShift[2].y()/(double)volumedepth), 0.5-(focalArea.Width.z/2.0+focalArea.Height.z/2.0)+pShift[2].x()/(double)volumedepth);
}

Datacube * VolumeContext::getVolumeData()
{
	Datacube * dc = downsampler->GetLatestResult();
	return dc;
}

//Return current dimensions of the displayed volume in real file size
void VolumeContext::getDim(int *d)
{
	d[0]=wbound-(-(pShift[0].x()*(wbound/volumewidth)))-(pShift[0].y()*(wbound/volumewidth));
	d[1]=hbound-((pShift[1].y()*(hbound/volumeheight)))+(pShift[1].x()*(hbound/volumeheight));
	d[2]=dbound-((pShift[2].y()*(dbound/volumedepth)))+(pShift[2].x()*(dbound/volumedepth));
}

void VolumeContext::setInputSettings(int w,int h,int d,int s)
{
	inputwidth=w;
	inputheight=h;
	inputdepth=d;
	scatterCount=s;
	zoomAmount=2*std::max(inputwidth,std::max(inputheight,inputdepth));
}

void VolumeContext::setDataCoverage(int x,int y,int z,int w,int h,int d)
{
	datax=x;
	datay=y;
	dataz=z;
	datawidth=w;
	dataheight=h;
	datadepth=d;
}

void VolumeContext::setDetail(DetailContext * d)
{
	detail=d;
}

DetailContext* VolumeContext::getDetail()
{
	return detail;
}

int VolumeContext::getVolumeByPlane(int p)
{
	switch(p)
	{
		case 0:
			return volumewidth;
		break;
		case 1:
			return volumeheight;
		break;
		case 2:
			return volumedepth;
		break;
	}
	return -1;
}

FitsImageHeader* VolumeContext::getImage()
{
	return imageItem;
}

Datacube * VolumeContext::getCube()
{
	return displayCube;
}

void VolumeContext::setCube()
{
	Datacube * dc = downsampler->GetLatestResult();
	if(dc)
	{
		if(displayCube)
		{
			delete displayCube;
			displayCube=NULL;
		}
		displayCube = dc;
	}	
}

void VolumeContext::setVolumeDimension()
{
	if(displayCube==NULL)
	{
		volumewidth=inputwidth;
		volumeheight=inputheight;
		volumedepth=inputdepth;
	}
	else
	{
		volumewidth=displayCube->width;
		volumeheight=displayCube->height;
		volumedepth=displayCube->depth;;
	}
}

VolumeContext::~VolumeContext()
{
	delete downsampler;
}