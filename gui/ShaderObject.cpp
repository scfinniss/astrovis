#include "ShaderObject.h"

ShaderObject::ShaderObject(QString vshader, QString fshader)
{
	shaderProgram = NULL;
    vertexShader = fragmentShader = NULL;

    //loadShader(vshader,fshader);
}

void ShaderObject::loadShader(QString vshader,QString fshader)
{
    if(shaderProgram)
    {
        shaderProgram->release();
        shaderProgram->removeAllShaders();
    }
    else shaderProgram = new QGLShaderProgram;

    if(vertexShader)
    {
        delete vertexShader;
        vertexShader = NULL;
    }

    if(fragmentShader)
    {
        delete fragmentShader;
        fragmentShader = NULL;
    }

    // load and compile vertex shader
    QFileInfo vsh(vshader);
    if(vsh.exists())
    {
        vertexShader = new QGLShader(QGLShader::Vertex);
        if(vertexShader->compileSourceFile(vshader))
            shaderProgram->addShader(vertexShader);
        else qWarning() << "Vertex Shader Error" << vertexShader->log();
    }
    else qWarning() << "Vertex Shader source file " << vshader << " not found.";


    // load and compile fragment shader
    QFileInfo fsh(fshader);
    if(fsh.exists())
    {
        fragmentShader = new QGLShader(QGLShader::Fragment);
        if(fragmentShader->compileSourceFile(fshader))
            shaderProgram->addShader(fragmentShader);
        else qWarning() << "Fragment Shader Error" << fragmentShader->log();
    }
    else qWarning() << "Fragment Shader source file " << fshader << " not found.";

    if(!shaderProgram->link())
    {
        qWarning() << "Shader Program Linker Error" << shaderProgram->log();
    }
}

ShaderObject::~ShaderObject()
{
	
}