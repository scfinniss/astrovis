#include "ValueDisplay.h"

ValueDisplay::ValueDisplay()
{
	QGridLayout* layout=new QGridLayout(this);
	x=new QLabel();
	y=new QLabel();
	z=new QLabel();
	val=new QLabel();
	xcov=new QLabel("X:");ycov=new QLabel("Y:");zcov=new QLabel("Z:");
	xdim=new QLabel("X:");ydim=new QLabel("Y:");zdim=new QLabel("Z:");
	QLabel* covLab=new QLabel("Current Coverage");
	QLabel* dimLab=new QLabel("File Dimensions");
	layout->addWidget(x,0,0);
	layout->addWidget(y,0,1);
	layout->addWidget(z,0,2);
	layout->addWidget(val,0,3);
	layout->addWidget(dimLab,1,0);
	layout->addWidget(xdim,2,0);
	layout->addWidget(ydim,2,1);
	layout->addWidget(zdim,2,2);
	layout->addWidget(covLab,3,0);
		layout->addWidget(xcov,4,0);
	layout->addWidget(ycov,4,1);
	layout->addWidget(zcov,4,2);
	x->setText("X: ");
	y->setText("Y: ");
	z->setText("Z: ");
	val->setText("Value: ");
}

void ValueDisplay::setValue(double xp,double yp,double zp,float v)
{
	QString number;
	x->setText("X: "+number.setNum(xp));
	y->setText("Y: "+number.setNum(yp));
	z->setText("Z: "+number.setNum(zp));
	val->setText("Value: "+number.setNum(v));
}

void ValueDisplay::setCoverage(int*c)
{
	QString number;
	QString number2;
	QString number3;
	xcov->setText("X: "+number.setNum(c[0])+" -> "+number2.setNum(c[0]+c[1])+"("+number3.setNum(c[1])+")");
	ycov->setText("Y: "+number.setNum(c[2])+" -> "+number2.setNum(c[2]+c[3])+"("+number3.setNum(c[3])+")");
	zcov->setText("Z: "+number.setNum(c[4])+" -> "+number2.setNum(c[4]+c[5])+"("+number3.setNum(c[5])+")");
}

void ValueDisplay::setDimensions(int x,int y,int z)
{
	QString number;
	xdim->setText("X: "+number.setNum(x));
	ydim->setText("Y: "+number.setNum(y));
	zdim->setText("Z: "+number.setNum(z));
}

ValueDisplay::~ValueDisplay()
{
	delete x;
	delete y;
	delete z;
	delete val;
}
