#include "VolRender.h"

#include <float.h>
#include "VolumeContext.h"
#include <queue>
#include <cmath>

//Constructor
VolRender::VolRender(QMainWindow *parent, FitsTreeWidget * tree)
:QGLWidget(QGLFormat(QGL::SampleBuffers) , parent)
{
    renderContext = NULL;
    defaultContext = NULL;

    setMouseTracking(true);
    InitializeValues();
}

VolRender::VolRender(QMainWindow *parent, FitsTreeWidget * tree, ShaderObject* s)
:QGLWidget(QGLFormat(QGL::SampleBuffers) , parent)
{
    renderContext = NULL;
    defaultContext = NULL;
    shader=s;

    setMouseTracking(true);
    InitializeValues();
}

//Initialize gl variables
void VolRender::initializeGL()
{
    dprintf ("GL Init.\n");
    makeCurrent();
    glewInit();
    glClearColor(0.25f, 0.266f, 0.301f, 0.0f);

    //Enable Blending
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_FRONT_AND_BACK);
    
    //Initialise rotation matrix
    glLoadIdentity();
    glGetFloatv(GL_MODELVIEW_MATRIX, rotationMatrix);
}

//Resize Window
void VolRender::resizeGL(int w, int h)
{
    makeCurrent();
    glViewport(0, 0, w, h); 
}

//Draw Step
void VolRender::paintGL()
{
    makeCurrent();
    //Input Function
    updateGL();
    //Draw Operations
    
    if(displayHasChanged)
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //renderContext->setBoundaries();
        draw3D();

        draw2D();
        displayHasChanged=false;
    }  
}

void VolRender::clearProjection()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
}

void VolRender::clearModelView()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void VolRender::PerspectiveTransform()
{  
    gluPerspective(40.0,(double)width()/(double)height(),5.0,10000.0);
}

//Draw all 3D elements of the display
void VolRender::draw3D()
{
    clearProjection();
    PerspectiveTransform();
    clearModelView();

    applyRotation();
    if(renderContext->focalArea.Point.Valid)
        drawSelector();
    
    
    
    if(frameVisible)
    {
        drawSelectionFrame();
        glEnable(GL_DEPTH_TEST);
        //Draw Volume Frame
        //x,y,z renderContext->datacubeDims of volume
        //Closest plane of volume and currently moused over edges of frame
        drawFrame(renderContext->volumewidth,
                    renderContext->volumeheight,
                        renderContext->volumedepth,
                            nearPlane,selectedEdge);
    }
    glEnable(GL_DEPTH_TEST);
    drawVolume();
    glDisable(GL_DEPTH_TEST);
    
}

//Apply rotation transforms
void VolRender::applyRotation()
{
    gluLookAt(sin((circular)/360*2*Pi)*renderContext->zoomAmount,0,-cos((circular)/360*2*Pi)*renderContext->zoomAmount, 
            0.0,0.0,0.0,
            0.0f,1.0f,0.0f);

    glPushMatrix();
    glLoadIdentity();
    glRotatef(vertical, cos((circular)/360*2*Pi), 0, sin((circular)/360*2*Pi));
    glGetFloatv(GL_MODELVIEW_MATRIX, rotationMatrix);
    glPopMatrix();

    glMultMatrixf(rotationMatrix);
}

//Draw Volume render of data
void VolRender::drawVolume()
{
    setClip();
    
    shader->shaderProgram->bind(); //enable shader

    shader->shaderProgram->setUniformValue ("alphaSetting", blendSetting);

    glGetDoublev (GL_MODELVIEW_MATRIX , model);
    glGetDoublev (GL_PROJECTION_MATRIX, proj );
    glGetIntegerv(GL_VIEWPORT, view );

    double x, y, z;
    x = y = z = 0.0;
    double xmin, ymin, xmax, ymax;
    xmin = ymin = zmin = DBL_MAX;
    xmax = ymax = zmax = DBL_MIN;

    for( int i = 0; i < 8; ++i )
    {
        float bbx = (i&1) ? renderContext->volumewidth/2 : -renderContext->volumewidth/2 ;
        float bby = (i&2) ? renderContext->volumeheight/2 : -renderContext->volumeheight/2 ;
        float bbz = (i&4) ? renderContext->volumedepth/2 : -renderContext->volumedepth/2 ;

        gluProject( bbx, bby, bbz,model, proj, view, &x, &y, &z );

        if(x < xmin) xmin = x;
        if(x > xmax) xmax = x;
        if(y < ymin) ymin = y;
        if(y > ymax) ymax = y;
        if(z < zmin) zmin = z;
        if(z > zmax) zmax = z;

    }
    float deltaZ=0.00000010f;

    zmax=(zmax-deltaZ/2);
    int j=0;
    clipToggle(true);
    glColor4f(1.0,1.0,1.0,1.0);

    while(zmax>zmin)
    {
        zmax-=j*deltaZ;
        j++;
        glBegin(GL_QUADS);
        {
            gluUnProject( xmin,ymin,zmax,model, proj, view, &x, &y, &z );
            glTexCoord3f((-x/renderContext->volumewidth+0.5f), y/renderContext->volumeheight+0.5f, (z/renderContext->volumedepth+0.5f));
            glVertex3d(x, y, z);

            gluUnProject( xmax,ymin,zmax,model, proj, view, &x, &y, &z );
            glTexCoord3f((-x/renderContext->volumewidth+0.5f), y/renderContext->volumeheight+0.5f, (z/renderContext->volumedepth+0.5f));
            glVertex3d(x, y, z);

            gluUnProject( xmax,ymax,zmax,model, proj, view, &x, &y, &z );
            glTexCoord3f((-x/renderContext->volumewidth+0.5f), y/renderContext->volumeheight+0.5f, (z/renderContext->volumedepth+0.5f));
            glVertex3d(x, y, z);

            gluUnProject( xmin,ymax,zmax,model, proj, view, &x, &y, &z );
            glTexCoord3f((-x/renderContext->volumewidth+0.5f), y/renderContext->volumeheight+0.5f, (z/renderContext->volumedepth+0.5f));
            glVertex3d(x, y, z);
        }
        glEnd();
    }

    clipToggle(false);
    shader->shaderProgram->release(); //disable shader
}

//Draw Volume Frame
void VolRender::drawFrame(double x,double y,double z,int nearEdge,QPoint selectEdge)
{

    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==0||nearEdge==5)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==0&&selectEdge.x()==2)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==5&&selectEdge.x()==2)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(-x/2,-y/2,-z/2);
        glVertex3d(-x/2,y/2,-z/2);
    }
    glEnd();
    
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==0||nearEdge==4)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==0&&selectEdge.x()==1)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==4&&selectEdge.x()==2)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(x/2,-y/2,-z/2);
        glVertex3d(x/2,y/2,-z/2);
    }
    glEnd();
    
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==1||nearEdge==5)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==1&&selectEdge.x()==2)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==5&&selectEdge.x()==1)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(-x/2,-y/2,z/2);
        glVertex3d(-x/2,y/2,z/2);
    }
    glEnd();
    
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==1||nearEdge==4)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==1&&selectEdge.x()==1)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==4&&selectEdge.x()==1)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(x/2,-y/2,z/2);
        glVertex3d(x/2,y/2,z/2);
    }
    glEnd();
    
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==0||nearEdge==3)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==0&&selectEdge.y()==1)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==3&&selectEdge.y()==1)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(-x/2,-y/2,-z/2);
        glVertex3d(x/2,-y/2,-z/2);
    }
    glEnd();
    
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==0||nearEdge==2)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==0&&selectEdge.y()==2)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==2&&selectEdge.y()==1)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(-x/2,y/2,-z/2);
        glVertex3d(x/2,y/2,-z/2);
    }
    glEnd();
    
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==1||nearEdge==3)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==1&&selectEdge.y()==1)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==3&&selectEdge.y()==2)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(-x/2,-y/2,z/2);
        glVertex3d(x/2,-y/2,z/2);
    }
    glEnd();
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==1||nearEdge==2)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==1&&selectEdge.y()==2)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==2&&selectEdge.y()==2)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(-x/2,y/2,z/2);
        glVertex3d(x/2,y/2,z/2);
    }
    glEnd();
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==5||nearEdge==3)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==3&&selectEdge.x()==2)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==5&&selectEdge.y()==1)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(-x/2,-y/2,-z/2);
        glVertex3d(-x/2,-y/2,z/2);
    }
    glEnd();
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==5||nearEdge==2)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==2&&selectEdge.x()==2)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==5&&selectEdge.y()==2)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(-x/2,y/2,-z/2);
        glVertex3d(-x/2,y/2,z/2);
    }
    glEnd();
    
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==4||nearEdge==3)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==3&&selectEdge.x()==1)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==4&&selectEdge.y()==1)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(x/2,-y/2,-z/2);
        glVertex3d(x/2,-y/2,z/2);
    }
    glEnd();
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
    if(nearEdge==4||nearEdge==2)
    {
        glLineWidth(3);
        glColor4f(1.0,0.0,0.0,0.75);
        if(nearEdge==2&&selectEdge.x()==1)
            glColor4f(1.0,0.3,0.0,0.75);
        if(nearEdge==4&&selectEdge.y()==2)
            glColor4f(1.0,0.3,0.0,0.75);
    }
    glBegin(GL_LINES);
    {
        glVertex3d(x/2,y/2,-z/2);
        glVertex3d(x/2,y/2,z/2);
    }
    glEnd();
    glLineWidth(1);
    glColor4f(1.0,0.0,0.0,0.40);
}

//Draw selected volume frame
void VolRender::drawSelectionFrame()
{
    qglColor(Qt::green);
    if(renderContext->pShift[1].x()!=0||renderContext->pShift[2].x()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), renderContext->volumeheight/2+renderContext->pShift[1].x(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), renderContext->volumeheight/2+renderContext->pShift[1].x(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glEnd();
    }
    if(renderContext->pShift[1].y()!=0||renderContext->pShift[2].y()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glEnd();
    }
    if(renderContext->pShift[1].y()!=0||renderContext->pShift[2].x()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glEnd();
    }
    if(renderContext->pShift[1].x()!=0||renderContext->pShift[2].y()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), renderContext->volumeheight/2+renderContext->pShift[1].x(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), renderContext->volumeheight/2+renderContext->pShift[1].x(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glEnd();
    }
    if(renderContext->pShift[0].x()!=0||renderContext->pShift[2].x()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), renderContext->volumeheight/2+renderContext->pShift[1].x(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glEnd();
    }
    if(renderContext->pShift[0].y()!=0||renderContext->pShift[2].y()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), renderContext->volumeheight/2+renderContext->pShift[1].x(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glEnd();
    }
    if(renderContext->pShift[0].y()!=0||renderContext->pShift[2].x()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), renderContext->volumeheight/2+renderContext->pShift[1].x(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glEnd();
    }
    if(renderContext->pShift[0].x()!=0||renderContext->pShift[2].y()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), renderContext->volumeheight/2+renderContext->pShift[1].x(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glEnd();
    }
    
    if(renderContext->pShift[0].x()!=0||renderContext->pShift[1].x()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), renderContext->volumeheight/2+renderContext->pShift[1].x(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), renderContext->volumeheight/2+renderContext->pShift[1].x(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glEnd();
    }
    if(renderContext->pShift[0].y()!=0||renderContext->pShift[1].y()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glEnd();
    }
    if(renderContext->pShift[0].y()!=0||renderContext->pShift[1].x()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), renderContext->volumeheight/2+renderContext->pShift[1].x(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glVertex3f(-renderContext->volumewidth/2+renderContext->pShift[0].y(), renderContext->volumeheight/2+renderContext->pShift[1].x(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glEnd();
    }
    if(renderContext->pShift[0].x()!=0||renderContext->pShift[1].y()!=0)
    {
        glBegin(GL_LINE_LOOP);
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), renderContext->volumedepth/2+renderContext->pShift[2].x());
        glVertex3f(renderContext->volumewidth/2+renderContext->pShift[0].x(), -renderContext->volumeheight/2+renderContext->pShift[1].y(), -renderContext->volumedepth/2+renderContext->pShift[2].y());
        glEnd();
    }
    
}

void VolRender::drawSelector()
{
    //Border of selector frame
    double b=1.0;

    double xadjust,yadjust,zadjust;
    xadjust=-renderContext->focalArea.Width.x / 2.0 ;
    yadjust=-renderContext->focalArea.Height.y / 2.0  ;
    zadjust=-(renderContext->focalArea.Width.z+renderContext->focalArea.Height.z) / 2.0  ;

    //Corners of selector
    Coordinate BL=renderContext->focalArea.Point;
    Coordinate TL=addCoordinate(renderContext->focalArea.Point, renderContext->focalArea.Height, true);
    Coordinate TR=addCoordinate(renderContext->focalArea.Point, addCoordinate(renderContext->focalArea.Width,renderContext->focalArea.Height, true), true);
    Coordinate BR=addCoordinate(renderContext->focalArea.Point, renderContext->focalArea.Width, true);

    glEnable(GL_DEPTH_TEST);

    glColor4f(0.0,0.0,0.0,1.0);

    //Array Holding selector vertices
    double * vertArray = new double[3];

    glPushMatrix();
    glTranslated(xadjust* renderContext->volumewidth,yadjust* renderContext->volumeheight,zadjust* renderContext->volumedepth);

    //Draw Selector Frame
    glBegin(GL_QUADS);

        glVertex3dv(translateCoordinate(vertArray,BL,-b,0.0));
        glVertex3dv(translateCoordinate(vertArray,TL,-b,0.0));
        glVertex3dv(translateCoordinate(vertArray,TL ,0.0,0.0));
        glVertex3dv(translateCoordinate(vertArray,BL,0.0,0.0));


        glVertex3dv(translateCoordinate(vertArray,BR,b,0.0));
        glVertex3dv(translateCoordinate(vertArray,TR,b,0.0));
        glVertex3dv(translateCoordinate(vertArray,TR ,0.0,0.0));
        glVertex3dv(translateCoordinate(vertArray,BR,0.0,0.0));


        glVertex3dv(translateCoordinate(vertArray,BL,-b,-b));
        glVertex3dv(translateCoordinate(vertArray,BR,b,-b));
        glVertex3dv(translateCoordinate(vertArray,BR ,b,0.0));
        glVertex3dv(translateCoordinate(vertArray,BL,-b,0.0));


        glVertex3dv(translateCoordinate(vertArray,TL,-b,b));
        glVertex3dv(translateCoordinate(vertArray,TR,b,b));
        glVertex3dv(translateCoordinate(vertArray,TR ,b,0.0));
        glVertex3dv(translateCoordinate(vertArray,TL,-b,0.0));

    glEnd();

    

    if(selectorDisplayVisible)
    {
        shader->shaderProgram->bind(); //enable shader
        
        shader->shaderProgram->setUniformValue ("alphaSetting", 1.0f);

        //Selector Display (Turn on and off)
        //xadjust=yadjust=zadjust=0;
        xadjust=0.5-renderContext->focalArea.Width.x/2.0;
        yadjust=0.5-renderContext->focalArea.Height.y/2.0;
        zadjust=0.5-renderContext->focalArea.Width.z/2.0-renderContext->focalArea.Height.z/2.0;
        
        glBegin(GL_QUADS);
            glTexCoord3d(1.0-BL.x-xadjust,BL.y+yadjust,BL.z+zadjust);
            glVertex3dv(translateCoordinate(vertArray,BL,0.0,0.0));
            glTexCoord3d(1.0-BR.x-xadjust,BR.y+yadjust,BR.z+zadjust);
            glVertex3dv(translateCoordinate(vertArray,BR,0.0,0.0));
            glTexCoord3d(1.0-TR.x-xadjust,TR.y+yadjust,TR.z+zadjust);
            glVertex3dv(translateCoordinate(vertArray,TR,0.0,0.0));
            glTexCoord3d(1.0-TL.x-xadjust,TL.y+yadjust,TL.z+zadjust);
            glVertex3dv(translateCoordinate(vertArray,TL,0.0,0.0));
        glEnd();

        glPopMatrix();

        shader->shaderProgram->setUniformValue ("alphaSetting", 0.0f);

        glDisable(GL_DEPTH_TEST);

        shader->shaderProgram->release(); //disable shader
    }

    delete [] vertArray;
}

double* VolRender::translateCoordinate(double *&v,Coordinate c, double bw,double bh)
{
    v[0]=(c.x)*renderContext->volumewidth+(std::ceil(std::abs(renderContext->focalArea.Width.x)))*bw+(std::ceil(std::abs(renderContext->focalArea.Height.x)))*bh;
    v[1]=(c.y)*renderContext->volumeheight+(std::ceil(std::abs(renderContext->focalArea.Width.y)))*bw+(std::ceil(std::abs(renderContext->focalArea.Height.y)))*bh;
    v[2]=(c.z)*renderContext->volumedepth+(std::ceil(std::abs(renderContext->focalArea.Width.z)))*bw+(std::ceil(std::abs(renderContext->focalArea.Height.z)))*bh;
    return v;
}

Coordinate VolRender::addCoordinate(Coordinate a, Coordinate b, bool s)
{
    Coordinate r;
    if(s)
    {
        r.x=a.x+b.x;
        r.y=a.y+b.y;
        r.z=a.z+b.z;
    }
    else
    {
        r.x=a.x-b.x;
        r.y=a.y-b.y;
        r.z=a.z-b.z;
    }
    return r;
}

Coordinate VolRender::mulCoordinate(Coordinate a, double m)
{
    Coordinate r;
    r.x=a.x*m;
    r.y=a.y*m;
    r.z=a.z*m;
    return r;
}

void VolRender::resizeSelector()
{
    
}

void VolRender::OrthoTransform()
{
    glOrtho(0,this->width(),0,this->height(),-1.0,1.0);

}

//Draw all 2D elements of the display
void VolRender::draw2D()
{
    clearProjection();
    OrthoTransform();
    clearModelView();

    drawHUD(true);
    drawProgress();
}

//Draw HUD Elements
void VolRender::drawHUD(bool drawMode)
{
    //Define a "HUD box" inside which all hud elements lie
    //defined as 15% of the largest renderContext->datacubeDim

    double hudBox= std::min(150,(int)(std::max(this->width(),this->height())*0.15));
    hudBox-=((int)hudBox)%2;

    glEnable(GL_POINT_SMOOTH);
    glColor4f(0.7,0.7,0.7,1.0);

    glPushMatrix();
    glTranslated(this->width()-hudBox*1.2,hudBox*1.2,0);

    /*glBegin(GL_QUADS);
        glVertex2d(0, 0);
        glVertex2d(hudBox, 0);
        glVertex2d(hudBox, -hudBox);
        glVertex2d(0, -hudBox);
    glEnd();*/

    glColor4f(0.0,0.0,0.15,1.0);
    if(HUDElement==4)
            glColor4f(0.5,0.0,0.7,1.0);
    //Centre Button
    glPushMatrix();
    glTranslated(hudBox*0.5,-hudBox*0.5,0);
    glPointSize(60*(hudBox/150.0));
    glBegin(GL_POINTS);
    glVertex2d(0, 0);
    glEnd();
    glPopMatrix();
    //

    //Arrow
    for(int i=0;i<4;i++)
    {
        glColor4f(0.0,0.0,0.15,1.0);
        if(HUDElement==i)
            glColor4f(0.5,0.0,0.7,1.0);
        glPushName(i);
        glPushMatrix();
        glTranslated(hudBox*0.5,-hudBox*0.5,0);
        glRotated(90.0*i,0,0,1.0);
        glTranslated(0,hudBox*0.5,0);
        glBegin(GL_POLYGON);
        glVertex2d(0, 0);
        glVertex2d(-hudBox*0.25, -hudBox*0.25);
        if(drawMode)
            glVertex2d(0, -hudBox*0.125);
        glVertex2d(hudBox*0.25, -hudBox*0.25);
        glVertex2d(0, 0);
        glEnd();
        glPopMatrix();
        glPopName();
    }
    //

    glPopMatrix();
}

void VolRender::drawProgress()
{
    glColor4f(1.0,1.0,1.0,progressOpacity);

    QString prog;
    QString tot;
    QString type;

    if(renderContext->getDownsampler()->iteration < renderContext->getDownsampler()->scatterIterationMax)
    {
        type=QString("Scatter Read ");
        tot.setNum(renderContext->getDownsampler()->scatterIterationMax);
        prog.setNum(renderContext->getDownsampler()->iteration);
        renderText(width()-200,height()-20.0,0.0,type.append(prog).append("/").append(tot));

    }
    else if(renderContext->getDownsampler()->iteration < renderContext->getDownsampler()->sliceIterationMax+renderContext->getDownsampler()->scatterIterationMax)
    {
        type=QString("Slice Read ");
        tot.setNum(renderContext->getDownsampler()->sliceIterationMax);
        prog.setNum(renderContext->getDownsampler()->iteration-renderContext->getDownsampler()->scatterIterationMax);
        renderText(width()-200,height()-20.0,0.0,type.append(prog).append("/").append(tot));
    }
    else
    {
        renderText(width()-200,height()-20.0,0.0,"Complete");
    }

    for(int i=0;i<10;i++)
    {
        glPointSize(5);
        if(i<=progressCount)
            glPointSize(10);
        glBegin(GL_POINTS);
        glVertex2d(width()-210-(10-i)*15,height()-15);
        glEnd();
    }
}

//Set Clipping Region for volume renderer
void VolRender::setClip()
{
    makeCurrent();
    GLdouble plane[4] ;

    plane[0] = +1. ;  plane[1] =  0. ;  plane[2] =  0. ;  plane[3] = (renderContext->volumewidth/2.0f)-renderContext->pShift[0].y()-FLT_EPSILON;
    glClipPlane( GL_CLIP_PLANE0, plane ) ;

    plane[0] = -1. ;  plane[1] =  0. ;  plane[2] =  0. ;  plane[3] = (renderContext->volumewidth/2.0f)+renderContext->pShift[0].x()-FLT_EPSILON;
    glClipPlane( GL_CLIP_PLANE1, plane ) ;

    plane[0] =  0. ;  plane[1] = +1. ;  plane[2] =  0. ;  plane[3] = renderContext->volumeheight/2.0f -renderContext->pShift[1].y()-FLT_EPSILON;
    glClipPlane( GL_CLIP_PLANE2, plane ) ;

    plane[0] =  0. ;  plane[1] = -1. ;  plane[2] =  0. ;  plane[3] = renderContext->volumeheight/2.0f+renderContext->pShift[1].x()-FLT_EPSILON;
    glClipPlane( GL_CLIP_PLANE3, plane ) ;

    plane[0] =  0. ;  plane[1] =  0. ;  plane[2] = +1. ;  plane[3] = renderContext->volumedepth/2.0f -renderContext->pShift[2].y()-FLT_EPSILON;
    glClipPlane( GL_CLIP_PLANE4, plane ) ;

    plane[0] =  0. ;  plane[1] =  0. ;  plane[2] = -1. ;  plane[3] = renderContext->volumedepth/2.0f+renderContext->pShift[2].x()-FLT_EPSILON;
    glClipPlane( GL_CLIP_PLANE5, plane ) ;
}

//Set Clipping mode
void VolRender::clipToggle(bool b)
{
    makeCurrent();
        if(!b)
        {
            glDisable( GL_CLIP_PLANE0 ) ;
            glDisable( GL_CLIP_PLANE1 ) ;
            glDisable( GL_CLIP_PLANE2 ) ;
            glDisable( GL_CLIP_PLANE3 ) ;
            glDisable( GL_CLIP_PLANE4 ) ;
            glDisable( GL_CLIP_PLANE5 ) ;       
        }
        else
        {
            glEnable( GL_CLIP_PLANE0 ) ;
            glEnable( GL_CLIP_PLANE1 ) ;
            glEnable( GL_CLIP_PLANE2 ) ;
            glEnable( GL_CLIP_PLANE3 ) ;
            glEnable( GL_CLIP_PLANE4 ) ;
            glEnable( GL_CLIP_PLANE5 ) ;
        }
}

void VolRender::setTransferMap(float** tex,int * rgba, int t)
{
    int count=0;
    for(int i=0;i<4; i++)
    {
        if(rgba[i])
        {
            setTransferTexture(tex[count], i, t);
            count++;
        }
            
    }  
}

//
void VolRender::setTransferTexture(float* tex, int c,int t)
{
    makeCurrent();
    glDeleteTextures(1, &colourMap[c]);
    glGenTextures(1, &colourMap[c]);
    glActiveTextureARB( GL_TEXTURE1+c);
   
    glBindTexture(GL_TEXTURE_1D, colourMap[c]);

    glTexImage1D(GL_TEXTURE_1D, 0, GL_INTENSITY, t, 0, GL_RED, GL_FLOAT, tex);

    glTexParameteri( GL_TEXTURE_1D,GL_TEXTURE_WRAP_S, GL_CLAMP );
    
    glTexParameteri( GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    shader->shaderProgram->bind();

    switch(c)
    {
        case 0:
            shader->shaderProgram->setUniformValue("redLUT",  1+c );
        break;
        case 1:
            shader->shaderProgram->setUniformValue("greenLUT",  1+c );
        break;
        case 2:
            shader->shaderProgram->setUniformValue("blueLUT",  1+c );
        break;
        case 3:
            shader->shaderProgram->setUniformValue("alphaLUT",  1+c );
        break;
    }
    
    shader->shaderProgram->release();
    glActiveTextureARB(GL_TEXTURE0);
    updateDisplay();
}

//Set Render Texture
void VolRender::setTex (int subImage) 
{
    makeCurrent();

    if(subImage==-1)
    {
        newTex();
    }     
    else
    {
        subTex(subImage);
    }   
    updateDisplay();    
}

void VolRender::initTex()
{
    float * texture = new float[renderContext->volumewidth*renderContext->volumeheight*renderContext->volumedepth];
    memset(texture,0,renderContext->volumewidth*renderContext->volumeheight*renderContext->volumedepth*4);

    glActiveTextureARB( GL_TEXTURE0);
    //Texture Creation
    glDeleteTextures(1, &planeTexture);
    glGenTextures(1, &planeTexture);
    
    glEnable (GL_TEXTURE_3D);

    glBindTexture(GL_TEXTURE_3D, planeTexture);

    glTexImage3D(GL_TEXTURE_3D, 0, GL_INTENSITY, renderContext->volumewidth,renderContext->volumeheight, renderContext->volumedepth, 0, GL_RED, GL_FLOAT, texture);    

    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    glDisable (GL_TEXTURE_3D);
    delete [] texture;
}

void VolRender::newTex()
{
    float *texture = renderContext->getCube()->getFloats();

    glActiveTextureARB( GL_TEXTURE0);
    //Texture Creation
    glDeleteTextures(1, &planeTexture);
    glGenTextures(1, &planeTexture);
    
    glEnable (GL_TEXTURE_3D);

    glBindTexture(GL_TEXTURE_3D, planeTexture);

    glTexImage3D(GL_TEXTURE_3D, 0, GL_INTENSITY, renderContext->volumewidth,renderContext->volumeheight, renderContext->volumedepth, 0, GL_RED, GL_FLOAT, texture);    

    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    glDisable (GL_TEXTURE_3D);
}

void VolRender::subTex(int subImage)
{
    float *texture = extractVolumeSlice(0,subImage,subImage+1);;

    glActiveTextureARB( GL_TEXTURE0);
    
    glEnable (GL_TEXTURE_3D);

    glBindTexture(GL_TEXTURE_3D, planeTexture);

    glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, subImage, renderContext->volumewidth,renderContext->volumeheight, 1, GL_RED, GL_FLOAT, texture);      

    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_3D_EXT,GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    glDisable (GL_TEXTURE_3D);

    delete [] texture;
}

float* VolRender::extractVolumeSlice(int orientation, int slicestart,int sliceend)
{
    float * extract = NULL;
    int length = 0;
    //dprintf("%d %d %d\n", orientation,slicestart,sliceend);
    switch(orientation)
    {
        case 0:
            length=renderContext->getCube()->width*renderContext->getCube()->height*(sliceend-slicestart);
            extract = new float[length];
            for(int i=0;i<length;i++)
            {
                extract[i]=renderContext->getCube()->getFloats()[i+(renderContext->getCube()->width*renderContext->getCube()->height*slicestart)];
            }
        break;
        case 1:

        break;
        case 2:

        break;
    }
    return extract;
}

//Input Phase
void VolRender::updateGL()
{
    //circular+=0.5f;
    
    //Perform all input functions
    updateInput();
    //Find nearest plane face
    nearPlane=calculateNearPlane(circular,vertical);

    if(leftDown)
    {
        if(HUDElement>=0)
            HUDActions();
    }
}

//Handle Input Queue
void VolRender::updateInput()
{
    while(inputQueue.size()>0)
    {
        switch(inputQueue.front()->Type)
        {
            //Left Press
            case 0:
                leftPress(inputQueue.front());
            break;
            //Right Press
            case 1:
                rightPress(inputQueue.front());
            break;
            //Left Release
            case 2:
                leftRelease(inputQueue.front());
            break;
            //Right Release
            case 3:
                rightRelease(inputQueue.front());
            break;
            //Left Double Click
            case 4:
                leftDoubleClick(inputQueue.front());
            break;
            //Right Double Click
            case 5:
                rightDoubleClick(inputQueue.front());
            break;
            //Mouse Move
            case 6:
                mouseDrag(inputQueue.front());
                if(rightDown)
                    rightMouseDrag(inputQueue.front());
                else if(leftDown)
                    leftMouseDrag(inputQueue.front());
            break;
            //Mouse Wheel
            case 7:
                wheelScroll(inputQueue.front());
            break;
            //Keyboard Press
            case 8:

            break;
        }
        delete inputQueue.front();
        inputQueue.pop();
    }
}

void VolRender::updateProgress()
{
    if(renderContext->getDownsampler()->iteration < renderContext->getDownsampler()->scatterIterationMax)
    {
        progressOpacity=std::min(progressOpacity+0.05f, 1.0f);
        progressCount=renderContext->getImage()->getParent()->progress()*10;
    }
    else if(renderContext->getDownsampler()->iteration < renderContext->getDownsampler()->sliceIterationMax+renderContext->getDownsampler()->scatterIterationMax)
    {
        progressOpacity=std::min(progressOpacity+0.05f, 1.0f);
        progressCount=((renderContext->getDownsampler()->iteration-renderContext->getDownsampler()->scatterIterationMax)/(float)(renderContext->getDownsampler()->sliceIterationMax))*10;
    }
    else
    {
        progressOpacity=std::max(progressOpacity-0.05f, 0.0f);
        if(progressOpacity == 0.0f)
            killTimer(progressTimerID);
    }
    updateDisplay();
}

//Calculate which of the 6 faces is currently closest to the camera
int VolRender::calculateNearPlane(float circ,float vert)
{
    int nP = 0;
    switch(((int)((circ/45.0f+1)/2.0f))%4)
    {
        case 0:
            nP=0;
        break;
        case 1:
            nP=4;
        break;
        case 2:
            nP=1;
        break;
        case 3:
            nP=5;
        break;
    }
    switch(std::max(-1,std::min(1,(int)(vert/45.1f))))
    {
        case -1:
            nP=2;
        break;
        case 1:
            nP=3;
        break;
    }
    /*if(nP!=nearPlane)
    {
        switch(closestPlane)
        {
            case 0:
                shiftAid[0]=renderContext->pShift[2].y()/renderContext->volumedepth;
                shiftAid[1]=(renderContext->volumedepth+renderContext->pShift[2].x())/renderContext->volumedepth;
            break;
            case 1:
                shiftAid[0]=-renderContext->pShift[2].x()/renderContext->volumedepth;
                shiftAid[1]=(renderContext->volumedepth-renderContext->pShift[2].y())/renderContext->volumedepth;
            break;
            case 2:
                shiftAid[0]=-renderContext->pShift[1].x()/renderContext->volumeheight;
                shiftAid[1]=(renderContext->volumeheight-renderContext->pShift[1].y())/renderContext->volumeheight;
            break;
            case 3:
                shiftAid[0]=renderContext->pShift[1].y()/renderContext->volumeheight;
                shiftAid[1]=(renderContext->volumeheight+renderContext->pShift[1].x())/renderContext->volumeheight;
            break;
            case 4:
                shiftAid[0]=-renderContext->pShift[0].x()/renderContext->volumewidth;
                shiftAid[1]=(renderContext->volumewidth-renderContext->pShift[0].y())/renderContext->volumewidth;
            break;
            case 5:
                shiftAid[0]=renderContext->pShift[0].y()/renderContext->volumewidth;
                shiftAid[1]=(renderContext->volumewidth+renderContext->pShift[0].x())/renderContext->volumewidth;
            break;
        }
    }*/
    return nP;
}

//Find which volume face is being moused over
int VolRender::volumeHitDetection()
{
    initSelectMode(mousePoint.x(),mousePoint.y());
    PerspectiveTransform();
    clearModelView();

    applyRotation();

    volumeSelectRender();

    int hit=endSelectMode();

    if(hit)
        return findNameHit(hit,selectBuffer);
    return -1;

}

//HUD Actions
void VolRender::HUDActions()
{
    switch(HUDElement)
    {
        case 0:
            changeVerticalRotation(1.0);
        break;
        case 1:
            changeCircularRotation(1.0);
        break;
        case 2:
            changeVerticalRotation(-1.0);
        break;
        case 3:
            changeCircularRotation(-1.0);
        break;
        case 4:
            circular=(int)((circular/45.0f+1)/2.0f)*90.0f;
            vertical=std::max(-1,std::min(1,(int)(vertical/45.1f)))*90.0f;
            updateDisplay();
        break;
    }
}

//Find which hud element is being selected
int VolRender::HUDHitDetection(int x,int y)
{
    double hudBox= std::min(150,(int)(std::max(this->width(),this->height())*0.15));
    hudBox-=((int)hudBox)%2;

    if(!contains(x,y,this->width()-hudBox*1.2,this->height()-hudBox*1.2,hudBox,hudBox))
        return -1;
    
    if(pow(60*(hudBox/150.0)/2.0,2)>=pow((x-(this->width()-hudBox*0.7)),2)+pow((y-(this->height()-hudBox*0.7)),2))
        return 4;


    initSelectMode(x,y);
    OrthoTransform();
    clearModelView();

    drawHUD(false);

    int hit=endSelectMode();

    if(hit)
        return findNameHit(hit,selectBuffer);
    return -1;
}

//Rect contains
bool VolRender::contains(int x,int y, int px,int py,int w,int h)
{
    if((x>px&&x<px+w)&&(y>py&&y<py+h))
        return true;
    return false;
}

//Start selection mode
void VolRender::initSelectMode(int cursorX, int cursorY) 
{
    GLint vp[4];

    glSelectBuffer(BUFSIZE,selectBuffer);
    glRenderMode(GL_SELECT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glGetIntegerv(GL_VIEWPORT,vp);
    gluPickMatrix(cursorX-1,vp[3]-cursorY-1,2,2,vp);
    glInitNames();  
}

int VolRender::endSelectMode() 
{
    int hits;

    //clearProjection();
    //clearModelView();
    glFlush();

    hits = glRenderMode(GL_RENDER);
    return hits;
}

int VolRender::findNameHit (GLint hits, GLuint buffer[])
{
    int i;
    GLuint names = 0;
    GLuint *ptr = (GLuint *) buffer;
    GLuint minZ = 0xffffffff;
    GLuint *ptrNames = NULL;
    //GLuint numberOfNames;
    
    for (i = 0; i < hits; i++) {    
        names = *ptr;
        ptr++;
        if (*ptr < minZ) {
            //numberOfNames = names;
            minZ = *ptr;
            ptrNames = ptr+2;
        }
        ptr += names+2;
    }
    ptr = ptrNames;
    int* n = (int*)(ptr);
    return *n;
}

//Selection render mode for volume
void VolRender::volumeSelectRender()
{

    glEnable(GL_DEPTH_TEST);
    glPushName(1);

    drawFace(0,false);

    glPopName();
    glPushName(0);

    drawFace(0,true);

    glPopName();
    glPushName(2);

    drawFace(1,true);

    glPopName();
    glPushName(3);

    drawFace(1,false);

    glPopName();
    glPushName(4);

    drawFace(2,true);

    glPopName();
    glPushName(5);

    drawFace(2,false);

    glPopName();
    glDisable(GL_DEPTH_TEST);
}

//Draw faces of volume
void VolRender::drawFace(int n,bool reverse)
{
    float floatArray [12];
    float texArray [12];
    switch(n)
    {
        case 0://xy plane
            floatArray[0]=renderContext->volumewidth/2.0f+renderContext->pShift[0].x();floatArray[1]=renderContext->volumeheight/2.0f+renderContext->pShift[1].x();floatArray[2]=0;
            floatArray[3]=-renderContext->volumewidth/2.0f+renderContext->pShift[0].y();floatArray[4]=renderContext->volumeheight/2.0f+renderContext->pShift[1].x();floatArray[5]=0;
            floatArray[6]=-renderContext->volumewidth/2.0f+renderContext->pShift[0].y();floatArray[7]=-renderContext->volumeheight/2.0f+renderContext->pShift[1].y();floatArray[8]=0;
            floatArray[9]=renderContext->volumewidth/2.0f+renderContext->pShift[0].x();floatArray[10]=-renderContext->volumeheight/2.0f+renderContext->pShift[1].y();floatArray[11]=0;

            texArray[0]=1.0f-(renderContext->volumewidth-1+renderContext->pShift[0].x())/renderContext->volumewidth;texArray[1]=1.0f-(renderContext->volumeheight-1+renderContext->pShift[1].x())/renderContext->volumeheight;
            texArray[3]=1.0f-(renderContext->pShift[0].y()/renderContext->volumewidth);texArray[4]=1.0f-(renderContext->volumeheight-1+renderContext->pShift[1].x())/renderContext->volumeheight;
            texArray[6]=1.0f-(renderContext->pShift[0].y()/renderContext->volumewidth);texArray[7]=1.0f-(renderContext->pShift[1].y()/renderContext->volumeheight);
            texArray[9]=1.0f-(renderContext->volumewidth-1+renderContext->pShift[0].x())/renderContext->volumewidth;texArray[10]=1.0f-(renderContext->pShift[1].y()/renderContext->volumeheight);

            if(!reverse)
            {
                glPushMatrix();
                glTranslatef(0,0,renderContext->volumedepth/2+renderContext->pShift[2].x());
                texArray[2]=texArray[5]=texArray[8]=texArray[11]=(renderContext->volumedepth-1+renderContext->pShift[2].x())/renderContext->volumedepth;
            }
            else
            {
                glPushMatrix();
                glTranslatef(0,0,-renderContext->volumedepth/2+renderContext->pShift[2].y());
                texArray[2]=texArray[5]=texArray[8]=texArray[11]=((renderContext->pShift[2].y()+1)/renderContext->volumedepth);
            }
            drawPrimitive(GL_QUADS,4,floatArray,texArray,reverse);
            glPopMatrix();
            break;
        case 1://xz plane
            floatArray[3]=renderContext->volumewidth/2.0f+renderContext->pShift[0].x();floatArray[1]=0;floatArray[5]=renderContext->volumedepth/2.0f+renderContext->pShift[2].x();
            floatArray[6]=-renderContext->volumewidth/2.0f+renderContext->pShift[0].y();floatArray[4]=0;floatArray[8]=renderContext->volumedepth/2.0f+renderContext->pShift[2].x();
            floatArray[9]=-renderContext->volumewidth/2.0f+renderContext->pShift[0].y();floatArray[7]=-0;floatArray[11]=-renderContext->volumedepth/2.0f+renderContext->pShift[2].y();
            floatArray[0]=renderContext->volumewidth/2.0f+renderContext->pShift[0].x();floatArray[10]=-0;floatArray[2]=-renderContext->volumedepth/2.0f+renderContext->pShift[2].y();

            texArray[0]=1.0f-(renderContext->volumewidth-1+renderContext->pShift[0].x())/renderContext->volumewidth;texArray[2]=0.0f+(renderContext->pShift[2].y()/renderContext->volumedepth);
            texArray[3]=1.0f-(renderContext->volumewidth-1+renderContext->pShift[0].x())/renderContext->volumewidth;texArray[5]=0.0f+(renderContext->volumedepth-1+renderContext->pShift[2].x())/renderContext->volumedepth;
            texArray[6]=1.0f-(renderContext->pShift[0].y()/renderContext->volumewidth);texArray[8]=0.0f+(renderContext->volumedepth-1+renderContext->pShift[2].x())/renderContext->volumedepth;
            texArray[9]=1.0f-(renderContext->pShift[0].y()/renderContext->volumewidth);texArray[11]=0.0f+(renderContext->pShift[2].y()/renderContext->volumedepth);

            if(!reverse)
            {
                glPushMatrix();
                glTranslatef(0,-renderContext->volumeheight/2+renderContext->pShift[1].y(),0);
                texArray[1]=texArray[4]=texArray[7]=texArray[10]=(renderContext->volumeheight-1-renderContext->pShift[1].y())/renderContext->volumeheight;
            }
            else
            {
                glPushMatrix();
                glTranslatef(0,renderContext->volumeheight/2+renderContext->pShift[1].x(),0);
                texArray[1]=texArray[4]=texArray[7]=texArray[10]=-renderContext->pShift[1].x()/renderContext->volumeheight;
            }
            drawPrimitive(GL_QUADS,4,floatArray,texArray,reverse);
            glPopMatrix();
            break;
        case 2://yz plane
            floatArray[0]=0;floatArray[10]=renderContext->volumeheight/2.0f+renderContext->pShift[1].x();floatArray[11]=renderContext->volumedepth/2.0f+renderContext->pShift[2].x();
            floatArray[3]=0;floatArray[1]=renderContext->volumeheight/2.0f+renderContext->pShift[1].x();floatArray[2]=-renderContext->volumedepth/2.0f+renderContext->pShift[2].y();
            floatArray[6]=0;floatArray[4]=-renderContext->volumeheight/2.0f+renderContext->pShift[1].y();floatArray[5]=-renderContext->volumedepth/2.0f+renderContext->pShift[2].y();
            floatArray[9]=0;floatArray[7]=-renderContext->volumeheight/2.0f+renderContext->pShift[1].y();floatArray[8]=renderContext->volumedepth/2.0f+renderContext->pShift[2].x();

            texArray[1]=1.0f-(renderContext->volumeheight-1+renderContext->pShift[1].x())/renderContext->volumeheight;texArray[2]=0.0f+(renderContext->pShift[2].y()/renderContext->volumedepth);
            texArray[4]=1.0f-(renderContext->pShift[1].y()/renderContext->volumeheight);texArray[5]=0.0f+(renderContext->pShift[2].y()/renderContext->volumedepth);
            texArray[7]=1.0f-(renderContext->pShift[1].y()/renderContext->volumeheight);texArray[8]=0.0f+(renderContext->volumedepth-1+renderContext->pShift[2].x())/renderContext->volumedepth;
            texArray[10]=1.0f-(renderContext->volumeheight-1+renderContext->pShift[1].x())/renderContext->volumeheight;texArray[11]=0.0f+(renderContext->volumedepth-1+renderContext->pShift[2].x())/renderContext->volumedepth;

            if(!reverse)
            {
                glPushMatrix();
                glTranslatef(-renderContext->volumewidth/2+renderContext->pShift[0].y(),0,0);
                texArray[0]=texArray[3]=texArray[6]=texArray[9]=(renderContext->volumewidth-1-renderContext->pShift[0].y())/renderContext->volumewidth;
            }
            else
            {
                glPushMatrix();
                glTranslatef(renderContext->volumewidth/2+renderContext->pShift[0].x(),0,0);
                texArray[0]=texArray[3]=texArray[6]=texArray[9]=-renderContext->pShift[0].x()/renderContext->volumewidth;
            }
            drawPrimitive(GL_QUADS,4,floatArray,texArray,reverse);
            glPopMatrix();
            break;
    }
}

void VolRender::drawPrimitive(GLenum primitive, int vertexCount,float* points,float* texCoords,bool reverse)
{
    glBegin(primitive);
    if(!reverse)
    {
        for(int i=0;i<vertexCount;i++)
        {
            glTexCoord3f(texCoords[i*3], texCoords[i*3+1], texCoords[i*3+2]);
            glVertex3f(points[i*3], points[i*3+1], points[i*3+2]);
        }   
    }
    else
    {
        for(int i=vertexCount-1;i>=0;i--)
        {
            glTexCoord3f(texCoords[i*3], texCoords[i*3+1], texCoords[i*3+2]);
            glVertex3f(points[i*3], points[i*3+1], points[i*3+2]);
        }   
    }

    glEnd();
}

//Return location of intersection with volume
Coordinate VolRender::volumeIntersection()
{
    selectPlane=volumeHitDetection();
    return planeIntersection(selectPlane);
}

//Find pinpoint location of plane intersection
Coordinate VolRender::planeIntersection(int planeNum)
{
    glGetDoublev (GL_MODELVIEW_MATRIX , model);
    glGetDoublev (GL_PROJECTION_MATRIX, proj );
    glGetIntegerv(GL_VIEWPORT, view );

    double planeN[3];
    double planePos[3];
    
    double nearX,nearY,nearZ;
    gluUnProject( mousePoint.x(),this->height()-mousePoint.y(),zmin,model, proj, view, &nearX, &nearY, &nearZ);
    double farX,farY,farZ;
    gluUnProject( mousePoint.x(),this->height()-mousePoint.y(),zmax,model, proj, view, &farX, &farY, &farZ);
    
    double rayDir[3]={farX-nearX,farY-nearY,farZ-nearZ};
    double rayS[3]={nearX,nearY,nearZ};
    
    Coordinate c;
    c.Valid=true;
    c.Plane=planeNum;
    switch(planeNum)
    {
        case -1:
            c.Valid=false;
            return c;
        break;
        case 0:
            planeN[0]=0.0;planeN[1]=0.0;planeN[2]=1.0;
            planePos[0]=0.0;planePos[1]=0.0;planePos[2]=-renderContext->volumedepth/2.0f+renderContext->pShift[2].y();
        break;
        case 1:
            planeN[0]=0.0;planeN[1]=0.0;planeN[2]=-1.0;
            planePos[0]=0.0;planePos[1]=0.0;planePos[2]=renderContext->volumedepth/2.0f+renderContext->pShift[2].x();
        break;
        case 2:
            planeN[0]=0.0;planeN[1]=-1.0;planeN[2]=0.0;
            planePos[0]=0.0;planePos[1]=renderContext->volumeheight/2.0f+renderContext->pShift[1].x();planePos[2]=0.0;
        break;
        case 3:
            planeN[0]=0.0;planeN[1]=1.0;planeN[2]=0.0;
            planePos[0]=0.0;planePos[1]=-renderContext->volumeheight/2.0f+renderContext->pShift[1].y();planePos[2]=0.0;
        break;
        case 4:
            planeN[0]=-1.0;planeN[1]=0.0;planeN[2]=0.0;
            planePos[0]=renderContext->volumewidth/2.0f+renderContext->pShift[0].x();planePos[1]=0.0;planePos[2]=0.0;
        break;
        case 5:
            planeN[0]=1.0;planeN[1]=0.0;planeN[2]=0.0;
            planePos[0]=-renderContext->volumewidth/2.0f+renderContext->pShift[0].y();planePos[1]=0.0;planePos[2]=0.0;
        break;
    }
    double lambda=intersectionCalculation(planeN,planePos,rayDir,rayS);
    
    c.x=rayS[0]+rayDir[0]*lambda;
    c.y=rayS[1]+rayDir[1]*lambda;
    c.z=rayS[2]+rayDir[2]*lambda;
    return c;
}

//Plane collision point calculation
double VolRender::intersectionCalculation(double *planeNormal,double *planePosition,double *rayDirection, double *rayStart)
{
    double DotProduct = dotProduct(planeNormal,rayDirection);
    double  c[3]={planePosition[0]-rayStart[0],planePosition[1]-rayStart[1],planePosition[2]-rayStart[2]};
    return dotProduct(planeNormal,c)/DotProduct;
}

//Find Dot Product
double VolRender::dotProduct(double * a,double * b)
{
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

void VolRender::volumeEdgeCollision(Coordinate c)
{
    
    int edgeLoc[4];
    edgeLoc[0] = edgeLoc[1] = edgeLoc[2] = edgeLoc[3] = 0;

    double one,two;
    one = two = 0.0; 

    int originalX = selectedEdge.x();
    int originalY = selectedEdge.y();
    
    switch((int)floor(nearPlane/2))
    {
        case 0:
            edgeLoc[0]=renderContext->volumewidth/2.0+renderContext->pShift[0].x();
            edgeLoc[1]=-renderContext->volumewidth/2.0+renderContext->pShift[0].y();
            edgeLoc[2]=renderContext->volumeheight/2.0+renderContext->pShift[1].x();
            edgeLoc[3]=-renderContext->volumeheight/2.0+renderContext->pShift[1].y();

            one=c.x;
            two=c.y;
            
            //If the left mouse button is currently down, don't reset the selected edges
            if(!leftDown)
                selectedEdge=QPoint(0,0);

            switch(selectedEdge.x())
            {
                case 0:

                    if((abs(one-edgeLoc[0])<=renderContext->volumewidth/200.0 && std::max(abs(two-edgeLoc[2]),abs(two-edgeLoc[3]))<=renderContext->volumeheight+1.5))
                        selectedEdge.setX(1);//dprintf("Hit on 1\n");
                    else if((abs(one-edgeLoc[1])<=renderContext->volumewidth/200.0 && std::max(abs(two-edgeLoc[2]),abs(two-edgeLoc[3]))<=renderContext->volumeheight+1.5))
                        selectedEdge.setX(2);//dprintf("Hit on 2\n");

                break;
                case 1:
                    dragDisplacement.Displacement.setX(one-edgeLoc[0]);
                break;
                case 2:
                    dragDisplacement.Displacement.setX(one-edgeLoc[1]);
                break;
            }

            switch(selectedEdge.y())
            {
                case 0:
                    if((abs(two-edgeLoc[2])<=renderContext->volumeheight/200.0 && std::max(abs(one-edgeLoc[1]),abs(one-edgeLoc[0]))<=renderContext->volumewidth+1.5))
                        selectedEdge.setY(2);//dprintf("Hit on 3\n");
                    else if((abs(two-edgeLoc[3])<=renderContext->volumeheight/200.0 && std::max(abs(one-edgeLoc[1]),abs(one-edgeLoc[0]))<=renderContext->volumewidth+1.5))
                        selectedEdge.setY(1);//dprintf("Hit on 4\n");
                break;
                case 1:
                    dragDisplacement.Displacement.setY(two-edgeLoc[3]);
                break;
                case 2:
                    dragDisplacement.Displacement.setY(two-edgeLoc[2]);
                break;
            }

        break;
        case 1:
            edgeLoc[0]=renderContext->volumewidth/2.0+renderContext->pShift[0].x();
            edgeLoc[1]=-renderContext->volumewidth/2.0+renderContext->pShift[0].y();
            edgeLoc[2]=renderContext->volumedepth/2.0+renderContext->pShift[2].x();
            edgeLoc[3]=-renderContext->volumedepth/2.0+renderContext->pShift[2].y();

            one=c.x;
            two=c.z;

            //If the left mouse button is currently down, don't reset the selected edges
            if(!leftDown)
                selectedEdge=QPoint(0,0);

            switch(selectedEdge.x())
            {
                case 0:

                    if((abs(one-edgeLoc[0])<=renderContext->volumewidth/200.0 && std::max(abs(two-edgeLoc[2]),abs(two-edgeLoc[3]))<=renderContext->volumedepth+1.5))
                        selectedEdge.setX(1);//dprintf("Hit on 1\n");
                    else if((abs(one-edgeLoc[1])<=renderContext->volumewidth/200.0 && std::max(abs(two-edgeLoc[2]),abs(two-edgeLoc[3]))<=renderContext->volumedepth+1.5))
                        selectedEdge.setX(2);//dprintf("Hit on 2\n");

                break;
                case 1:
                    dragDisplacement.Displacement.setX(one-edgeLoc[0]);
                break;
                case 2:
                    dragDisplacement.Displacement.setX(one-edgeLoc[1]);
                break;
            }

            switch(selectedEdge.y())
            {
                case 0:
                    if((abs(two-edgeLoc[2])<=renderContext->volumedepth/200.0 && std::max(abs(one-edgeLoc[1]),abs(one-edgeLoc[0]))<=renderContext->volumewidth+1.5))
                        selectedEdge.setY(2);//dprintf("Hit on 3\n");
                    else if((abs(two-edgeLoc[3])<=renderContext->volumedepth/200.0 && std::max(abs(one-edgeLoc[1]),abs(one-edgeLoc[0]))<=renderContext->volumewidth+1.5))
                        selectedEdge.setY(1);//dprintf("Hit on 4\n");
                break;
                case 1:
                    dragDisplacement.Displacement.setY(two-edgeLoc[3]);
                break;
                case 2:
                    dragDisplacement.Displacement.setY(two-edgeLoc[2]);
                break;
            }

        break;
        case 2:
            edgeLoc[0]=renderContext->volumedepth/2.0+renderContext->pShift[2].x();
            edgeLoc[1]=-renderContext->volumedepth/2.0+renderContext->pShift[2].y();
            edgeLoc[2]=renderContext->volumeheight/2.0+renderContext->pShift[1].x();
            edgeLoc[3]=-renderContext->volumeheight/2.0+renderContext->pShift[1].y();

            one=c.z;
            two=c.y;

            //If the left mouse button is currently down, don't reset the selected edges
            if(!leftDown)
                selectedEdge=QPoint(0,0);

            switch(selectedEdge.x())
            {
                case 0:

                    if((abs(one-edgeLoc[0])<=renderContext->volumedepth/200.0 && std::max(abs(two-edgeLoc[2]),abs(two-edgeLoc[3]))<=renderContext->volumeheight+1.5))
                        selectedEdge.setX(1);//dprintf("Hit on 1\n");
                    else if((abs(one-edgeLoc[1])<=renderContext->volumedepth/200.0 && std::max(abs(two-edgeLoc[2]),abs(two-edgeLoc[3]))<=renderContext->volumeheight+1.5))
                        selectedEdge.setX(2);//dprintf("Hit on 2\n");

                break;
                case 1:
                    dragDisplacement.Displacement.setX(one-edgeLoc[0]);
                break;
                case 2:
                    dragDisplacement.Displacement.setX(one-edgeLoc[1]);
                break;
            }

            switch(selectedEdge.y())
            {
                case 0:
                    if((abs(two-edgeLoc[2])<=renderContext->volumeheight/200.0 && std::max(abs(one-edgeLoc[1]),abs(one-edgeLoc[0]))<=renderContext->volumedepth+1.5))
                        selectedEdge.setY(2);//dprintf("Hit on 3\n");
                    else if((abs(two-edgeLoc[3])<=renderContext->volumeheight/200.0 && std::max(abs(one-edgeLoc[1]),abs(one-edgeLoc[0]))<=renderContext->volumedepth+1.5))
                        selectedEdge.setY(1);//dprintf("Hit on 4\n");
                break;
                case 1:
                    dragDisplacement.Displacement.setY(two-edgeLoc[3]);
                break;
                case 2:
                    dragDisplacement.Displacement.setY(two-edgeLoc[2]);
                break;
            }
        break;
    }
    if(selectedEdge.x()!=originalX||selectedEdge.y()!=originalY)
        updateDisplay();
}

//move select plane shift units
void VolRender::shiftPlane(int plane,int side,int shift)
{
    if(side==0)
    {
        if(shift>0)
            renderContext->pShift[plane].setX(std::min(0,renderContext->pShift[plane].x()+shift));
        else
            renderContext->pShift[plane].setX(std::max((int)-renderContext->getVolumeByPlane(plane)+1+renderContext->pShift[plane].y(),renderContext->pShift[plane].x()+shift));
        emit(planeShifted(plane,side,renderContext->pShift[plane].x()/(double)renderContext->getVolumeByPlane(plane)));
    }
    else
    {
        if(shift>0)
            renderContext->pShift[plane].setY(std::max(0,renderContext->pShift[plane].y()-shift));
        else
            renderContext->pShift[plane].setY(std::min(((int)renderContext->getVolumeByPlane(plane)-1)+renderContext->pShift[plane].x(),renderContext->pShift[plane].y()-shift));
        emit(planeShifted(plane,side,renderContext->pShift[plane].y()/(double)renderContext->getVolumeByPlane(plane)));
    }

}

//Mouse Drag action on volume edge
void VolRender::volumeEdgeDrag(int x,int y)
{
    switch((int)floor(nearPlane/2))
    {
        case 0:
                if(selectedEdge.x()==1)
                    shiftPlane(0,0,(int)dragDisplacement.Displacement.x());
                if(selectedEdge.x()==2)
                    shiftPlane(0,1,-(int)dragDisplacement.Displacement.x());
                if(selectedEdge.y()==2)
                    shiftPlane(1,0,(int)dragDisplacement.Displacement.y());
                if(selectedEdge.y()==1)
                    shiftPlane(1,1,-(int)dragDisplacement.Displacement.y());
        break;
        case 1:
                if(selectedEdge.x()==1)
                    shiftPlane(0,0,(int)dragDisplacement.Displacement.x());
                if(selectedEdge.x()==2)
                    shiftPlane(0,1,-(int)dragDisplacement.Displacement.x());
                if(selectedEdge.y()==2)
                    shiftPlane(2,0,(int)dragDisplacement.Displacement.y());
                if(selectedEdge.y()==1)
                    shiftPlane(2,1,-(int)dragDisplacement.Displacement.y());
        break;
        case 2:
                if(selectedEdge.x()==1)
                    shiftPlane(2,0,(int)dragDisplacement.Displacement.x());
                if(selectedEdge.x()==2)
                    shiftPlane(2,1,-(int)dragDisplacement.Displacement.x());
                if(selectedEdge.y()==2)
                    shiftPlane(1,0,(int)dragDisplacement.Displacement.y());
                if(selectedEdge.y()==1)
                    shiftPlane(1,1,-(int)dragDisplacement.Displacement.y());
        break;
    }
    updateDisplay();
}

//Mouse Press Event
void VolRender::mousePressEvent ( QMouseEvent * e )
{
    mousePoint=QPoint(e->x(),e->y());
    inputEvent * event = new inputEvent;
    if(e->button()==Qt::LeftButton)
        event->Type=LEFTCLICK;
    else if(e->button()==Qt::RightButton)
        event->Type=RIGHTCLICK;
    event->Position=QPoint(mousePoint.x(),mousePoint.y());
    inputQueue.push(event);
}

//Mouse Press Action
//Left
void VolRender::leftPress(inputEvent * e)
{
    //dprintf("Left Mouse Clicked\n");
    leftDown=true;
}
//Right
void VolRender::rightPress(inputEvent * e)
{
    //dprintf("Right Mouse Clicked\n");
    rightDown=true;
}

//Mouse Release Event
void VolRender::mouseReleaseEvent( QMouseEvent * e )
{
    mousePoint=QPoint(e->x(),e->y());
    inputEvent * event = new inputEvent;
    if(e->button()==Qt::LeftButton)
        event->Type=LEFTRELEASE;
    else if(e->button()==Qt::RightButton)
        event->Type=RIGHTRELEASE;
    event->Position=QPoint(mousePoint.x(),mousePoint.y());
    inputQueue.push(event);
}

//Mouse Release Action
//Left
void VolRender::leftRelease(inputEvent * e)
{
    //dprintf("Left Mouse Released\n");
    leftDown=false;
}
//Right
void VolRender::rightRelease(inputEvent * e)
{
    //dprintf("Right Mouse Released\n");
    rightDown=false;
}

//Double Click Event
void VolRender::mouseDoubleClickEvent ( QMouseEvent * e )
{
    mousePoint=QPoint(e->x(),e->y());
    inputEvent * event = new inputEvent;
    if(e->button()==Qt::LeftButton)
        event->Type=LEFTDOUBLE;
    else if(e->button()==Qt::RightButton)
        event->Type=RIGHTDOUBLE;
    event->Position = QPoint(mousePoint.x(),mousePoint.y());
    inputQueue.push(event);
}

//Mouse Double Click Action
//Left
void VolRender::leftDoubleClick(inputEvent * e)
{
    if(HUDElement==4)
    {
        circular=0.0;
        vertical=0.0;
        updateDisplay();
    }
    else
    {
        clearProjection();
        PerspectiveTransform();
        clearModelView();

        applyRotation();
        Coordinate c = volumeIntersection();
        setSelectorSize();
        setSelectorPosition(c);
        if(selectPlane>=0)
        {
            switch(selectPlane/2)
            {
                case 0:
                    emit selectorPlaced(selectPlane, (int)((renderContext->focalArea.Point.z+0.5)*renderContext->dbound), 
                                        renderContext->focalArea.Point.x*renderContext->wbound,renderContext->focalArea.Point.y*renderContext->hbound);
                break;
                case 1:
                    emit selectorPlaced(selectPlane+2+(selectPlane%2)*-2+1, (int)((renderContext->focalArea.Point.y+0.5)*renderContext->hbound), 
                                        renderContext->focalArea.Point.x*renderContext->wbound,renderContext->focalArea.Point.z*renderContext->dbound);
                break;
                case 2:
                    emit selectorPlaced(selectPlane-2, renderContext->wbound-(int)((renderContext->focalArea.Point.x+0.5)*renderContext->wbound), 
                                        renderContext->focalArea.Point.z*renderContext->dbound,renderContext->focalArea.Point.y*renderContext->hbound);
                break;
            }
        }
        
    } 
}

//Takes a collision point and creates the selection frame around it
void VolRender::setSelectorSize()
{
    switch(selectPlane/2)
    {
        case 0:
            renderContext->setSelectorWidth(renderContext->selectorWidth/(float)renderContext->wbound,0,0);
            renderContext->setSelectorHeight(0,renderContext->selectorHeight/(float)renderContext->hbound,0);
        break;
        case 1:
            renderContext->setSelectorWidth(renderContext->selectorWidth/(float)renderContext->wbound,0,0);
            renderContext->setSelectorHeight(0,0,renderContext->selectorHeight/(float)renderContext->dbound);
        break;
        case 2:
            renderContext->setSelectorWidth(0,0,renderContext->selectorHeight/(float)renderContext->dbound);
            renderContext->setSelectorHeight(0,renderContext->selectorHeight/(float)renderContext->hbound,0);
        break;
    }
    updateDisplay();
}

void VolRender::setSelectorPosition(Coordinate volumeIntersect)
{
    renderContext->focalArea.Point.Valid=volumeIntersect.Valid;
    if(volumeIntersect.Valid)
    {
        double x,y,z;
        
        switch(selectPlane/2)
        {
            case 0:
                x = (volumeIntersect.x)/renderContext->volumewidth;
                y = (volumeIntersect.y)/renderContext->volumeheight;
                renderContext->setSelectorPoint(x,y,volumeIntersect.z/renderContext->volumedepth);
                
            break;
            case 1:
                x=(volumeIntersect.x)/renderContext->volumewidth;
                z=(volumeIntersect.z)/renderContext->volumedepth;

                renderContext->setSelectorPoint(x,volumeIntersect.y/renderContext->volumeheight,z);
            break;
            case 2:
                z=(volumeIntersect.z)/renderContext->volumedepth;
                y=(volumeIntersect.y)/renderContext->volumeheight;

                renderContext->setSelectorPoint(volumeIntersect.x/renderContext->volumewidth,y,z);
            break;
        } 
    }
    updateDisplay();
}

//Right
void VolRender::rightDoubleClick(inputEvent * e)
{
    //dprintf("Right Mouse Double Clicked\n");
}

//Mouse Move Event
//Left Click - Selection
//Right Click - Rotation-Movement
void VolRender::mouseMoveEvent ( QMouseEvent * e )
{
    inputEvent * event = new inputEvent;
    event->Type=MOVE;
    event->Displacement = QPoint(mousePoint.x()-e->x(),mousePoint.y()-e->y());
    mousePoint=QPoint(e->x(),e->y());
    event->Position = QPoint(mousePoint.x(),mousePoint.y());
    inputQueue.push(event); 
}

//Mouse Move Action
//Plain Move
void VolRender::mouseDrag(inputEvent * e)
{
    clearProjection();
    PerspectiveTransform();
    clearModelView();

    applyRotation();
    volumeEdgeCollision(planeIntersection(nearPlane));
    
    int HudCheck = HUDHitDetection(mousePoint.x(),mousePoint.y());
    if(HUDElement != HudCheck)
        updateDisplay();
    HUDElement=HudCheck;
}
//Left Drag
void VolRender::leftMouseDrag(inputEvent * e)
{
    //dprintf("Left Mouse Drag\n");
    if(selectedEdge.x()!=0||selectedEdge.y()!=0)
        volumeEdgeDrag(e->Displacement.x(),e->Displacement.y());
}
//Right Drag
void VolRender::rightMouseDrag(inputEvent * e)
{
    //dprintf("Right Mouse Drag\n");

    if(e->Displacement.x()!=0)
    {
        changeCircularRotation(e->Displacement.x());
    }
    if(e->Displacement.y()!=0)
    {
        changeVerticalRotation(e->Displacement.y());
    }
}

void VolRender::changeVerticalRotation(float r)
{
    vertical=std::min(90.0f,std::max(-90.0f,vertical+r));
    updateDisplay();
}

void VolRender::changeCircularRotation(float r)
{
    circular=((int)(circular+360-r))%360;
    updateDisplay();
}

//Wheel Event
void VolRender::wheelEvent ( QWheelEvent * e )
{
    inputEvent * event = new inputEvent;
    event->Type=WHEEL;
    event->Displacement = QPoint(e->delta(),0);
    inputQueue.push(event); 
}

//Mouse Wheel Scroll Action
void VolRender::wheelScroll(inputEvent * e)
{
    /*dprintf("Mouse Wheel Scrolled ");
    if(e.Displacement.x()>0)
        printf("Up\n");
    else
        printf("Down\n");*/

    renderContext->zoomAmount=std::min(maxZoom,std::max((renderContext->zoomAmount-2.0f*(e->Displacement.x()/12)),minZoom));
    updateDisplay();
}

//Key Press Events
void VolRender::keyPressEvent( QKeyEvent *e )
{

}

void VolRender::enterEvent(QEvent *e)
{
    
    //timerID=startTimer(16);
    //setFocus();
}

void VolRender::leaveEvent(QEvent *e)
{
    //killTimer(timerID);
}

void VolRender::updateDisplay()
{
    displayHasChanged=true;
}
void VolRender::progressUpdate()
{
    progressTimerID = startTimer(150);
}

void VolRender::timerEvent(QTimerEvent *e)
{
    if(e->timerId() == displayTimerID)
    {
        makeCurrent();
        update();
    }
    else if(e->timerId() == progressTimerID)
    {
        updateProgress();
    }
}

void VolRender::toggleFrames()
{
    frameVisible=!frameVisible;
    updateDisplay();
}

void VolRender::toggleTransparent()
{
    blendSetting=1.0f-std::min(blendSetting,1.0f);//  a > b ? b : a;
    updateDisplay();
}

void VolRender::pauseDownsampler()
{
    renderContext->pauseDownsampler();
}

void VolRender::resumeDownsampler()
{
    renderContext->resumeDownsampler();
}

void VolRender::fitsImageHeaderLoadRequest(QTreeWidgetItem* i,int w,int h,int d,int s)
{
    if(i->type()==1001)
    {}
    if(i->type()==1002)
    {
        FitsImageHeader * f = (FitsImageHeader*)(i);
        VolumeContext * context = new VolumeContext(f);
        context->setInputSettings(w,h,d,s);
        activateNewContext(context);
    }
    if(i->type()==1003)
    {
        activateExistingContext((VolumeContext*)i);
    }
}

//This is triggered by the volume render file menu
//It in turn triggers the refine dialog responsible for conveying the volume dimensions
void VolRender::refineMenuTrigger()
{
    //renderContext->setBoundaries();
    int dim[3];
    renderContext->getDim(dim);
    emit(refineDialog(renderContext->getImage(),dim[0],dim[1] ,dim[2]));
}

//This is triggered when the refine dialog returns the volume dimensions
//It creates a new volume from the current one but with different downsampling boundaries
void VolRender::refineVolumeRequest(QTreeWidgetItem*,int w,int h,int d,int s)
{
    if(renderContext!=defaultContext)
    {
        VolumeContext *c = new VolumeContext(renderContext);
        c->setInputSettings(w,h,d,s);
        c->setBoundaries(renderContext);
        activateNewContext(c);
    }
}

void VolRender::activateNewContext(VolumeContext* c)
{
    ContextList.push_back(c);

    setRenderContext(c);
    initTex();
    renderContext->startDownsampler();
    emit(newContextActivated(renderContext));
    progressUpdate();

    updateDisplay();  
}

void VolRender::activateExistingContext(VolumeContext* c)
{
    setRenderContext(c);

    if(renderContext->getCube()!=NULL)
    {
        setTex(-1);  
        updateDisplay();   
    }

    renderContext->resumeDownsampler();

    setSelectorSize();
    Coordinate d;
    d.x=renderContext->focalArea.Point.x*renderContext->volumewidth;
    d.y=renderContext->focalArea.Point.y*renderContext->volumeheight;
    d.z=renderContext->focalArea.Point.z*renderContext->volumedepth;
    d.Valid=renderContext->focalArea.Point.Valid;
    setSelectorPosition(d);
    progressUpdate();

    updateDisplay();
}

void VolRender::DownsamplerComplete(double s)
{
    if(!(s<-1.0))
    {
        renderContext->setCube();
        renderContext->setVolumeDimension();
    }
    if(renderContext->getCube()!=NULL)
    {
        setTex(std::max(-1,(int)s )); 
    }
}

//Create Shader Program
//Parameters - vertex shader file, fragment shader file
void VolRender::createShaders(QString vshader, QString fshader)
{
    makeCurrent();
    if(shader->shaderProgram)
    {
        shader->shaderProgram->release();
        shader->shaderProgram->removeAllShaders();
    }
    else shader->shaderProgram = new QGLShaderProgram;

    if(shader->vertexShader)
    {
        delete shader->vertexShader;
        shader->vertexShader = NULL;
    }

    if(shader->fragmentShader)
    {
        delete shader->fragmentShader;
        shader->fragmentShader = NULL;
    }

    // load and compile vertex shader
    QFileInfo vsh(vshader);
    if(vsh.exists())
    {
        shader->vertexShader = new QGLShader(QGLShader::Vertex);
        if(shader->vertexShader->compileSourceFile(vshader))
            shader->shaderProgram->addShader(shader->vertexShader);
        else qWarning() << "Vertex Shader Error" << shader->vertexShader->log();
    }
    else qWarning() << "Vertex Shader source file " << vshader << " not found.";


    // load and compile fragment shader
    QFileInfo fsh(fshader);
    if(fsh.exists())
    {
        shader->fragmentShader = new QGLShader(QGLShader::Fragment);
        if(shader->fragmentShader->compileSourceFile(fshader))
            shader->shaderProgram->addShader(shader->fragmentShader);
        else qWarning() << "Fragment Shader Error" << shader->fragmentShader->log();
    }
    else qWarning() << "Fragment Shader source file " << fshader << " not found.";

    if(!shader->shaderProgram->link())
    {
        qWarning() << "Shader Program Linker Error" << shader->shaderProgram->log();
    }
}

void VolRender::connectInfoBox(InfoBox* info)
{
    info->addLabel(new QLabel("Datacube Dimensions"));
    info->skipLine();
    info->addLabel(imagewidth);
    info->addLabel(imageheight);
    info->addLabel(imagedepth);
    info->skipLine();
    info->addLabel(new QLabel("Volume Coverage"));
    info->skipLine();
    info->addLabel(widthcov);
    info->skipLine();
    info->addLabel(heightcov);
    info->skipLine();
    info->addLabel(depthcov);
}

void VolRender::setInfoLabels()
{
    dprintf("%d\n", renderContext->datawidth);
    imagewidth->setText("Width: "+QString::number(renderContext->datawidth));
    imageheight->setText("Height: "+QString::number(renderContext->dataheight));
    imagedepth->setText("Depth: "+QString::number(renderContext->datadepth));

    widthcov->setText("Width: "+QString::number(renderContext->xbound)+QString::fromUtf8("\t\t->\t")+QString::number(renderContext->xbound+renderContext->wbound));
    heightcov->setText("Height: "+QString::number(renderContext->ybound)+QString::fromUtf8("\t\t->\t")+QString::number(renderContext->ybound+renderContext->hbound));
    depthcov->setText("Depth: "+QString::number(renderContext->zbound)+QString::fromUtf8("\t\t->\t")+QString::number(renderContext->zbound+renderContext->dbound));
}

//Initialize program values
void VolRender::InitializeValues()
{
    planeTexture = 0;
    colourMap[0] = colourMap[1] = colourMap[2] = colourMap[3] = 0;
    rgbaMap = NULL;
    zmin = DBL_MAX;
    zmax = DBL_MIN;

    imagewidth=new QLabel();
    imageheight=new QLabel();
    imagedepth=new QLabel();

    widthcov=new QLabel();
    heightcov=new QLabel();
    depthcov=new QLabel();

    renderContext = NULL;
    defaultContext = NULL;

    //Shader Init
    //shader->shaderProgram = NULL;
    //shader->vertexShader = shader->fragmentShader = NULL;
    
    circular=0;
    vertical=0;

    //Init mouse values
    mousePoint=QPoint(0,0);
    leftDown=false;
    rightDown=false;
    edgeDrag=false;

    planeShift[0] = QPoint(0,0); 
    planeShift[1] = QPoint(0,0);
    planeShift[2] = QPoint(0,0);

    for (int i = 0; i < 16; i++) {
        rotationMatrix[i] = 0.0f;
        model[i] = 0.0;
        proj[i] = 0.0;
    }

    for (int i = 0; i < BUFSIZE; i++)
        selectBuffer[i] = 0u;

    transferSize = 0;

    view[0] = view[1] = view[2] = view[3] = 0;

    displayTimerID = startTimer(33);

    //Declare detail view connection action
    viewMove=new QAction(this);

    blendSetting=0.0f;

    //Display Variables
    nearPlane=-1;
    selectPlane=-1;
    selectedEdge=QPoint(0,0);
    frameVisible=true;
    selectorDisplayVisible=true;

    HUDElement=-1; 

    resultWaiting=false;

    progressOpacity=0.0f;
    progressCount=0;

    createDefaultContext();
    #ifdef __APPLE__
    createShaders("./astrovis.app/Contents/Resources/Basic.vsh", "./astrovis.app/Contents/Resources/Basic.fsh");
    #else
    createShaders("./Basic.vsh", "./Basic.fsh");
    #endif

    //Camera variables
    renderContext->zoomAmount = 200;
    zoomAmount = 200;
    minZoom = 0;
    maxZoom = 10000;

    //Plane Shift Amounts
    renderContext->pShift[0] = QPoint(0,0);
    renderContext->pShift[1] = QPoint(0,0);
    renderContext->pShift[2] = QPoint(0,0);

    displayHasChanged=false;
}

//Create default context
void VolRender::createDefaultContext()
{
    #ifdef __APPLE__
    FitsFileHandler * f =new FitsFileHandler("./astrovis.app/Contents/Resources/astrovis.fits", NULL);
    #else
    FitsFileHandler * f =new FitsFileHandler("astrovis.fits", NULL);
    #endif
    defaultContext = new VolumeContext(f, 0);

    ContextList.push_back(defaultContext);

    setRenderContext(defaultContext);
    //initTex();
    defaultContext->setInputSettings(100,50,3,0);

    //defaultContext->setBoundaries();
    defaultContext->startDownsampler();  
}

//Set new render context
void VolRender::setRenderContext(VolumeContext * c)
{
    if(renderContext!=NULL)
    {
        renderContext->pauseDownsampler();
        disconnect(renderContext->getDownsampler(), SIGNAL(iterationComplete(double)),this,SLOT(DownsamplerComplete(double)));
    }

    renderContext=c;

    connect(renderContext->getDownsampler(), SIGNAL(iterationComplete(double)),this,SLOT(DownsamplerComplete(double)));
    renderContext->setVolumeDimension();
    setInfoLabels();
}

void VolRender::selectorDimensionChange(int w,int h)
{
    VolumeContext::selectorWidth=w;
    VolumeContext::selectorHeight=h;

    Coordinate c;
    c.Valid=renderContext->focalArea.Point.Valid;
    c.x=renderContext->focalArea.Point.x*renderContext->volumewidth;
    c.y=renderContext->focalArea.Point.y*renderContext->volumeheight;
    c.z=renderContext->focalArea.Point.z*renderContext->volumedepth;

    setSelectorSize();
    setSelectorPosition(c);

    updateDisplay();
}

void VolRender::moveSelector(float x,float y,float z)
{
    renderContext->setSelectorPoint(x,
                                        y,
                                            z);
    updateDisplay();
}

void VolRender::selectorMove(double x,double y,double z)
{
    renderContext->moveSelector(x,y,z);
    updateDisplay();
}

//Deconstructor
VolRender::~VolRender()
{

}