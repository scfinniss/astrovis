#ifndef DETAILRENDER_H
#define DETAILRENDER_H

#include <QtGui>

#define GL_GLEXT_PROTOTYPES

#include <GL/glew.h>

#ifdef __APPLE__
#include <OpenGL/glext.h>
#include <OpenGL/glu.h>
#else
#include <GL/glext.h>
#include <GL/glu.h>
#endif

#include "VolRender.h"
#include "DetailContext.h"
#include "../fits/FitsFileHandler.h"
#include "../fits/FitsImageHeader.h"
#include "../fits/FitsTreeWidget.h"
#include "ShaderObject.h"

class DetailRender: public QGLWidget
{	
	Q_OBJECT

	public:
		DetailRender(QMainWindow * parent, FitsTreeWidget * tree,QGridLayout *DetailViewLayout,ShaderObject* s);
		DetailRender(QMainWindow * parent, FitsTreeWidget * tree, VolRender * volume);
		DetailRender(QMainWindow *parent, FitsTreeWidget * tree, VolRender * volume, QGridLayout *DetailViewLayout);
		DetailRender(QMainWindow *parent, FitsTreeWidget * tree, VolRender * volume, QGridLayout *DetailViewLayout, ShaderObject * s);
		~DetailRender();

	public slots:
		void dataRequestComplete(Datacube*);
		void dataSliceComplete(int,Datacube*);
		void textureUpdateTimer();
		void fitsImageHeaderLoadRequest(QTreeWidgetItem* i);

		void setTransferMap(float**,int*,int);

		void incrementSlicePosition();
		void decrementSlicePosition();
		int setSlicePosition(int);

		void setTopOrientation();
		void setFrontOrientation();
		void setSideOrientation();
		int setSliceOrientation(int);

		void placeFocus(int,int,double,double);

		void shiftPlane(int,int,double);

		void centreView();

		void newVolumeContext(VolumeContext *);

	signals:
		void pauseVolume();
		void resumeVolume();
		void windowResize(int w,int h);
		void volumeSelectorMove(double,double,double);
		void selectorMoved(float, float, float);

	private:
		void initializeGL();
		void resizeGL(int w, int h);

		void volumeConnect(VolRender*);

		void InitializeValues();

		void addButtons();
		void createFrame(QGridLayout*);
		void moveButtons();

		void setTransferTexture(float*, int,int);
		void setTex();

		void paintGL();
		void drawDisplay();
		void drawLabel();
		void drawDisplayInfo();

		void createDefaultContext();

		void activateNewContext(DetailContext* c);
		void activateExistingContext(DetailContext* c);

		void setRenderContext(DetailContext*);

		//Mouse management variables
		QPoint mousePoint;
		bool leftDown;
		bool rightDown;

		void updateInput();

		//Event Functions
		void keyPressEvent( QKeyEvent *e );

		void mousePressEvent ( QMouseEvent * e );
		void leftPress(inputEvent*);
		void rightPress(inputEvent*);

		void mouseReleaseEvent( QMouseEvent * e );
		void leftRelease(inputEvent* e);
		void rightRelease(inputEvent* e);

		void mouseDoubleClickEvent ( QMouseEvent * e);
		
		void rightDoubleClick(inputEvent*);
		void leftDoubleClick(inputEvent*);

		void mouseMoveEvent ( QMouseEvent * e );
		void mouseDrag(inputEvent*);
		void leftMouseDrag(inputEvent*);
		void rightMouseDrag(inputEvent*);

		void wheelEvent ( QWheelEvent * e );
		void wheelScroll(inputEvent*);

		void enterEvent(QEvent * e);
		void leaveEvent(QEvent * e);

		void updateDisplay();
		void timerEvent(QTimerEvent *e);

		
		void createShaders(QString vshader, QString fshader);

		DetailContext * renderContext;
		DetailContext * defaultContext;

		std::vector<DetailContext*> ContextList;

		//User input even queue
		std::queue<inputEvent*> inputQueue;

		//Variables
		int displayTimerID;
		bool displayHasChanged;

		//QGLShader *fragmentShader,*vertexShader;
		//QGLShaderProgram *shaderProgram;

		ShaderObject* shader;

		//Texture reference
		GLuint planeTexture;

		GLuint * texturePieces;
		//Textures larger than 2048 get divided into smaler pieces by Width and Height
		int texPiecesW;
		int texPiecesH;

		int texMaxSize;

		GLuint colourMap[4];

		QFont f;

		//Interface Buttons and Widgets
		QPushButton * stepForward ;
    	QPushButton * stepBackward ;
    	QLabel * sliceLabel ;
    	QLineEdit* slicePositionEdit ;
    	QMenu* orientationMenu;

    	QString viewString[3];
    	
    	QFrame* detailFrame;
    	QBoxLayout* detailLayout;

    	bool texPending;

    	void setSliceLabel(int n);
};

#endif
