#ifndef VOLUMECONTEXT_H
#define VOLUMECONTEXT_H

#include "../fits/FitsImageHeader.h"
#include "../fits/FitsFileHandler.h"
#include "../fits/Datacube.h"
#include "../ipc/Downsampler.h"
#include "DetailContext.h"
#include "Context.h"

class VolumeContext : public Context
{
	public:
		VolumeContext(FitsImageHeader * data);
		VolumeContext(FitsFileHandler*,int);
		VolumeContext(VolumeContext*);
		~VolumeContext();

		static int selectorWidth,selectorHeight;
		//Dimensions of data being accessed
		int datax,datay,dataz;
		int datawidth,dataheight,datadepth;
		//Dimensions of generated volume
		int volumewidth,volumeheight,volumedepth;
		//Plain Shifts
		QPoint pShift[3];
		//File Boundaries
		int xbound,ybound,zbound,wbound,hbound,dbound;

		float minValue,maxValue;

		int zoomAmount;

		Area focalArea;



		void initValues();

		int startDownsampler();
		void stopDownsampler();
		void pauseDownsampler();
		void resumeDownsampler();
		void waitDownsampler();

		void moveSelector(double,double,double);


		Downsampler * getDownsampler();

		void setBoundaries(VolumeContext*);
		void setBoundaries();
		void setBoundaries(int x,int y,int z,int w,int h,int d);

		void setSelectorWidth(double,double,double);
		void setSelectorHeight(double,double,double);
		void setSelectorPoint(double,double,double);

		Datacube * getVolumeData();

		void setInputSettings(int w,int h,int d,int s);

		void setDataCoverage(int,int,int,int,int,int);

		void setDetail(DetailContext*);
		DetailContext* getDetail();

		int getVolumeByPlane(int p);

		void getDim(int*);

		void setVolumeDimension();

		FitsImageHeader * getImage();
		Datacube* getCube();
		void setCube();

		//static void setSelectorDimension(int w,int h);


	private:
		Downsampler * downsampler;
		FitsImageHeader * imageItem;
		Datacube * displayCube;

		DetailContext * detail;

		int boundaries[6];
		int inputwidth,inputheight,inputdepth, scatterCount;

		
		

};


//int VolumeContext::selectorHeight=0;

#endif