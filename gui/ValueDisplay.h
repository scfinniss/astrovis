#ifndef VALUEDISPLAY_H
#define VALUEDISPLAY_H

#include <QtOpenGL>

#define BUFSIZE 512

class ValueDisplay : public QWidget
{
	public:
		ValueDisplay();
		~ValueDisplay();
		void setValue(double,double,double,float);
		void setCoverage(int*c);
		void setDimensions(int x,int y,int z);
		
	private:
	QLabel *x,*y,*z,*val;
	QLabel *xdim,*ydim,*zdim;
	QLabel *xcov,*ycov,*zcov;
};

#endif
