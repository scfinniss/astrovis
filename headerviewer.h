#ifndef HEADERVIEWER
#define HEADERVIEWER

#include "fits/FitsFileHandler.h"
#include "fits/FitsTreeWidget.h"

#include <QtGui>

class Headerviewer : public QDialog {
	
	Q_OBJECT

public:

	Headerviewer( QMainWindow * parent);
	~Headerviewer();

	QTableWidget * table;
	QGridLayout * layout;
	FitsFileHandler * cube;

	QStringList sl;

	

	public slots:
		void DisplayHeader ();
		void GetHeader(QTreeWidgetItem* i);

};

#endif