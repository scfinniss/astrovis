#version 120

uniform sampler1D redLUT;
uniform sampler1D greenLUT;
uniform sampler1D blueLUT;
uniform sampler1D alphaLUT;

uniform float alphaSetting;

uniform sampler3D texture;


void main()
{
	float index= texture3D(texture,gl_TexCoord[0].xyz).r;

    float re = texture1D(redLUT, index).r;
    float gr = texture1D(greenLUT, index).r;
    float bl = texture1D(blueLUT, index).r;
    float al = max(texture1D(alphaLUT, index).r,alphaSetting);
    
    gl_FragColor = vec4(re,gr,bl,al);
}
